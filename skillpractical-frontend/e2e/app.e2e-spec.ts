import { SkillpracticalfrontendPage } from './app.po';

describe('skillpracticalfrontend App', () => {
  let page: SkillpracticalfrontendPage;

  beforeEach(() => {
    page = new SkillpracticalfrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
