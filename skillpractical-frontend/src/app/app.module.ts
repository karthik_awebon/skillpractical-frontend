import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { ApplicationRef, ErrorHandler } from '@angular/core';

import {
EventListComponent,
EventThumbnailComponent,
EventDetailsComponent,
EventListResolver,
EventService,
EventsAppComponent,
SessionListComponnet,
DurationPipe, EventsFilterPipe, EventResolver,UpcomingEventsComponent,NewEventQueryFormComponent
,AttendedEventsComponent,MyCertificatesComponent

} from './events/index'


import { AppComponent } from './app.component';
import { CollapsibleWellComponent} from './common/collapsible-well.component'
import { NavBarComponent } from './nav/navbar.component'
import { appRoutes } from './app.routes'
import { Error404Component } from './errors/404.component'

import {ShareButtonsModule} from 'ngx-sharebuttons';

import {UserService} from './community/user.service';

import {TooltipModule} from "ng2-tooltip";

import { ToastrModule } from 'toastr-ng2';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ChartsModule } from 'ng2-charts';

import { 

    HomePageComponent ,graphComponent,trendingComponent,updatesComponent,tasksComponent,
    HomePageService,PastReportsComponent,MessagesListComponent, UpdateDetailComponent, TaskDetailComponent, AlltasksComponent

        } from './homepage/index'

import {NavBarService,termsandconditionsComponent,privacypolicyComponent,FAQcomponent, LandingComponent, FeaturesComponent, AboutusComponent, ContactUsComponent, MembershipInfoComponent, FooterContentComponent
,PricingComponent
} from './nav/index'

import {RegisterComponent } from './user/register.component'
import {Login } from './user/login.component'
import {ForgotPassword} from './user/forgotpassword.component'
// import {CheckUserForRegistration} from './user/checkuser.component'
import {LoginService} from './user/services/login.service'
import {RegisterService} from './user/services/register.service'
import {ForgotPasswordService} from './user/services/forgotpassword.service'

import {SettingUserComponent} from './user/setting.component';

import {SettingsService} from './user/services/setting.service';

import {HelpFormComponent} from './common/helpform/helpform.component';

import {LoginGuard}  from './user/routerguard/loginguard.component'

import { ProjectFilterPipe } from './homepage/graph/pastreports/pastreportsfilter.pipe'

import { GlobalErrorHandler } from './common/error-handler';

import {LoggingService} from './common/logging.service';

import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { MessageDetailComponent } from 'app/homepage/messages/messages-detail/messages-detail.component';

import {NgxPaginationModule} from 'ngx-pagination';
import { PreventLoggedInAccess } from 'app/user/routerguard/preventroutesloggedin.component';
import { EqualValidator } from 'app/common/equal-validator.directive';
import { CapitalizePipe } from 'app/common/capitalize.pipe';
import { GeneralComponent } from 'app/user/general/general.component';
import { EducationComponent } from 'app/user/education/education.component';
import { ResumeLinksComponent } from 'app/user/resume-links/resume-links.component';
import { UserskillsComponent } from 'app/user/userskills/userskills.component';
import { ChangepasswordComponent } from 'app/user/changepassword/changepassword.component';
import { UpgradeComponent } from './user/upgrade/upgrade.component';
import { UpgradeService } from './user/services/upgrade.service';
import { OrderDetailsComponent } from 'app/user/order-details/order-details.component';
import { SubscriptionAndBillingComponent } from 'app/user/subscription-and-billing/subscription-and-billing.component';
import { SubscriptionService } from 'app/user/services/subscription.service';
import { BookWorkshopComponent } from 'app/nav/workshops/book-workshop/book-workshop.component';
import { WorkshopDetailComponent } from 'app/nav/workshops/workshop-detail/workshop-detail.component';
import { WorkshopService } from 'app/nav/workshops/workshop.service';
import { WorkshopThumbnailComponent } from 'app/nav/workshops/workshop-thumbnail/workshop-thumbnail.component';
import { RequestWorkshopComponent } from 'app/nav/workshops/request-workshop/request-workshop.component';
import { BlogComponent } from 'app/blog/blog/blog.component';
import { BlogDetailComponent } from 'app/blog/blog-detail/blog-detail.component';
import { BlogPostService } from 'app/blog/blogpost.service';

import { TestComponent } from 'app/Tests/view/home/home.component';
import { QuestionComponent, MessageComponent } from 'app/Tests/view/question/question.component';
import { TimerComponent } from 'app/Tests/view/timer/timer.component';
import { AnswerComponent } from 'app/Tests/view/answer/answer.component';
import { ConfirmDialogComponent } from 'app/Tests/view/confirm/confirm.dialog';
import { TestDetailComponent, CheckAnswersComponent } from 'app/Tests/view/testdetail/testdetail.component';
import { SearchTestComponent } from 'app/Tests/view/searchtest/searchtest.component';
import { TestCardDisplayComponent } from 'app/Tests/view/testcarddisplay/testcarddisplay.component';
import { TestInstructionComponent } from 'app/Tests/view/testinstruction/testinstruction.component';
import { DataService } from 'app/Tests/service/data.service';
import { MaterialModule } from './Tests/material/material.module';
import { StarComponent } from './common/rating/star.component';

import { DisqusModule } from 'angular2-disqus';
import { WorkshopFilterPipe } from 'app/nav/workshops/shared/workshop-filter.pipe';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,CapitalizePipe,EqualValidator,EventsAppComponent,EventListComponent, EventThumbnailComponent, NavBarComponent,
    EventDetailsComponent, Error404Component,SessionListComponnet,
    CollapsibleWellComponent,DurationPipe,EventsFilterPipe,UpcomingEventsComponent,NewEventQueryFormComponent,
    AttendedEventsComponent,MyCertificatesComponent,
    
      //homepage components
      HomePageComponent ,MessagesListComponent,graphComponent,trendingComponent,AlltasksComponent,
      updatesComponent,UpdateDetailComponent,tasksComponent,TaskDetailComponent,PastReportsComponent,MessageDetailComponent,

      //upgrade
      UpgradeComponent,
     //user
     RegisterComponent, Login,
     ForgotPassword,
     //user setting
     SettingUserComponent,
     PricingComponent,
     //common
     HelpFormComponent,
     
     //landingpage
     LandingComponent,
     //footer content
     FooterContentComponent,
     //features 
     FeaturesComponent,
     //aboutus
     AboutusComponent,
     //ContactUs
     ContactUsComponent,
     //Frequently asked questions
     FAQcomponent, 
    //membership info
     MembershipInfoComponent,
     //privacy policy component
     privacypolicyComponent,
     termsandconditionsComponent,
     ProjectFilterPipe,
     GeneralComponent,
     EducationComponent,
     ResumeLinksComponent,
     UserskillsComponent,
     ChangepasswordComponent,
     OrderDetailsComponent,
     SubscriptionAndBillingComponent,
     BookWorkshopComponent,
     WorkshopThumbnailComponent,
     WorkshopDetailComponent,
     RequestWorkshopComponent,
     WorkshopFilterPipe,
     BlogComponent,
     BlogDetailComponent,

    //  rating component
    StarComponent
    
  ],
  imports: [

    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,    HttpClientModule,
    RouterModule.forRoot(appRoutes, {useHash:true}),
    ShareButtonsModule.forRoot(),
    TooltipModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    ChartsModule,
    MatButtonModule, MatCheckboxModule,NgxPaginationModule,
    MaterialModule,
    DisqusModule
  ],
  providers: [EventService,EventResolver, EventListResolver,LoginService,UserService,HomePageService,RegisterService,SettingsService,
               LoginGuard,ForgotPasswordService,
              GlobalErrorHandler,
              LoggingService,NavBarService,NavBarComponent,

              PreventLoggedInAccess,

              UpgradeService,
              SubscriptionService,
              WorkshopService,
              BlogPostService,

              DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(){
    
  }
}
