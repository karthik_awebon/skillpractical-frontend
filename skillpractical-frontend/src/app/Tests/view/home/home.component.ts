import { Component, OnInit, HostListener } from '@angular/core';
import {DataService} from '../../service/data.service';
import { MatDialog } from '@angular/material';
import {ConfirmDialogComponent} from '../confirm/confirm.dialog';
import { ActivatedRoute, Router } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import * as $ from 'jquery/dist/jquery.min.js';
import { ApiSettings } from '../../../ApiSettings';
import { StarRatingTestComponent } from '../../common/givetestrating.component';
import { Directive, ElementRef} from '@angular/core'
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class TestComponent implements OnInit {

  testStarted = false;
  testShown = false;
  testId:number;
  testInfo: any;
  timeLeft: any;
  time: any;
  questions: Array<any> = new Array<any>();
  answers: Array<any>;
  submitted: Array<any>;
  answerResponse: Array<any>;
  currentQuestion = 0;
  userTest: any;
  currentQuestionInfo: any;
  isFirstQuestion = true;
  isLastQuestion = false;


  @HostListener('window:beforeunload')
  doSomething(e) {
    if (this.testStarted) {
      return false;
    }

  }

  @HostListener('window:unload')
  doSomethingElse($event) {
    if (this.testStarted) {
      let body = JSON.stringify({user_id: this.dataService.USER_ID, test_id: this.testId, time_left: this.timeLeft});
      $.ajax({
        type: 'POST',
        url: ApiSettings.API_ENDPOINT + '/rest/usertest/updateUserTestList.on',
        async: false,
        processData: false,
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem("token")
        },
        data: body,
        contentType: 'application/json',
        success : function(data) {
          //console.log(true);
        }
      });
    }
  }
  constructor(private dataService: DataService,private router:Router, private dialog: MatDialog,private route:ActivatedRoute) {

  }

  changeQuestion(event) {
      this.currentQuestion = event.value;
      this.dataService.getQuestion(this.questions[this.currentQuestion].question_id).subscribe(responseJson => {
        this.currentQuestionInfo = responseJson.question_info;
      });    
  }

  showConfirmDialog(message, title): Observable<boolean> {
    let dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        message: message,
        title: title
      }
    });

    return dialogRef.afterClosed();

  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.startTest(params.id);
    });
    this.dataService.timerTick.subscribe(res => {
      this.timeLeft = res.value;
    });
  }

  startTest(testId:number) {
    this.testId = testId;
    this.dataService.createTestForUser(this.dataService.USER_ID, this.testId).subscribe(res => {
      this.userTest = res.user_test_info;
      if (this.userTest.status !== 'completed') {
        this.time = this.userTest.time_left;
      }
      console.log(this.userTest);
      this.dataService.getQuestions(this.testId).subscribe(resp => {
        console.log(resp);
        this.questions = resp.test_info.question_info;
        this.answers = new Array<any>(this.questions.length);
        this.submitted = new Array<any>(this.questions.length);
        this.answerResponse = new Array<any>(this.questions.length);
        this.dataService.getQuestion(this.questions[this.currentQuestion].question_id).subscribe(responseJson => {
            this.currentQuestionInfo = responseJson.question_info;
        });
        if (this.userTest.status !== 'completed') {
          this.dataService.getAnswers(this.dataService.USER_ID,  this.testId).subscribe(res => {
            this.resumeTest(res.user_test_answer_info);
          });
        } else {
          this.dataService.getAnswers(this.dataService.USER_ID,  this.testId).subscribe(res => {
            this.resumeTest(res.user_test_answer_info);
          });
        }
        this.testInfo = resp.test_info;
        if (this.userTest.time_left === 0) {
          this.time = this.testInfo.test_duration;
        }
        if (this.userTest.status !== 'completed') {
          this.testStarted = true;
        } else {
          this.testStarted = false;
        }

        this.testShown = true;
        console.log(this.questions);
      });
    });
  }

  resumeTest(prevAnswers) {
    if (!prevAnswers.status) {
      this.questions.forEach((item, index) => {
        let ans = prevAnswers.find(it => it.question_id == item.question_id);
        if (ans) {
          this.answerResponse[index] = ans;
          this.submitted[index] = true;
          if (item.question_type === 'single') {
            this.answers[index] = ans.answers;
          } else {
            this.answers[index] = this.parseAnswers(ans.answers);
          }
        }
      });
    }
  }

  parseAnswers(arrAns) {
    return arrAns.split(',');

  }

  previousQuestion() {
    if (this.currentQuestion > 0) {
      this.dataService.getQuestion(this.questions[--this.currentQuestion].question_id).subscribe(responseJson => {
        this.currentQuestionInfo = responseJson.question_info;
        if(this.currentQuestion == 0){
          this.isFirstQuestion = true;
        }
        this.isLastQuestion = false;           
      });
    }
  }

  nextQuestion() {
    if (this.currentQuestion < this.questions.length - 1) {
      this.dataService.getQuestion(this.questions[++this.currentQuestion].question_id).subscribe(responseJson => {
        this.currentQuestionInfo = responseJson.question_info;
        this.isFirstQuestion = false;
        if(this.currentQuestion == this.questions.length - 1){
          this.isLastQuestion = true;
        } 
      });      
    }
  }

  onTimeFinished() {
    this.dataService.completeTest(this.dataService.USER_ID,  this.testId,this.timeLeft).subscribe(res => {
      this.testStarted = false;
      this.dataService.testCompleted.emit();
      // window.location.href = '#/tests/testdetail/'+this.testId;
      this.openDiaolog();
    });

    
  }

  completeTestBtn() {
 
    this.dataService.completeTest(this.dataService.USER_ID,  this.testId, this.timeLeft).subscribe(res => {
      this.testStarted = false;
      this.dataService.testCompleted.emit();
      // window.location.href = '#/tests/testdetail/'+this.testId;
        this.openDiaolog();
    });
  }

  pauseTest() {
    this.showConfirmDialog('You are leaving test. Your progress will be saved', 'Test save').subscribe(res => {
      if (res) {
        this.dataService.pauseTest(this.dataService.USER_ID,  this.testId, this.timeLeft).subscribe(res => {
          this.testShown = false;
          this.dataService.testCompleted.emit();
          window.location.href = '#/tests/testdetail/'+this.testId;
        });
      }
    });
  }

  submitAnswer() {
    console.log(this.userTest);
    console.log(this.answers[this.currentQuestion]);
    this.dataService.submitAnswer(this.userTest.user_test_id,
      this.questions[this.currentQuestion].question_id,
        this.prepareAnswers(this.currentQuestionInfo, this.answers[this.currentQuestion])).subscribe(res => {
      this.submitted[this.currentQuestion] = true;
      this.answerResponse[this.currentQuestion] = res.user_test_info;
      console.log(this.answers[this.currentQuestion]);
      this.checkAllSubmitted();
     });
  }

  checkAllSubmitted() {
    let count = 0;
    this.submitted.forEach(item => {
      if (item) {
        count++;
      }
    });
    if (count === this.questions.length) {
      this.dataService.completeTest(this.dataService.USER_ID,  this.testId,this.timeLeft).subscribe(res => {
        this.testStarted = false;
        this.dataService.testCompleted.emit();
        // window.location.href = '#/tests/testdetail/'+this.testId;
        this.openDiaolog();
      });
    }
  }

  prepareAnswers(question, answers) {
    if (question.question_type === 'single') {
      return answers;
    }
    let result = '';
    answers.forEach(item => {
      result += item + ',';
    });
    if (result.length > 0) {
      result = result.substr(0, result.length - 1);
    }
    console.log(result);
    return result;
  }

  canSubmitAnswer() {
    if (this.submitted[this.currentQuestion]) {
      return false;
    }
    if (this.answers[this.currentQuestion]) {
      if (Array.isArray(this.answers[this.currentQuestion])) {
        if (this.answers[this.currentQuestion].length > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    }
    return false;
  }


  //for rating component
  @ViewChild('openmodal') openmodal: ElementRef;

  @ViewChild('closemodal') closemodal: ElementRef;

  openDiaolog() {
    this.openmodal.nativeElement.click();
  }

  score : number = 0;
  displayRatingScore = 3.5;
  
  onRateChange = (score) => {
    this.score = score;
    this.dataService.updateTestRating(this.testId, this.score).subscribe(res => {
      if(this.closemodal.nativeElement.click()){
      window.location.href = '#/tests/testdetail/'+this.testId;
      }
    });

  }

  closeratingpopup(){
    window.location.href = '#/tests/testdetail/'+this.testId;
  }

}
