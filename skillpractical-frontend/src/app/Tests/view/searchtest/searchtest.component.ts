import { Component, OnInit } from '@angular/core';
import {DataService} from '../../service/data.service';
import {Observable} from 'rxjs/Observable';
@Component({
  selector: 'app-searchtest',
  templateUrl: './searchtest.component.html',
  styleUrls: ['./searchtest.component.css']
})
export class SearchTestComponent implements OnInit {
  branches:any;
  subjects:any;
  selectedBranch: string = "";
  selectedSubject: string = "";
  selectedTestLevel: string = "";
  completed: boolean;
  resumed: boolean;
  savedtest: boolean;
  search_text: string = "";
  searchResult: Observable<any[]>;
  p: number = 1;
  total: number;
  loading: boolean;
  itemsPerPage: number = 10;
  isTextSearch: boolean = false;
  
  constructor(private dataService: DataService) {

  }
  ngOnInit() {
    this.dataService.getBranches().subscribe(res => {
        this.branches = res.branch_list;
        this.dataService.getSubjects(this.branches.map((branch)=>{ return branch.branch_id}).join(",")).subscribe(res => {
          this.subjects = res.subject_list;
        });        
    });
    this.getPage(1); 
  }

  textSearch(){
    this.isTextSearch=true;
    this.clearFilter();
    this.getPage(1);
  }

  clearFilter(){
    this.selectedBranch = '';
    this.selectedSubject = '';
    this.selectedTestLevel = '';
    this.completed = false;
    this.resumed = false;
    this.savedtest = false;
    this.dataService.getSubjects(this.branches.map((branch)=>{ return branch.branch_id}).join(",")).subscribe(res => {
      this.subjects = res.subject_list;
    });  
  }

  getPage(page: number) {
    this.loading = true;
    var offset = (page-1)*(this.itemsPerPage);
    
    if(!this.isTextSearch && (this.selectedBranch || this.selectedSubject)){
      this.search_text = '';
      var selectedSubjects = this.selectedSubject;
      if(this.selectedSubject == ""){
        selectedSubjects = this.subjects.map(subject=>{
            return subject.subject_id;
        }).join(",");
      }
      this.searchResult = this.dataService.searchTest(selectedSubjects,
        this.selectedTestLevel,this.dataService.USER_ID,(this.completed)?"Y":"N",(this.resumed)?"Y":"N",
        (this.savedtest)?"Y":"N",offset,this.itemsPerPage)
          .do(res => {
            if(res.result_info.total_records > 0){
              this.total = res.result_info.total_records;
            }else{
              this.total = 0;
            }
              this.p = page;
              this.loading = false;
          })
          .map((res) => {
            if(res.result_info.total_records > 0){
              return res.result_info.test_info;
            }else{
              return [];
            }          
            }
          );
      }else{
        this.searchResult = this.dataService.searchTestByText(this.dataService.USER_ID,offset,this.itemsPerPage,this.search_text)
            .do(res => {
              if(res.result_info.total_records > 0){
                this.total = res.result_info.total_records;
              }else{
                this.total = 0;
              }
                this.p = page;
                this.loading = false;
                this.isTextSearch = false;
            })
            .map((res) => {
              if(res.result_info.total_records > 0){
                return res.result_info.test_info;
              }else{
                return [];
              }          
              }
            );
      }
  }
  isBranchSelected(branchId): boolean {
    if(!this.selectedBranch) {
        return false;
    }
    var selectedBranchs  = this.selectedBranch.split(",");
    for(var x=0;x<selectedBranchs.length;x++){
      if(selectedBranchs[x] == branchId)
        return true;
    }
    return false;
  }

  isSubjectSelected(subjectId): boolean {
    if(!this.selectedSubject) {
        return false;
    }
    var selectedSubjects  = this.selectedSubject.split(",");
    for(var x=0;x<selectedSubjects.length;x++){
      if(selectedSubjects[x] == subjectId)
        return true;
     }
    return false;
  }

  selectSubject(subjectId:string){
    if(this.selectedSubject != ""){
      var selectedSubjects  = this.selectedSubject.split(",");
      var inSelectedList = false;
      for(var x=0;x<selectedSubjects.length;x++){
        if(selectedSubjects[x] == subjectId){
          inSelectedList = true;
        }
      }
      if(!inSelectedList)         
        this.selectedSubject+=","+subjectId;
      else{
        var unSelectedElement = selectedSubjects.indexOf(String(subjectId));
        selectedSubjects.splice(unSelectedElement, 1);
        this.selectedSubject = selectedSubjects.join(",");
      }
    }else{
      this.selectedSubject = String(subjectId);
    }
    this.getPage(1);
  }

  isTestLevelSelected(testLevel): boolean {
    if(!this.selectedTestLevel) {
        return false;
    }
    var selectedTestLevels  = this.selectedTestLevel.split(",");
    for(var x=0;x<selectedTestLevels.length;x++){
      if(selectedTestLevels[x] == testLevel)
        return true;
     }
    return false;
  }

  selectTestLevel(testLevel:string){
    if(this.selectedTestLevel != ""){   
      var selectedTestLevels  = this.selectedTestLevel.split(",");
      var inSelectedList = false;
      for(var x=0;x<selectedTestLevels.length;x++){
        if(selectedTestLevels[x] == testLevel){
          inSelectedList = true;
        }
      }
      if(!inSelectedList)         
        this.selectedTestLevel+=","+testLevel;
      else{
        var unSelectedElement = selectedTestLevels.indexOf(testLevel);
        selectedTestLevels.splice(unSelectedElement, 1);
        this.selectedTestLevel = selectedTestLevels.join(",");
      }
    }else{
      this.selectedTestLevel = testLevel;
    }       
    this.getPage(1);
  }

  clearBranchSelection(){
    this.selectedBranch = "";
    this.selectedSubject = "";
    this.search_text = "";
    this.dataService.getSubjects(this.branches.map((branch)=>{ return branch.branch_id}).join(",")).subscribe(res => {
      this.subjects = res.subject_list;
    });   
    this.getPage(1);
  }
  clearAllFilters(){
    this.clearFilter();
    this.search_text = "";
    this.getPage(1);
  }
  
  loadSubjects(selectedBranch:string){
    this.selectedBranch = String(selectedBranch);
    this.dataService.getSubjects(this.selectedBranch).subscribe(res => {
        this.subjects = res.subject_list;
        this.selectedSubject = "";
        this.getPage(1);
    });
  }
}
