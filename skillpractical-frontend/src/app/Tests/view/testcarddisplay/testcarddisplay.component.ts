import { Component, OnInit,Input,OnChanges } from '@angular/core';
import { DataService } from 'app/Tests/service/data.service';


@Component({
  selector: 'app-testcarddisplay',
  templateUrl: './testcarddisplay.component.html',
  styleUrls: ['./testcarddisplay.component.css']
})
export class TestCardDisplayComponent implements OnInit,OnChanges {
  @Input() testInfo: any;
  @Input() userTestInfo : any;
  @Input() userFavInfo: any;
  userFavorite:boolean = false;
  constructor(private dataService: DataService) {

  }
  ngOnInit() {
    if(!this.isString(this.userFavInfo))
      this.userFavorite = true;
  }
  ngOnChanges(){
  }
  
  private isString (value) {
    return typeof value === 'string' || value instanceof String;
  };

  clickFavouriteFlag(flag:string){
    this.dataService.saveUserFavoriteTest(this.dataService.USER_ID,this.testInfo.test_id,flag).subscribe(res => {
      if(res.result_msg === "SUCCESS" ){
        if(flag === "N")
          this.userFavorite = false;
        else
          this.userFavorite = true;
      }
    });
  }
}
