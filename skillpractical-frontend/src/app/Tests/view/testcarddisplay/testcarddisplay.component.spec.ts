import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestCardDisplayComponent } from './testcarddisplay.component';

describe('TestCardDisplayComponent', () => {
  let component: TestCardDisplayComponent;
  let fixture: ComponentFixture<TestCardDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestCardDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCardDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
