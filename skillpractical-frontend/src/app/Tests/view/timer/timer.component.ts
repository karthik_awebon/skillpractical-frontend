import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable, Subscription } from 'rxjs/RX';
import { DataService } from 'app/Tests/service/data.service';

@Component({
  selector: 'app-timer',
  template: ` 
        <h1>
            {{hoursDisplay ? hoursDisplay : '00'}} : {{(minutesDisplay) && (minutesDisplay <= 59) ? minutesDisplay : '00'}} : {{(secondsDisplay) && (secondsDisplay <= 59) ? secondsDisplay : '00'}} <br/>
        </h1>
    `,
  styles: [ `
        h1 {
            color: #57acec;
            margin-top: 24px; 
            text-align: center;   
        }    
    `]
})
export class TimerComponent implements OnInit {

  ticks = 0;
  @Output() timeFinished: EventEmitter<any> = new EventEmitter<any>();
  @Input() startTime;
  minutesDisplay: number = 0;
  hoursDisplay: number = 0;
  secondsDisplay: number = 0;

  sub: Subscription;
  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.dataService.testCompleted.subscribe(res => {
      this.sub.unsubscribe();
    });
    this.startTimer();
  }

  private startTimer() {

    let timer = Observable.timer(1000, 1000);
    this.sub = timer.subscribe(
      t => {

        this.ticks = +this.startTime - t;
        this.dataService.timerTick.emit({value: this.ticks});
        if (this.ticks === 0) {
          this.sub.unsubscribe();
          this.timeFinished.emit({value: true});
        }
        this.secondsDisplay = this.getSeconds(this.ticks);
        this.minutesDisplay = this.getMinutes(this.ticks);
        this.hoursDisplay = this.getHours(this.ticks);
      }
    );
  }

  private getSeconds(ticks: number) {
    return this.pad(ticks % 60);
  }

  private getMinutes(ticks: number) {
    return this.pad((Math.floor(ticks / 60)) % 60);
  }

  private getHours(ticks: number) {
    return this.pad(Math.floor((ticks / 60) / 60));
  }

  private pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  }
}
