import { Component, OnInit,Inject } from '@angular/core';
import {DataService} from '../../service/data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-testinstruction',
  templateUrl: './testinstruction.component.html',
  styleUrls: ['./testinstruction.component.css']
})
export class TestInstructionComponent implements OnInit {
    testInfo: any;
    constructor(private dataService: DataService,private route:ActivatedRoute) {
  
    }
    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.dataService.getTestInfo(this.dataService.USER_ID,params.id).subscribe(res => {
              this.testInfo = res.test_info;
            });
          });        
    }
    private isString (value) {
        return typeof value === 'string' || value instanceof String;
    };
}