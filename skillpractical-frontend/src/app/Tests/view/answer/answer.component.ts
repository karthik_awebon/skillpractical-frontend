import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit{
  @Input() questions;
  @Input() submitted;
  @Input() answers;
  @Output() navigate: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {

  }

  navigateToQuestion(index) {
    this.navigate.emit({value: index});
  }
  getBackgroundColor(index) {
    if (this.answers[index]) {
      if (this.answers[index].status === 'correct') {
        return 'green';
      }
      if (this.answers[index].status === 'incorrect') {
        return 'red';
      }
    }
    //   let rightAnswers = this.questions[index].answer_info.filter(item => item.correct_answer === 'Y');
    //   let currentAnswers = this.answers[index];
    //   if (this.questions[index].question_type === 'single') {
    //     if (rightAnswers[0].answer_id === currentAnswers) {
    //       return 'green';
    //     } else {
    //       return 'red';
    //     }
    //   } else {
    //     let total = +this.questions[index].question_number_of_answers;
    //     let goodAnswers = 0;
    //     currentAnswers.forEach(item => {
    //       if (rightAnswers.find(it => it.answer_id === item)) {
    //         goodAnswers++;
    //       }
    //     });
    //     if (goodAnswers === total) {
    //       return 'green';
    //     } else {
    //       return 'red';
    //     }
    //   }
    // }
  }
}
