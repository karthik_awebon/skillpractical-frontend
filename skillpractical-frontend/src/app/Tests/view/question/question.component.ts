import {Component, OnInit, Input, Output, EventEmitter, OnChanges, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit, OnChanges {
  @Input() meta: any;
  @Input() answer: any;
  @Input() submitted: any;
  @Input() testStarted: any;
  solution: any;
  @Output() answerChange: EventEmitter<any> = new EventEmitter<any>();

  constructor(private dialog: MatDialog) { }

  showMessageDialog(message, title) {
    let dialogRef = this.dialog.open(MessageComponent, {
      data: {
        message: message,
        title: title
    }
    });
  }
  ngOnChanges() {
    if (this.isMultiple()) {
      if (!this.answer) {
        this.answer = new Array<any>();
      }
    }
    if (this.submitted) {
      this.solution = this.meta.question_solution;
    } else {
      this.solution = '';
    }
  }
  ngOnInit() {
    if (this.isMultiple()) {
      if (!this.answer) {
        this.answer = new Array<any>();
      }
    }
  }

  isSingle() {
    if (this.meta && this.meta.question_type === 'single') {
      return true;
    }
    return false;
  }

  isMultiple() {
    if (this.meta && this.meta.question_type === 'multiple') {
      return true;
    }
    return false;
  }

  clearAnswer() {
    this.answer = null;
    this.answerChange.emit(this.answer);
  }

  onAnswerChange(event) {
    this.answer = event.value;
    this.answerChange.emit(this.answer);
  }

  isChecked(id) {
     if (this.isArray(this.answer) && this.answer.find(item => +item === id)) {
      return true;
    }
  }

  onCheckChange(event) {
    if (this.isMultiple()) {
      if (!this.answer) {
        this.answer = new Array<any>();
      }
    }
    if (event.checked) {
      this.answer.push(event.source.name);
    } else {
      this.answer.splice(this.answer.findIndex(item => item === event.source.name), 1);
    }
    this.answerChange.emit(this.answer);
  }

  canClearResponse() {
    if (this.isMultiple()) {
      if (this.answer.length > 0) {
        return true;
      }
      return false;
    } else {
      if (this.answer) {
        return true;
      }
      return false;
    }
  }

  private isArray (value) {
    return value && typeof value === 'object' && value.constructor === Array;
  };
}

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
})
export class MessageComponent {

  message: string = '';
  title: string = '';
  constructor(
    public dialogRef: MatDialogRef<MessageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.message = data.message;
    this.title = data.title;
  }

}
