import { Component, OnInit,Inject } from '@angular/core';
import {DataService} from '../../service/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-testdetail',
  templateUrl: './testdetail.component.html',
  styleUrls: ['./testdetail.component.css']
})
export class TestDetailComponent implements OnInit {

  testInfo: any;
  minutesDisplay: number = 0;
  hoursDisplay: number = 0;
  secondsDisplay: number = 0;  
  constructor(private router: Router,private dataService: DataService,private route:ActivatedRoute,private dialog: MatDialog) {
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
  };
  }
  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.dataService.getTestInfo(this.dataService.USER_ID,params.id).subscribe(res => {
        this.testInfo = res.test_info;
        //console.log(this.testInfo);
        var test_duration = 0;
        if(typeof this.testInfo.user_test === 'string' || this.testInfo.user_test instanceof String){
          test_duration = this.testInfo.test_duration;
        }else{
          test_duration = this.testInfo.user_test.time_left;
        }
        this.secondsDisplay = this.getSeconds(test_duration);
        this.minutesDisplay = this.getMinutes(test_duration);
        this.hoursDisplay = this.getHours(test_duration);        
      });
    });

  }
  
  private showQuestionAnswers(){
    this.dataService.checkQuestionAndAnswers(this.testInfo.test_id,this.dataService.USER_ID).subscribe(res =>{
      console.log(res.result_info.question_info);
      let dialogRef = this.dialog.open(CheckAnswersComponent, {
        data: {
          questionAndAnswers: res.result_info.question_info,
          title: "Check Answers"
      }
      });
    })
  }

  private getSeconds(ticks: number) {
    return this.pad(ticks % 60);
  }

  private getMinutes(ticks: number) {
    return this.pad((Math.floor(ticks / 60)) % 60);
  }

  private getHours(ticks: number) {
    return this.pad(Math.floor((ticks / 60) / 60));
  }

  private pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  } 
  
  private isString (value) {
    return typeof value === 'string' || value instanceof String;
  };
}

@Component({
  selector: 'app-message',
  templateUrl: './checkanswers.component.html',
  styleUrls: ['./checkanswers.component.css']
})
export class CheckAnswersComponent {

  questionAndAnswers: any;
  title: string = '';
  constructor(
    public dialogRef: MatDialogRef<CheckAnswersComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.questionAndAnswers = data.questionAndAnswers;
    this.title = data.title;
  }

}