import { Injectable, EventEmitter } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import { ApiSettings } from '../../ApiSettings';


//const BASE_HREF = 'http://ec2-18-218-116-236.us-east-2.compute.amazonaws.com:10080/sp/rest/';


@Injectable()
export class DataService {

  public testCompleted: EventEmitter<any> = new EventEmitter<any>();
  public timerTick: EventEmitter<any> = new EventEmitter<any>();

  public USER_ID = localStorage.getItem("userid");
  public head = new HttpHeaders().set('Authorization', 'Bearer '+localStorage.getItem("token"));

  public httpOptions = {
    headers: this.head
  };

  constructor(private http: HttpClient) {}


  getQuestions(testId): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT + '/rest/test/reqTestByTestId.on', {test_id : testId}, this.httpOptions);
  }

  getAnswers(userId, testId): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT + '/rest/usertestanswer/reqUserTestAnswerByUserIdAndTestId.on', {user_id: userId, test_id : testId}, this.httpOptions);
  }

  createTestForUser(userId, testId): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT + '/rest/usertest/createUserTest.on', {test_id: testId, user_id: userId}, this.httpOptions);
  }

  submitAnswer(userTestId, questionId, answers): Observable<any> {
    return this.http.post( ApiSettings.API_ENDPOINT + '/rest/usertestanswer/createUserTestAnswer.on',
      {user_test_id: userTestId, question_id: questionId, answers: answers}, this.httpOptions);
  }

  completeTest(userId, testId,timeLeft): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT + '/rest/usertest/updateUserTestList.on', {user_id: userId, test_id: testId, time_left: timeLeft, status: 'completed'}, this.httpOptions);
    
  }

  pauseTest(userId, testId, timeLeft): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT + '/rest/usertest/updateUserTestList.on', {user_id: userId, test_id: testId, time_left: timeLeft, status: 'not_completed'}, this.httpOptions);
  }

  getTestInfo(userId, testId): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+ '/rest/test/reqTestInfoByTestAndUserId.on', {user_id:userId, test_id:testId}, this.httpOptions);
  }

  getBranches(): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+ '/rest/branch/reqBranchList.on', {}, this.httpOptions);
  }

  getSubjects(branchId): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+ '/rest/subject/reqSubjectByBranchId.on', {branch_id:branchId}, this.httpOptions);
  }

  getQuestion(questionId): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+ '/rest/test/reqQuestionByQuestionId.on', {question_id:questionId}, this.httpOptions);
  }

  checkQuestionAndAnswers(testId,userId): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+ '/rest/usertestanswer/reqQuestionWithAnswer.on', {test_id:testId,user_id:userId}, this.httpOptions);
  }

  searchTest(subjectId,testLevel,userId,completed='N',resumed='N',savedtest='N',offset=1,limit=10): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+ '/rest/test/reqSearchTest.on', 
    {subject_id:subjectId,test_level:testLevel,user_id:userId,completed:completed,resumed:resumed,savedtest:savedtest,offset:offset,limit:limit}
    , this.httpOptions);
  }
  
  searchTestByText(userId,offset=1,limit=10,search_text=""): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+ '/rest/test/reqSearchTestByText.on', 
    {offset:offset,limit:limit,search_text:search_text,user_id:userId,}
    , this.httpOptions);
  }
  
  saveUserFavoriteTest(userId,testId,flag="Y"): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+'/rest/userfavtest/createUserFavTest.on',{user_id:userId,test_id:testId,flag:flag}, this.httpOptions);
  }

  updateTestRating( testId, score): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT + '/rest/usertest/requpdateTestRating.on', {test_id: testId, rating : score}, this.httpOptions);
    
  }

}
