import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgxPaginationModule } from 'ngx-pagination';

import { TestComponent } from './view/home/home.component';
import {MessageComponent, QuestionComponent} from './view/question/question.component';
import { MaterialModule } from './material/material.module';
import {DataService} from './service/data.service';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TimerComponent} from './view/timer/timer.component';
import {AnswerComponent} from './view/answer/answer.component';
import {ConfirmDialogComponent} from './view/confirm/confirm.dialog';
import {TestDetailComponent,CheckAnswersComponent} from './view/testdetail/testdetail.component';
import {TestInstructionComponent} from './view/testinstruction/testinstruction.component';
import {SearchTestComponent} from './view/searchtest/searchtest.component';
import {TestCardDisplayComponent} from './view/testcarddisplay/testcarddisplay.component';
import { RouterModule } from '@angular/router';
import { testsRoutes } from './tests.routes';
import { CommonModule } from '@angular/common';
import { StarRatingTestComponent } from './common/givetestrating.component';


@NgModule({
  declarations: [
    TestComponent,
    QuestionComponent,
    TimerComponent,
    AnswerComponent,
    MessageComponent,
    ConfirmDialogComponent,
    TestDetailComponent,
    CheckAnswersComponent,
    SearchTestComponent,
    TestCardDisplayComponent,
    TestInstructionComponent,

    StarRatingTestComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    RouterModule.forChild(testsRoutes),
    HttpClientModule,
    // BrowserAnimationsModule,
    NgxPaginationModule   
  ],
  entryComponents: [MessageComponent, ConfirmDialogComponent,CheckAnswersComponent],
  providers: [DataService]
})
export class TestsModule { }
