import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TestComponent} from './view/home/home.component';
import { TestDetailComponent } from './view/testdetail/testdetail.component';
import { TestInstructionComponent } from './view/testinstruction/testinstruction.component';
import { SearchTestComponent } from './view/searchtest/searchtest.component';

export const testsRoutes = [

  {path: 'searchtest', component: SearchTestComponent},
  {path: 'test/:id', component: TestComponent},
  {path: 'testinstruction/:id', component: TestInstructionComponent},
  {path: 'testdetail/:id', component:TestDetailComponent},
  
];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class TestsRoutingModule { }
