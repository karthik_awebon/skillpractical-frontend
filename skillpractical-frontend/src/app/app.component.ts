import { Component } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  
  // Scroll to top on Route Change
  onDeactivate() {
    window.scrollTo(0, 0)
  }
  

  constructor(meta: Meta, title: Title) {

    title.setTitle('Master the Skills');

    meta.addTags([
      { name: 'author',   content: 'SkillPractical: Learning,Skill Development app for Engineering Students'},
      { name: 'keywords', content: 'skillpractical, engineering, skill development, skill upgrade'},
      { name: 'description', content: 'SkillPractical makes learning engineering very easy. Start learning subjects belongs to Computer Science,Electrical,Electronics,Mechanical,Civil' }
    ]);

  }

  
}
