import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { LoggingService } from '../common/logging.service';
import * as StackTrace from 'stacktrace-js';
import {LoggingModel} from '../common/logging.model';
@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  model = new LoggingModel('','');

constructor(private injector: Injector) { }
handleError(error) {
    const loggingService = this.injector.get(LoggingService);
    const location = this.injector.get(LocationStrategy);
    const message = error.message ? error.message : error.toString();
   // get the stack trace, lets grab the last 10 stacks only

   this.model.message = message;

    StackTrace.fromError(error).then(stackframes => {
      const stackString = stackframes
        .splice(0, 20)
        .map(function(sf) {
          return sf.toString();
        }).join('\n');
    // log on the server

    this.model.message = message;
    this.model.stackString = stackString;

      loggingService.postLogging(this.model);
    });
throw error;
  }
  
}