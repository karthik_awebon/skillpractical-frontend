import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';

import { LoggingModel } from './logging.model';

import {Router} from '@angular/router';

import { ApiSettings } from '../ApiSettings';

@Injectable()
export class LoggingService {
  token: string;

  constructor (private http:Http,private router:Router) {}


  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  
  private extractData(res: Response) {
    let body = res.json();
    return body.fields || { };
 }
  
  private errorlogurl = ApiSettings.API_ENDPOINT + '/errorlog.on';
  postLogging(LoggingModel: LoggingModel) : Observable<any> {
      console.log("logging model"+JSON.stringify(LoggingModel));
      let body = JSON.stringify(LoggingModel);
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });

      return this.http.post(this.errorlogurl, body, options)
                      .map(this.extractData)
                      .catch(this.handleError);
  }

}
