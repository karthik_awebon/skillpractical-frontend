import { Component, Input } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { UserIdModel } from '../shared/userid.model'
import {IUser} from '../../community/interfaces/userInterface'
import {IUpdatesData} from '../shared/updates.interface'
import { UserNameModel } from '../shared/username.model'
import {UserService} from '../../community/user.service';
import {HomePageService} from '../shared/homepage.services'
import { CollegeCodeModel } from '../shared/collegecode.model'
@Component({
   selector: 'updates-page',
   templateUrl:'updates.component.html',
   styleUrls: ['updates.component.css']
})
export class updatesComponent {
   
      userIdModel = new UserIdModel(null);
    
      user: IUser;
      userid:string;
    
      currentusername: string;
    
      updatesdata: IUpdatesData[];
    
      tasksname:string;
    
      errorMessage:string;
      modelusername = new UserNameModel('');
      
      modelcollegecode = new CollegeCodeModel('');
      collegecode:string;
      description:string;
      link:string;

     constructor(private userService:UserService,private homepageservice:HomePageService){
        
     }
    date:string;
    name:string;
    ngOnInit() {
        this.currentusername = localStorage.getItem("currentUserName");
        this.modelusername = new UserNameModel(this.currentusername);                         
        this.userService.getUserByName(this.modelusername).subscribe(
                          user => {
                            this.user = user;
                            this.userid = this.user.userid;                    
                            this.collegecode = this.user.collegecode;
                                this.getUpdatesData();    
                                    
                              console.log("user firstname from event detail"+this.user.firstname);
                              console.log("user from event details"+JSON.stringify(this.user));
                            },
                            error => console.log(error)
                          )                  
       }
       getUpdatesData(){
       
        this.modelcollegecode = new CollegeCodeModel(this.collegecode);
            this.homepageservice.getUpdatesData(this.modelcollegecode)                           
                    .subscribe(
                        (data) => {
                        this.updatesdata = data;                                                                                        
                        },
                        err => console.log('error: ', err)
                    );
                console.log("updates data: "+JSON.stringify(this.updatesdata));
       }
       
       clickViewDetail(date,name,description,link){
        this.date = date;
        this.name = name;
        this.description = description;
        this.link=link;
     console.log(" update name:"+this.name)
    }
    }
