import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'update-detail',
  templateUrl: './updates-detail.component.html',
  styleUrls: ['./updates-detail.component.css']
})
export class UpdateDetailComponent implements OnInit {
  @Input() name:string; 
  @Input() date:string; 
  @Input() description:string; 
  @Input() link:string; 

  constructor() { }
  ngOnInit() {
  }
  gohome() {
    window.location.href='homepagecomponent';
}
}