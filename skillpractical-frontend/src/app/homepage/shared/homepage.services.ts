import { Injectable, EventEmitter} from '@angular/core'
import { Subject, Observable} from 'rxjs/RX'
import { Router } from '@angular/router'
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { ApiSettings } from '../../ApiSettings';
import { UserIdModel } from './userid.model';
import {IGraphData} from './graphdata.interface';
import {UserNameModel} from './username.model';
import { ITasksData } from '../shared/tasksdata.interface';
import {ITrendingDataProjects} from '../shared/index'
import {CollegeCodeModel} from './collegecode.model'
import {IUpdatesData} from '../shared/updates.interface'
import {ICountTasks} from '../shared/Icount.tasks.interface'
import {ICountUpdates} from '../shared/Icount.updates.interfaces'
import {ICountTrending} from '../shared/Icount.trending.interface'
import {IMessagesCount} from '../shared/messagescount.interface' 
import { IMessages } from 'app/homepage/shared/messages.interface';
import { DeleteMessageModel } from 'app/homepage/messages/shared/deletemessage.model';
import { MessageIdModel } from 'app/homepage/messages/shared/messageid.model';
import { DeleteMessageDetailModel } from 'app/homepage/messages/shared/deletemessagedetail.model';

@Injectable()
export class HomePageService{
    constructor(private http:Http){
      
    }
    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
    private extractData(res: Response) {
        let body = res.json();
        return body.fields || { };
    }
    
    private url1 = ApiSettings.API_ENDPOINT + '/rest/graph/reqGraphDataByUserLastUpdatedMonth.on';
    getGraphDataByUser(UserIdModel:UserIdModel): Observable<any> {
        console.log("user id model.."+JSON.stringify(UserIdModel));
         let body = JSON.stringify(UserIdModel);
         let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
         let options = new RequestOptions({headers: headers});
        return this.http.post(this.url1,body,options)
            .map((response: Response) => <IGraphData> response.json().GraphDataByUser_info)
            .do(data => console.log('All graph data by user Id: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }
    private urlgraphdataallmonths = ApiSettings.API_ENDPOINT + '/rest/graph/reqGraphDataByUserEveryMonth.on';
    
    getGraphDataByUserEveryMonth(UserIdModel:UserIdModel): Observable<any> {
            console.log("user id model.."+JSON.stringify(UserIdModel));
             let body = JSON.stringify(UserIdModel);
             let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
             let options = new RequestOptions({headers: headers});
            return this.http.post(this.urlgraphdataallmonths,body,options)
                .map((response: Response) => <IGraphData[]> response.json().GraphDataByUserEveryMonth_info)
                .do(data => console.log('All graph data by user Id: ' +  JSON.stringify(data)))
                .catch(this.handleError);      
        }
    
    getTasksDataByUser(UserIdModel: UserIdModel): Observable<any> {
            console.log("user id model.."+JSON.stringify(UserIdModel));
             let body = JSON.stringify(UserIdModel);
                let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
                let options = new RequestOptions({headers: headers});
            return this.http.post(ApiSettings.API_ENDPOINT + '/rest/tasks/reqTasksDataByUser.on',body,options)
                .map((response: Response) => <ITasksData[]> response.json().TasksDataByUser_info)
                .do(data => console.log('All tasks data by user Id: ' +  JSON.stringify(data)))
                .catch(this.handleError);      
        }

        getTasksDataByUserWhereActive(UserIdModel: UserIdModel): Observable<any> {
            console.log("user id model.."+JSON.stringify(UserIdModel));
             let body = JSON.stringify(UserIdModel);
                let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
                let options = new RequestOptions({headers: headers});
            return this.http.post(ApiSettings.API_ENDPOINT + '/rest/tasks/reqTasksDataByUserWhereActive.on',body,options)
                .map((response: Response) => <ITasksData[]> response.json().TasksDataByUserWhereActive_info)
                .do(data => console.log('All tasks data by user Id: ' +  JSON.stringify(data)))
                .catch(this.handleError);      
        }

        getTrendingDataProjects(): Observable<ITrendingDataProjects[]> {
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
            return this.http.get(ApiSettings.API_ENDPOINT + '/rest/trending/reqTrendingDataProjects.on', options)
                .map((response: Response) => <ITrendingDataProjects[]> response.json().trending_data_Projects)
                .do(data => console.log('All trending data: ' +  JSON.stringify(data)))
                .catch(this.handleError);
        }
        
        getUpdatesData(CollegeCodeModel: CollegeCodeModel): Observable<any> {
        console.log("user id model.."+JSON.stringify(CollegeCodeModel));
         let body = JSON.stringify(CollegeCodeModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT + '/rest/updates/reqUpdatesData.on',body,options)
            .map((response: Response) => <IUpdatesData[]> response.json().UpdatesData_info)
            .do(data => console.log('All updates data by : ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    private _trendingcounturl = ApiSettings.API_ENDPOINT +'/rest/trending/reqTrendingUpdates.on';
    
       getTrendingCount(): Observable<ICountTrending> {
        let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
        let options = new RequestOptions({headers: headers});
            return this.http.get(this._trendingcounturl,options)
                .map((response: Response) =>  <ICountTrending> response.json().trending_updates)
                .do(data => console.log('trending Count : ' +  JSON.stringify(data)))
                .catch(this.handleError);
        }
        private _updatescounturl = ApiSettings.API_ENDPOINT +'/rest/updates/reqCollegeUpdates.on';
        
           getUpdatesCount(): Observable<ICountUpdates> {
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
                return this.http.get(this._updatescounturl,options)
                    .map((response: Response) =>  <ICountUpdates> response.json().college_updates)
                    .do(data => console.log('college Count : ' +  JSON.stringify(data)))
                    .catch(this.handleError);
            }
            private _taskcounturl = ApiSettings.API_ENDPOINT +'/rest/tasks/reqTaskUpdates.on';
            
               getTaskCount(): Observable<ICountTasks> {
                let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
                let options = new RequestOptions({headers: headers});
                    return this.http.get(this._taskcounturl,options)
                        .map((response: Response) =>  <ICountTasks> response.json().task_updates)
                        .do(data => console.log('task Count : ' +  JSON.stringify(data)))
                        .catch(this.handleError);
                }
                getMessages(UserIdModel: UserIdModel): Observable<any> {
                    console.log("user id model.."+JSON.stringify(UserIdModel));
                     let body = JSON.stringify(UserIdModel);
                        let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
                        let options = new RequestOptions({headers: headers});
                    return this.http.post(ApiSettings.API_ENDPOINT + '/rest/messages/reqMessagesByUser.on',body,options)
                        .map((response: Response) => <IMessages[]> response.json().messages_info)
                        .do(data => console.log('All messages by username: ' +  JSON.stringify(data)))
                        .catch(this.handleError);      
                }
                getMessagesToday(UserIdModel: UserIdModel): Observable<any> {
                    console.log("user id model.."+JSON.stringify(UserIdModel));
                     let body = JSON.stringify(UserIdModel);
                        let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
                        let options = new RequestOptions({headers: headers});
                    return this.http.post(ApiSettings.API_ENDPOINT + '/rest/messages/reqMessagesTodayByUser.on',body,options)
                        .map((response: Response) => <IMessages[]> response.json().messages_info_Today)
                        .do(data => console.log('All messages by username: ' +  JSON.stringify(data)))
                        .catch(this.handleError);      
                }
                getMessagesCountToday(UserIdModel: UserIdModel): Observable<any> {
                    console.log("user id model.."+JSON.stringify(UserIdModel));
                     let body = JSON.stringify(UserIdModel);
                        let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
                        let options = new RequestOptions({headers: headers});
                    return this.http.post(ApiSettings.API_ENDPOINT + '/rest/messages/reqMessagesCountByUser.on',body,options)
                        .map((response: Response) => <IMessagesCount[]> response.json().messagesCount_info)
                        .do(data => console.log('All messages by count susername: ' +  JSON.stringify(data)))
                        .catch(this.handleError);      
                }
             
                private messagedeleteurl = ApiSettings.API_ENDPOINT + '/rest/messages/reqdeleteMessagesInfo.on';
                postmessageDelete(deleteMessageModel: DeleteMessageModel) : Observable<any> {
                        console.log('delete message model'+JSON.stringify(deleteMessageModel));
                        let body = JSON.stringify(deleteMessageModel);
                        let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
                        let options = new RequestOptions({ headers: headers });
                 
                        return this.http.post(this.messagedeleteurl, body, options)
                                        .map(this.extractData)
                                        .catch(this.handleError);
            
                    }


                    getMessageById(messageIdModel:MessageIdModel): Observable<any> {
                        console.log("model.."+JSON.stringify(messageIdModel));
                         let body = JSON.stringify(messageIdModel);
                            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
                            let options = new RequestOptions({headers: headers});
                        return this.http.post(ApiSettings.API_ENDPOINT + '/rest/messages/reqMessagesById.on',body,options)
                            .map((response: Response) => <IMessages[]> response.json().messages_info_ById)
                            .do(data => console.log('All Messages by ID: ' +  JSON.stringify(data)))
                            .catch(this.handleError);      
                    }



                    private updateTasksStatus = ApiSettings.API_ENDPOINT + '/rest/tasks/reqUpdateTaskStatusInfo.on';
                    updateTasksSatus(UserIdModel: UserIdModel) : Observable<any> {
                            console.log('user id model'+JSON.stringify(UserIdModel));
                            let body = JSON.stringify(UserIdModel);
                            let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
                            let options = new RequestOptions({ headers: headers });
                     
                            return this.http.post(this.updateTasksStatus, body, options)
                                            .map(this.extractData)
                                            .catch(this.handleError);
                
                        }
                        private messageDetaildeleteurl = ApiSettings.API_ENDPOINT + '/rest/messages/reqdeleteMessageInfo.on';
                        postmessageDetailDelete(deleteMessageDetailModel: DeleteMessageDetailModel) : Observable<any> {
                                console.log('delete message Detail model'+JSON.stringify(deleteMessageDetailModel));
                                let body = JSON.stringify(deleteMessageDetailModel);
                                let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
                                let options = new RequestOptions({ headers: headers });
                         
                                return this.http.post(this.messageDetaildeleteurl, body, options)
                                                .map(this.extractData)
                                                .catch(this.handleError);
                    
                            }
}