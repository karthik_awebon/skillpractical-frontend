export interface IMessages {
    
     cid:string;
     name:string;
     attachmenturl:string;
     description:string;
     created_date:string;
     clickedCheckbox:boolean;
 
 }