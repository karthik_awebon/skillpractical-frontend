import { Component, Input } from '@angular/core'

import { ActivatedRoute } from '@angular/router'

import { UserNameModel } from '../../community/username.model';
import {UserService} from '../../community/user.service';
import { IUser } from '../../community/interfaces/userInterface';
import { HomePageService } from '../shared/homepage.services'

import { UserIdModel } from '../shared/userid.model';

import {ITrendingDataProjects} from '../shared/index'

import {Router} from '@angular/router';

@Component({
   selector: 'trending-page',
   templateUrl:'trending.component.html',
   styleUrls: ['trending.component.css']
})
export class trendingComponent {

  userIdModel = new UserIdModel(null);
 
   user: IUser;
   userid:string;
 
 
   skillpracticaltrendingdataprojects: ITrendingDataProjects[];
 
   errorMessage:string;
 
  constructor(private router:Router,private userService:UserService,private homepageservice:HomePageService){
     
  }
 
  ngOnInit() {

                                         this.homepageservice.getTrendingDataProjects()                           
                                                             .subscribe(
                                                                 (data) => {
                                                                    this.skillpracticaltrendingdataprojects = data;                                                                                       
                                                                 },
                                                                 err => console.log('error: ', err)
                                                             );
                                               console.log("trending data: "+JSON.stringify(this.skillpracticaltrendingdataprojects));
            }

 
    }



