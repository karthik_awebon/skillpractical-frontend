import { Component, OnInit } from '@angular/core';
import { IMessages } from 'app/homepage/shared/messages.interface';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { HomePageService } from 'app/homepage';
import { UserIdModel } from 'app/events';
import { MessageIdModel } from 'app/homepage/messages/shared/messageid.model';
import { DeleteMessageModel } from 'app/homepage/messages/shared/deletemessage.model';
import { DeleteMessageDetailModel } from 'app/homepage/messages/shared/deletemessagedetail.model';
@Component({
  selector: 'app-message-detail',
  templateUrl: './messages-detail.component.html',
  styleUrls: ['./messages-detail.component.css']
})
export class MessageDetailComponent implements OnInit {
  userid: string;
  errorMessage:string;
  c_id:string;
  private sub: Subscription;
  constructor (private _route: ActivatedRoute,private homepageservice:HomePageService ) {
    
}
messageIdModel= new MessageIdModel(null);
  messagesbyid:IMessages;  
  name:string;
  description:string;
  // date:string;
  attachmenturl:string;
  createddate:string;
  modelDeleteId = new DeleteMessageDetailModel(null,null);

  ngOnInit() {
    this.userid =  localStorage.getItem("userid");

    this.sub = this._route.params.subscribe(
      params => {
          let id = params['id'];
          this.c_id = id;
          console.log(this.c_id)
      });
      // this.c_id =  localStorage.getItem("c_id");
      this.messageIdModel.c_id = this.c_id;
      this.homepageservice.getMessageById(this.messageIdModel)
              .subscribe(
                  (data) => {
                      this.messagesbyid = data;
                      this.name = this.messagesbyid.name;
                      this.description = this.messagesbyid.description;
                      this.attachmenturl = this.messagesbyid.attachmenturl;
                      this.createddate = this.messagesbyid.created_date;
                      console.log("messageDetails....."+JSON.stringify(this.messagesbyid)) 
                          },
                      error => this.errorMessage = <any>error);  
      
  }
  goBack() {
    window.history.back();
  }
  deleteMessages(){
    let model=new DeleteMessageDetailModel(this.c_id,this.userid);
    this.homepageservice.postmessageDetailDelete(model)
                .subscribe(
                    data=>{
                        console.log(this.c_id)
                        this.ngOnInit();
                    },
                    err => console.log(err)
                );
  }


}