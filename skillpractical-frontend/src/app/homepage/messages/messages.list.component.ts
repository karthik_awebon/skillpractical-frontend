import { Component } from '@angular/core'
import {Router, ActivatedRoute} from '@angular/router';
import { HomePageService } from '../../homepage/shared/homepage.services';
import {UserIdModel} from '../../homepage/shared/userid.model'
import { Subscription } from 'rxjs';
import { DeleteMessageModel } from 'app/homepage/messages/shared/deletemessage.model';
import { IMessages } from 'app/homepage/shared/messages.interface';
@Component({
    selector:'messages-list',
    templateUrl:'messages.list.component.html',
      styleUrls: ['messages.list.component.css']
})
export class MessagesListComponent{
  errorMessage:string;
  c_id:number;
  userid:string;
   useridmodel = new UserIdModel('');
   allmessages:IMessages[];
   modelDeleteId = new DeleteMessageModel(null);
   private sub: Subscription;
   
   constructor (private _route: ActivatedRoute,private homepageservice:HomePageService ) {
   
      
  }
  //sorting
  key: string = 'name'; //set default
  reverse: boolean = false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }
  ngOnInit() {
    this.sub = this._route.params.subscribe(
        params => {
            let id = params['id'];
            this.c_id = id;
        });
                this.userid =  localStorage.getItem("userid");
                this.useridmodel.userid = this.userid;
                this.homepageservice.getMessages(this.useridmodel)
                        .subscribe(
                            (data) => {
                                this.allmessages = data;
                                    },
                                error => this.errorMessage = <any>error);  
                              
                                
                //  delete code
      
           
            this.modelDeleteId = new DeleteMessageModel(this.getCId())
            
                  this.homepageservice.postmessageDelete( this.modelDeleteId)
                  .subscribe(
                    (data) => {
                     // this.allmessages = data;
                    //     console.log("allmessages....."+this.allmessages.push(data))                                
                          },
                      error => this.errorMessage = <any>error);
                                
                            
                            }
             selectedItems=[];
            checkMessages(rp){
                    rp.clickedCheckbox = (rp.clickedCheckbox) ? true : false;
                    this.selectedItems = this.allmessages.filter((x) => x.clickedCheckbox)
                    console.log(this.selectedItems);
            }
            getCId(){
                    let temp=[];
                    for(var i=0;i<this.selectedItems.length;i++){
                        temp.push({"c_id":this.selectedItems[i].c_id});
                    }
                    return temp;
                }
            //to enable delete button only if checked.
                public agreed: boolean;
                setAgreed(checked) {
                    this.agreed = checked;
                }
            //code to delete selected AVPresets
            deleteMessages(){
            let model=new DeleteMessageModel(this.getCId());
            this.homepageservice.postmessageDelete(model)
                        .subscribe(
                            data=>{
                                console.log(this.getCId())
                                this.ngOnInit();
                            },
                            err => console.log(err)
                        );
            }
            
                            refresh(){
                                this.ngOnInit();
                           }
       
        //initializing p to one
        p: number = 1;
        
    }