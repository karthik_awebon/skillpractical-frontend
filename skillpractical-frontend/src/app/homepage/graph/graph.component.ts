import {Component, NgModule} from '@angular/core';
 
 import { UserNameModel } from '../../community/username.model';
 import {UserService} from '../../community/user.service';
 import { IUser } from '../../community/interfaces/userInterface';
 import { HomePageService } from '../shared/homepage.services'

 import { UserIdModel } from '../shared/userid.model';

 import {IGraphData} from '../shared/graphdata.interface'


 @Component({
   selector: 'graph-page',
   templateUrl:'graph.component.html'
 })
 export class graphComponent {

  currentusername:string;
  modelusername = new UserNameModel('');
  userIdModel = new UserIdModel(null);

  user: IUser;
  userid:string;

  usergraphdata: IGraphData;

   constructor(private userService:UserService,private homepageservice:HomePageService) {
   
   }
   community: number;
   academics:number;
   tasks:number;
   eventsandworkshops:number;
   monthname:string;
  
   
   ngOnInit() {

    

        this.userid =  localStorage.getItem("userid");
        
                      this.userIdModel = new UserIdModel(this.userid);
                      this.homepageservice.getGraphDataByUser(this.userIdModel).subscribe(
                        (usergraphdata) => {
                          this.usergraphdata = usergraphdata;
                          this.community = this.usergraphdata.community;
                          this.academics = this.usergraphdata.academics;
                          this.tasks = this.usergraphdata.tasks;
                          this.eventsandworkshops = this.usergraphdata.eventsandworkshops;
                          this.monthname = this.usergraphdata.month_name;

                          console.log(" GRAPH Tasks"+JSON.stringify(this.usergraphdata.month_name));
                                  // var community = this.usergraphdata.community;
                                  // var academics = this.usergraphdata.academics;
                                  // var tasks = this.usergraphdata.tasks;
                                  // var eventsandworkshops = this.usergraphdata.eventsandworkshops;
                                  var monthname = this.usergraphdata.month_name;

                                  // localStorage.setItem("community", JSON.stringify(community) );
                                  // localStorage.setItem("academics", JSON.stringify(academics) );
                                  // localStorage.setItem("tasks", JSON.stringify(tasks) );
                                  // localStorage.setItem("eventsandworkshops", JSON.stringify(eventsandworkshops) );  
                                  localStorage.setItem("monthname", monthname );                                                   
                             
                                  // this.barChartData(); 

                                  this.barChartData= [

                                    {data: [this.community], label: 'Community'},
                                    {data: [this.academics], label: 'Academics'},
                                    {data: [this.tasks], label: 'Tasks'},
                                    {data: [this.eventsandworkshops], label: 'Events & Workshop'}
                              
                                  ];
                                  
                                 
                                
                        // console.log("community user from graph component"+JSON.stringify(this.community));
                        console.log("community "+JSON.stringify(this.usergraphdata.community));

                        console.log("user from graph component"+JSON.stringify(this.user));
                      },
                      error => console.log(error)   
        )                 
                      
    }

  
    public barChartOptions:any = {
      scaleShowVerticalLines: false,
      responsive: true
    };

    // this is only working on reload & renavigation have to fix it with local variables instead of localstorage
     public barChartLabels:string[] = [localStorage.getItem('monthname')];
     
    public barChartType:string = 'bar';
    public barChartLegend:boolean = true;
  

    public barChartData:any = [

      {data: [this.community], label: 'Community'},
      {data: [this.academics], label: 'Academics'},
      {data: [this.tasks], label: 'Tasks'},
      {data: [this.eventsandworkshops], label: 'Events & Workshop'}

    ];
  
    // events
    public chartClicked(e:any):void {
      console.log(e);
    }
  
    public chartHovered(e:any):void {
      console.log(e);
    }
  
 }