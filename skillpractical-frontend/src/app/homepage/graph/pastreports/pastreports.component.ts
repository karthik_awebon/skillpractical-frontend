import { Component, Input } from '@angular/core'

import { ActivatedRoute } from '@angular/router'

import {IGraphData} from '../../shared/graphdata.interface'

import { UserIdModel } from '../../shared/userid.model';

import { HomePageService } from '../../shared/homepage.services'

@Component({
   selector: 'pastreports-page',
   templateUrl:'pastreports.component.html',
   styleUrls: ['pastreports.component.css']
})
export class PastReportsComponent {

  public listFilter:any;

    userIdModel = new UserIdModel(null);

    usergraphdata: IGraphData[];

     userid:string
 constructor(private homepageservice:HomePageService){
    
 }

                        ngOnInit() {             

                                         this.userid =  localStorage.getItem("userid")

                                        this.userIdModel = new UserIdModel(this.userid);
                                        this.homepageservice.getGraphDataByUserEveryMonth(this.userIdModel).subscribe(
                                          usergraphdata => {
                                            this.usergraphdata = usergraphdata;
                                        
                                    },
                                            error => console.log(error)
                                          )   
                    
                        }

}
