import { PipeTransform, Pipe } from '@angular/core'

import { IGraphData } from '../../shared/graphdata.interface'

@Pipe({
    name: 'pastReportsFilter'
})
export class ProjectFilterPipe implements PipeTransform {

         transform(value: IGraphData[], filterBy: string): IGraphData[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        
        return filterBy ? value.filter((pasreport: IGraphData) =>
            pasreport.month_name.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }

    
}