import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'task-detail',
  templateUrl: './tasksdetail.component.html',
  styleUrls: ['./tasksdetail.component.css']
})
export class TaskDetailComponent implements OnInit {

  @Input() task_id:string; 
  @Input() task_name:string;
  @Input() task_description:string; 
  @Input() task_pdf_link:string;
  @Input() duedate:string;
  @Input() status:string;

  attachment:boolean = false;

  constructor() { 

    
  }

  ngOnInit() {

  }

}