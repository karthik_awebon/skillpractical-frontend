import { Component, Input, ViewChild, ElementRef } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { UserNameModel } from '../../community/username.model';
import {UserService} from '../../community/user.service';
import { IUser } from '../../community/interfaces/userInterface';
import { HomePageService } from '../shared/homepage.services'
import { UserIdModel } from '../shared/userid.model';
import { ITasksData } from '../shared/tasksdata.interface';
@Component({
   selector: 'alltasks-page',
   templateUrl:'viewalltasks.component.html',
   styleUrls: ['viewalltasks.component.css']
})
export class AlltasksComponent {
  currentusername:string;
  modelusername = new UserNameModel('');
  userIdModel = new UserIdModel(null);
  user: IUser;
  userid:string;
  usertasksdata: ITasksData[];
//   tasksname:string;
  errorMessage:string;

   //initializing p to one
   p: number = 1;
   
 constructor(private homepageservice:HomePageService){
    
 }

 ngOnInit() {
    

    this.userid =  localStorage.getItem("userid");
    this.userIdModel = new UserIdModel(this.userid);
    this.homepageservice.getTasksDataByUser(this.userIdModel)                           
                        .subscribe(
                            (data) => {
                                this.usertasksdata = data;                                                                                        
                            },
                            err => console.log('error: ', err)
                        );
          console.log("user tasks data: "+this.usertasksdata);
  
                        
        }

                                      
 }