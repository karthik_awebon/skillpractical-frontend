import { Component, Input, ViewChild, ElementRef } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { UserNameModel } from '../../community/username.model';
import {UserService} from '../../community/user.service';
import { IUser } from '../../community/interfaces/userInterface';
import { HomePageService } from '../shared/homepage.services'
import { UserIdModel } from '../shared/userid.model';
import { ITasksData } from '../shared/tasksdata.interface';
@Component({
   selector: 'tasks-page',
   templateUrl:'tasks.component.html',
   styleUrls: ['tasks.component.css']
})
export class tasksComponent {
  currentusername:string;
  modelusername = new UserNameModel('');
  userIdModel = new UserIdModel(null);
  user: IUser;
  userid:string;
  usertasksdata: ITasksData[];
//   tasksname:string;
  errorMessage:string;
  task_id : string;
  task_description:string;
  task_pdf_link:string;
  task_name:string;
  duedate:string;
  status:string;
    constructor(private router: Router,private homepageservice:HomePageService){
        this.router.routeReuseStrategy.shouldReuseRoute = function() {
            return false;
        }
        this.ngOnInit();
        this.homepageservice.updateTasksSatus(this.userIdModel)                           
        .subscribe(
            (data) => {
                                                                                             
            },
            err => console.log('error: ', err)
        );
    }
 ngOnInit() {
    this.userid =  localStorage.getItem("userid");
    this.userIdModel = new UserIdModel(this.userid);
    
    this.homepageservice.getTasksDataByUserWhereActive(this.userIdModel)                           
                        .subscribe(
                            (data) => {
                                this.usertasksdata = data;  
                                // this.homepageservice.updateTasksSatus(this.userIdModel)                           
                                // .subscribe(
                                //     (data) => {
                                                                                                                     
                                //     },
                                //     err => console.log('error: ', err)
                                // );
                            },
                            err => console.log('error: ', err)
                        );
          console.log("user tasks data: "+this.usertasksdata);
          
        }
        
        clickViewDetail(task_id,task_name,task_description,task_pdf_link,duedate,status){
            this.task_id = task_id;
            this.task_description = task_description;
            this.task_pdf_link = task_pdf_link;
            this.task_name= task_name;
            this.duedate = duedate;
            this.status = status;
         console.log("task id:"+this.task_id)
        }
                                      
 }
 