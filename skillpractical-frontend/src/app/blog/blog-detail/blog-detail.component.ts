import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { BlogposttitleModel } from 'app/blog/blog-detail/blogposttitle.model';
import { BlogPostService } from 'app/blog/blogpost.service';
import { IBlogpost } from 'app/blog/blog-detail/blogpost.interface';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {

  private sub: Subscription;
  modelblogposttitle = new BlogposttitleModel('');
  constructor(private route: ActivatedRoute,private blogPostService : BlogPostService) { }
  
  blogpostdetail: IBlogpost;

  blogpost_title :string;
  blogpost_image:string;
  blogpost_description:string;
  blogpost_author:string;
  blogpost_date:string;
  blogpost_author_image:string;
  blogpost_author_designation:string;

  header:any;

  ngOnInit() {

    this.sub = this.route.params.subscribe(
      params => {
          let blogpost_title = params['blogpost_title']
          this.blogpost_title = blogpost_title
          console.log("blogpost_title.."+this.blogpost_title);
        }) 

          this.modelblogposttitle = new BlogposttitleModel(this.blogpost_title);
            this.blogPostService.getBlogPostByTitle(this.modelblogposttitle)
                            .subscribe(
                            (data) => {
                                this.blogpostdetail = data; 
                                this.blogpost_author = this.blogpostdetail.blogpost_author;
                                this.blogpost_image = this.blogpostdetail.blogpost_image;
                                this.blogpost_description = this.blogpostdetail.blogpost_description;
                                this.blogpost_title = this.blogpostdetail.blogpost_title;
                                this.blogpost_date = this.blogpostdetail.blogpost_date;
                                // this.blogpostdetail = data.ticker;
                                this.blogpost_author_image = this.blogpostdetail.blogpost_author_image;
                                this.blogpost_author_designation = this.blogpostdetail.blogpost_author_designation;

    });  
  }

  goBack() {
    window.history.back();
  }
}
