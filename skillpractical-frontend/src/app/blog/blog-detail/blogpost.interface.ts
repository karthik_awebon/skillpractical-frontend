export interface IBlogpost {
    blogpost_title:string;
    blogpost_description: string;
    blogpost_image : string;
    blogpost_date :string;
    blogpost_author:string;
    blogpost_id : string;
    blogpost_author_image:string;
    blogpost_author_designation:string;
  }