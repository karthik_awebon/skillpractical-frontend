import { Component, OnInit, Input } from '@angular/core';
import { BlogPostService } from 'app/blog/blogpost.service';
import { IBlogpost } from 'app/blog/blog-detail/blogpost.interface';
import { BlogposttitleModel } from 'app/blog/blog-detail/blogposttitle.model';
import { BlogpostidModel } from './blogpostid.model';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

    modelblogposttitle = new BlogposttitleModel('');
    modelblogpostid = new BlogpostidModel('');
    blogpost_title : string;
  errorMessage: string;
  blogpost_id : string;
  blogpost : IBlogpost[];
  blogpostimage : IBlogpost[];
  blogpost_title0 : string;
  blogpost_image0: string;
  blogpost_author0: string;
  blogpost_date0 : string;
  blogpost_description0 : string;
  blogpost_title1 : string;
  blogpost_image1: string;
  blogpost_author1: string;
  blogpost_date1 : string;
  blogpost_description1 : string;
  blogpost_title2 : string;
  blogpost_image2: string;
  blogpost_author2: string;
  blogpost_date2 : string;
  blogpost_description2 : string;
  blogpost_title3 : string;
  blogpost_image3: string;
  blogpost_author3: string;
  blogpost_date3 : string;
  blogpost_description3 : string;
  blogpost_title4 : string;
  blogpost_image4: string;
  blogpost_author4: string;
  blogpost_date4 : string;
  blogpost_description4 : string;
  // blogpost_title5 : string;
    //   blogpost_image5: string;
    //   blogpost_author5: string;
    //   blogpost_date5 : string;
    //   blogpost_description5 : string;
  // blogpost_title6 : string;
   //   blogpost_image6: string;
    //   blogpost_author6: string;
    //   blogpost_date6 : string;
    //   blogpost_description6 : string;
  constructor(private blogPostService: BlogPostService) { }
  
  ngOnInit() {
    this.modelblogposttitle = new BlogposttitleModel(this.blogpost_title);   
         
    this.blogPostService.getBlogs()
          .subscribe(
              (data) => {
              this.blogpost = data;
              this.blogpost_title0 = this.blogpost[0].blogpost_title;
              this.blogpost_image0 = this.blogpost[0].blogpost_image;
              this.blogpost_author0 = this.blogpost[0].blogpost_author;
              this.blogpost_date0 = this.blogpost[0].blogpost_date;
              this.blogpost_description0 = this.blogpost[0].blogpost_description;
              //1
              this.blogpost_title1 = this.blogpost[1].blogpost_title;
              this.blogpost_image1 = this.blogpost[1].blogpost_image;
              this.blogpost_author1 = this.blogpost[1].blogpost_author;
              this.blogpost_date1 = this.blogpost[1].blogpost_date;
              this.blogpost_description1 = this.blogpost[1].blogpost_description;
              //2
              this.blogpost_title2 = this.blogpost[2].blogpost_title;
              this.blogpost_image2 = this.blogpost[2].blogpost_image;
              this.blogpost_author2 = this.blogpost[2].blogpost_author;
              this.blogpost_date2 = this.blogpost[2].blogpost_date;
              this.blogpost_description2 = this.blogpost[2].blogpost_description;
              //3
              this.blogpost_title3 = this.blogpost[3].blogpost_title;
              this.blogpost_image3 = this.blogpost[3].blogpost_image;
              this.blogpost_author3 = this.blogpost[3].blogpost_author;
              this.blogpost_date3 = this.blogpost[3].blogpost_date;
              this.blogpost_description3 = this.blogpost[3].blogpost_description;
              //4
              this.blogpost_title4 = this.blogpost[4].blogpost_title;
              this.blogpost_image4 = this.blogpost[4].blogpost_image;
              this.blogpost_author4 = this.blogpost[4].blogpost_author;
              this.blogpost_date4 = this.blogpost[4].blogpost_date;
              this.blogpost_description4 = this.blogpost[4].blogpost_description;
              //5
            //   this.blogpost_title5 = this.blogpost[5].blogpost_title;
            //   this.blogpost_image5 = this.blogpost[5].blogpost_image;
            //   this.blogpost_author5 = this.blogpost[5].blogpost_author;
            //   this.blogpost_date5 = this.blogpost[5].blogpost_date;
            //   this.blogpost_description5 = this.blogpost[5].blogpost_description;
              //6
            // this.blogpost_title6 = this.blogpost[6].blogpost_title;
            //   this.blogpost_image6 = this.blogpost[6].blogpost_image;
            //   this.blogpost_author6 = this.blogpost[6].blogpost_author;
            //   this.blogpost_date6 = this.blogpost[6].blogpost_date;
            //   this.blogpost_description6 = this.blogpost[6].blogpost_description;
              console.log(" All Blogs....."+JSON.stringify(this.blogpost))                  
              },
          error => this.errorMessage = <any>error) 


      this.blogPostService.getTrendingBlogPosts()
          .subscribe(
              (data) => 
              {
                this.blogpostimage = data;
              
                
                console.log(" All Blogs For Slider....."+JSON.stringify(this.blogpostimage))                  
              },
          error => this.errorMessage = <any>error)
      }


  updateview(){
    console.log("blogspost by id"+this.blogpost_id);
    
    this.modelblogpostid = new BlogpostidModel(this.blogpost_id);
    this.blogPostService.postBlogPostByid(this.modelblogpostid)
                            .subscribe(
                            (data) => {
                                this.blogpost = data; 
    });  
  }
}
