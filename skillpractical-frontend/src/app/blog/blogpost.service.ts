import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import {Router} from '@angular/router';
import { ApiSettings } from "app/ApiSettings";
import { BlogposttitleModel } from "app/blog/blog-detail/blogposttitle.model";
import { IBlogpost } from 'app/blog/blog-detail/blogpost.interface';
import { Observable } from 'rxjs/Observable';
import { BlogpostidModel } from 'app/blog/blog/blogpostid.model';

@Injectable()
export class BlogPostService {

  constructor (private http:Http,private router:Router,private _http: Http) {

  }
 
 
  private extractData(res: Response) {
      let body = res.json();
      return body.fields || { };
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
}
 
  private _blogUrl = ApiSettings.API_ENDPOINT+'/blogs/blog/getAllBlogList.on';

   getBlogs(): Observable<IBlogpost[]> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
        return this.http.get(this._blogUrl,options)
            .map((response: Response) => <IBlogpost[]> response.json().allblogposts_list)
            .do(data => console.log('All Blogs: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }


  private url = ApiSettings.API_ENDPOINT + '/blogs/blog/getBlogDetailsByBlogTitle.on';

  getBlogPostByTitle(blogposttitleModel:BlogposttitleModel): Observable<any> {
    console.log("blogposttitleModel.."+JSON.stringify(blogposttitleModel));
     let body = JSON.stringify(blogposttitleModel);
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
    return this._http.post(this.url,body,options)
        .map((response: Response) => <IBlogpost> response.json().BlogDetailsByBlogTitle)
        .do(data => console.log('BlogPost By Title: ' +  JSON.stringify(data)))
        .catch(this.handleError);      
  }


   private blogpostidurl = ApiSettings.API_ENDPOINT + '/blogs/blog/updateBlogPostViews.on';

    postBlogPostByid(blogpostidModel:BlogpostidModel): Observable<any> {
    console.log("blogpostidModel.."+JSON.stringify(blogpostidModel));
     let body = JSON.stringify(blogpostidModel);
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
    return this._http.post(this.blogpostidurl,body,options)
        .map((response: Response) => <IBlogpost[]> response.json())
        .do(data => console.log('BlogPost By Id: ' +  JSON.stringify(data)))
        .catch(this.handleError);      
}

   private _blogsliderUrl = ApiSettings.API_ENDPOINT+'/blogs/blog/getAllBlogListBasedOnViews.on';

    getTrendingBlogPosts(): Observable<IBlogpost[]> {
        console.log("Blogs model ..");          
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this._http.get(this._blogsliderUrl,options)
            .map((response: Response) => <IBlogpost[]> response.json().allblogposts_list_trending)
            .do(data => console.log('All Blogs For Slider: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }
}