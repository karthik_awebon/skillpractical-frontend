import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SettingsService } from 'app/user/services/setting.service';
import { EducationModel } from 'app/user/shared/education.model';
import { UserNameModel } from 'app/user/shared/username.model';
import { IUser } from 'app/community/interfaces/userInterface';
import { UserService } from 'app/community/user.service';
import { ToastrService, ToastrConfig } from 'toastr-ng2';
import { SettingUserComponent } from 'app/user/setting.component';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {

  noChangeYet:boolean = true;
  currentusername:string;
  userid:string;
  model = new EducationModel('','','','','','','');
  modelusername = new UserNameModel('');  
  user:IUser;

  filledstatustyleeducation:any;
  settingvalueeducation:number;

  constructor(private settingsService:SettingsService,private userService: UserService,private toastrService: ToastrService,private toastrConfig: ToastrConfig,
    private settingUserComponent:SettingUserComponent) { 
    toastrConfig.timeOut = 2000;

  }
 

  ngOnInit() {
    this.currentusername = localStorage.getItem("currentUserName");
    this.userid = localStorage.getItem("userid");
    
        this.modelusername = new UserNameModel(this.currentusername);                         
        this.userService.getUserByName(this.modelusername).subscribe(
                          user => {
                            this.user = user; 
                            this.model.rollnumber = this.user.rollnumber;
                            this.model.collegename = this.user.collegename;
                            this.model.collegecode = this.user.collegecode;
                            this.model.fieldofstudy = this.user.fieldofstudy;
                            this.model.year = this.user.year;
                            this.model.branch = this.user.branch

                            if(this.user.rollnumber == "null" || this.user.rollnumber == "" || this.user.rollnumber == null|| this.user.collegename == "null" || this.user.collegename == "" || this.user.collegename == null|| this.user.collegecode == "null" || this.user.collegecode == "" || this.user.collegecode == null||
                            this.user.fieldofstudy == "null" || this.user.fieldofstudy == "" || this.user.fieldofstudy == null|| this.user.year == "null" || this.user.year == "" || this.user.year == null|| this.user.branch == "null" || this.user.branch == "" || this.user.branch == null
                            
                          ){
                                this.settingvalueeducation = 0;
                                this.filledstatustyleeducation = {
                                'color': 'red'
                                  }
                                  localStorage.setItem("settingvalueeducation", JSON.stringify(this.settingvalueeducation)); 
                            } else {
                              this.settingvalueeducation = 25;
                              this.filledstatustyleeducation = {
                                'color': 'green'
                                }
                                localStorage.setItem("settingvalueeducation", JSON.stringify(this.settingvalueeducation)); 
                            }

                              
                                    
                              console.log("user from settings in education"+JSON.stringify(this.user));
                            },
                            error => console.log(error)
            )

  }


  isValid:boolean = true;

  isValidForm() {
     return this.isValid;
 }
 
  submitForm(form: NgForm) {

    this.isValid = false
    this.model.userid = this.userid;
      this.settingsService.postUpdateUserEducationForm(this.model)  
     
          .subscribe(
            data => {  
              this.toastrService.success('Your Education Details are Successfully Updated! ');
              // form.reset();
              this.isValid = true;

              this.ngOnInit();

              setTimeout(() => {
                this.settingUserComponent.ngOnInit();
            },1000)

            },
            err => console.log('error: ', err),
          );
          console.log(this.model);
     }
     changeVal(){ // triggers on change of any field(s)
      console.log("change");
      this.noChangeYet = false;
    }
}
