import { Component, OnInit, ViewChild } from '@angular/core';
import { SettingsService } from 'app/user/services/setting.service';
import { GeneralModel } from 'app/user/shared/general.model';
import { NgForm } from '@angular/forms';
import { UserNameModel } from 'app/user/shared/username.model';
import { IUser } from 'app/user/shared/userinterface';
import { UserService } from 'app/community/user.service';
import { ToastrService, ToastrConfig } from 'toastr-ng2';
import { SettingUserComponent } from '../setting.component';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {
  @ViewChild('form') myForm: NgForm;
  noChangeYet:boolean = true;
  currentusername:string;

  modelusername = new UserNameModel('');

  model = new GeneralModel('','','','','','','','','',true);

  userid:string;

  user:IUser;
  fillstatus :boolean;

  filledstatustylegeneral:any;
  settingvaluegeneral:number;

  constructor(private settingsService:SettingsService,private userService: UserService,private toastrService: ToastrService,private toastrConfig: ToastrConfig,
  private settingUserComponent:SettingUserComponent) { 
    toastrConfig.timeOut = 2000;
  
  }

  ngOnInit() {

    this.currentusername = localStorage.getItem("currentUserName");
    this.userid = localStorage.getItem("userid");
    
        this.modelusername = new UserNameModel(this.currentusername);                         
        this.userService.getUserByName(this.modelusername).subscribe(
                          user => {
                            this.user = user; 
                            this.model.firstname = this.user.firstname;
                            this.model.lastname = this.user.lastname;
                            this.model.emailid = this.user.emailid;
                            this.model.phonenumber = this.user.phonenumber;   
                            this.model.username = this.user.username; 
                            this.model.userid = this.user.userid; 
                            this.model.address = this.user.address;
                            this.model.state = this.user.state;
                            this.model.district = this.user.district;
                           
              if(this.user.firstname == "null" || this.user.firstname == "" || this.user.firstname == null|| this.user.lastname == "null" || this.user.lastname == "" || this.user.lastname == null|| this.user.phonenumber == "null" || this.user.phonenumber == "" || this.user.phonenumber == null||
                this.user.address == "null" || this.user.address == "" || this.user.address == null|| this.user.state == "null" || this.user.state == "" || this.user.state == null|| this.user.district == "null" || this.user.district == "" || this.user.district == null
              ){
                    this.settingvaluegeneral = 0;

                    this.filledstatustylegeneral = {
                    'color': 'red'
                      }

                      localStorage.setItem("settingvaluegeneral", JSON.stringify(this.settingvaluegeneral)); 

                } else {
                  this.filledstatustylegeneral = {
                    'color': 'green'
                    }
                    this.settingvaluegeneral = 25;

                    localStorage.setItem("settingvaluegeneral", JSON.stringify(this.settingvaluegeneral)); 
                }

                       

                console.log("user from settings in general"+JSON.stringify(this.user));
                            },
                            error => console.log(error)
            )

    this.myForm.valueChanges.subscribe((value: any) => {
      console.log("One of the inputs has changed");
 });

 }
  isValid:boolean = true;

  isValidForm() {
     return this.isValid;
 }
 
  submitForm(form: NgForm) {

    this.isValid = false;
    this.model.userid = this.userid;
    this.settingsService.postUpdateUsergeneralForm(this.model)  
        .subscribe(
          data => {  
            this.toastrService.success('Your General Settings is Successfully Updated! ');
            // form.reset();
            this.isValid = true;
            this.ngOnInit();

          setTimeout(() => {
            this.settingUserComponent.ngOnInit();
          },1000)

          },
          err => console.log('error: ', err),
        );
        console.log(this.model);
        }
  
   changeVal(){ // triggers on change of any field(s)
    console.log("change");
    this.noChangeYet = false;
   }

}
