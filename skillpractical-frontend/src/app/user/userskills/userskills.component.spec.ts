import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserskillsComponent } from './userskills.component';

describe('UserskillsComponent', () => {
  let component: UserskillsComponent;
  let fixture: ComponentFixture<UserskillsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserskillsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserskillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
