import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'app/user/services/setting.service';
import { NgForm } from '@angular/forms';

import { UserSkillsModel } from 'app/user/shared/userskills.model';
import { UserIdModel } from 'app/community/userid.model';
import { UserService } from 'app/community/user.service';
import { IUserSkills } from 'app/community/interfaces/userInterface';
import { ToastrService, ToastrConfig } from 'toastr-ng2';
import { SettingUserComponent } from '../setting.component';

@Component({
  selector: 'app-userskills',
  templateUrl: './userskills.component.html',
  styleUrls: ['./userskills.component.css']
})
export class UserskillsComponent implements OnInit {
  
  noChangeYet:boolean = true;
  model = new UserSkillsModel('','','','','','','');
  userskills : IUserSkills;
  userid:string;

  modeluserid = new UserIdModel('')

  settingvalueuserskills:number;
  filledstatustyleuserskills:any;


  constructor(private settingsService:SettingsService,private userService: UserService,private toastrService: ToastrService,private toastrConfig: ToastrConfig
  ,private settingUserComponent:SettingUserComponent) {
    toastrConfig.timeOut = 2000;
  
   }

  
  ngOnInit() {
    this.userid = localStorage.getItem("userid");
    this.modeluserid.userid = this.userid;
     this.userService.getSkillByUserId(this.modeluserid).subscribe(
       userskills => {
                         this.userskills = userskills; 
                         this.model.skill1 =  this.userskills.skill1;
                         this.model.skill2 = this.userskills.skill2;
                         this.model.skill3 = this.userskills.skill3;
                         this.model.rateyourself1 = this.userskills.rateyourself1;
                         this.model.rateyourself2 = this.userskills.rateyourself2;
                         this.model.rateyourself3 = this.userskills.rateyourself3;

                         if(this.userskills.skill1 == "null" || this.userskills.skill1 == "" || this.userskills.skill1 == null || this.userskills.skill2 == "null" || this.userskills.skill2 == "" 
                         || this.userskills.skill2 == null || this.userskills.skill3 == "null" || this.userskills.skill3 == "" || this.userskills.skill3 == null ||  this.userskills.rateyourself1 == "null" || this.userskills.rateyourself1 == "" || this.userskills.rateyourself1 == null
                         || this.userskills.rateyourself2 == "null" || this.userskills.rateyourself2 == "" || this.userskills.rateyourself2 == null || this.userskills.rateyourself3 == "null" || this.userskills.rateyourself3 == "" || this.userskills.rateyourself3 == null
                       ){
                             this.settingvalueuserskills = 0;
                             this.filledstatustyleuserskills = {
                             'color': 'red'
                               }
                               localStorage.setItem("settingvalueuserskills", JSON.stringify(this.settingvalueuserskills));
                         } else {
                           this.settingvalueuserskills = 25;
                           this.filledstatustyleuserskills = {
                             'color': 'green'
                             }
                             localStorage.setItem("settingvalueuserskills", JSON.stringify(this.settingvalueuserskills));
                         } 
                         
                           console.log("userskills by userid"+JSON.stringify(this.userskills));
                         },
                         error => console.log(error)
         )
  }

  isValid:boolean = true;

  isValidForm() {
     return this.isValid;
 }
 

  submitForm(form: NgForm) {
    this.isValid = false;
    this.model.userid = this.userid;
    this.settingsService.postUpdateUserskillsForm(this.model)  
   
        .subscribe(
          data => {  
            this.toastrService.success('Your Skills are Successfully Updated! ');
            // form.reset();
            this.isValid = true;

            this.ngOnInit();

            setTimeout(() => {
              this.settingUserComponent.ngOnInit();
            },1000)


          },
          err => console.log('error: ', err),
        );
        console.log(this.model);
   }
   changeVal(){ // triggers on change of any field(s)
    console.log("change");
    this.noChangeYet = false;
  }
   
}
