import {Injectable} from "@angular/core";
import { UserModel } from '../user.model'
import {UserRegisterModel} from '../userregister.model'

import {Observable} from 'rxjs/Observable';
import { Http, Response, Headers, RequestOptions } from '@angular/http'


import { ApiSettings } from '../../ApiSettings';

import {CheckUserForRegistrationModel} from '../shared/checkUserForRegistration.model';
import {IUserId} from '../shared/userid.interface';

import {GlobalErrorHandler} from '../../common/error-handler';
import {IResultMsg} from '../shared/result_message.interface';

@Injectable()
export class RegisterService {
  constructor (private http:Http,private globalErrorHandler:GlobalErrorHandler) {}

  private extractData(res: Response) {
        let body = res.json();
        return body.fields || { };
    }
  
  private handleError(error: Response) {
    //    this.globalErrorHandler.handleError(error);
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

//   this.globalErrorHandler.handleError();

    private registeruserurl = ApiSettings.API_ENDPOINT + '/users/user/newuserRegistrationemailsending.on';
    postRegisterUserForm(userRegisterModel: UserRegisterModel) : Observable<any> {
        console.log("model check for  user registration.."+JSON.stringify(userRegisterModel));
        let body = JSON.stringify(userRegisterModel);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.registeruserurl, body, options)
            .map((response: Response) => <IResultMsg[]> response.json().result_msg)
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);    
    
    }

   private checkUserForRegistrationurl = ApiSettings.API_ENDPOINT + '/users/user/checkForrollNumberAndDobForRegistration.on';
   checkUserForRegistration(checkUserForRegistrationModel:CheckUserForRegistrationModel): Observable<any> {
      console.log("model check user for registration.."+JSON.stringify(checkUserForRegistrationModel));
      let body = JSON.stringify(checkUserForRegistrationModel);
          let headers = new Headers({'Content-Type': 'application/json'});
          let options = new RequestOptions({headers: headers});
      return this.http.post(this.checkUserForRegistrationurl,body,options)
          .map((response: Response) => <IUserId[]> response.json().checkForrollNumberAndDobForRegistration_info)
          .do(data => console.log('All: ' +  JSON.stringify(data)))
          .catch(this.handleError);      
    }

}