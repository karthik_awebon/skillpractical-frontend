import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import { ApiSettings } from '../../ApiSettings';
import {IUser} from '../shared/userinterface';
import {UserNameModel} from '../shared/username.model';
import { UserLoginModel } from '../shared/userlogin.model';
import { IToken } from '../shared/token.interface';
import {Router} from '@angular/router';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ToastrService } from 'toastr-ng2';
import { ToastrConfig } from 'toastr-ng2';

@Injectable()
export class LoginService {
  token: string;

  constructor (toastrConfig: ToastrConfig,private toastrService: ToastrService,private http:Http,private router:Router) {
    toastrConfig.timeOut = 1000;
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
  
  sendCredentials(UserLoginModel:UserLoginModel): Observable<any> {
    console.log("model.."+JSON.stringify(UserLoginModel));
     let body = JSON.stringify(UserLoginModel);
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
    return this.http.post(ApiSettings.API_ENDPOINT + '/users/user/reqloginInfo.on',body,options)
        .map((response: Response) => <IToken[]> response.json())
        .do(data => console.log('Token: ' +  JSON.stringify(data)))
        .catch(this.handleError);      
}


  sendToken(token) {
    let userUrl = ApiSettings.API_ENDPOINT + '/rest/user/requsersInfo.on';
    let headers2 = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '+token});

    return this.http.get(userUrl, {headers: headers2});
  }


  getUserByName(userNameModel: UserNameModel): Observable<any> {
    console.log("user name model.."+JSON.stringify(userNameModel));
     let body = JSON.stringify(userNameModel);
        let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
        let options = new RequestOptions({headers: headers});
    return this.http.post(ApiSettings.API_ENDPOINT +'/rest/user/reqUserByUserName.on',body,options)
        .map((response: Response) => <IUser> response.json().userByUserName_info)
        .do(data => console.log('user Info by user name: ' +  JSON.stringify(data)))
        .catch(this.handleError);      
}

  // checkSession(token) {
  //   let userUrl = ApiSettings.API_ENDPOINT + '/rest/user/requsersInfo.on';
  //   let headers2 = new Headers({'Authorization': 'Bearer '+token});

  //   return this.http.get(userUrl, {headers: headers2});
  // }

  checkLogin() {
    if (localStorage.getItem("currentUserName")!="" && localStorage.getItem("token")!="") {
      return true;
    } else {
      return false;
    }
  }


  logout() {
    
    localStorage.setItem("token", "");
    localStorage.setItem("currentUserName", "");
    localStorage.setItem("userid", "");
    localStorage.setItem("userid", "");

    // reset profile setting localstorage
    localStorage.setItem("settingvaluegeneral",""); 
    localStorage.setItem("settingvalueresumelinks","");
    localStorage.setItem("settingvalueuserskills",""); 
    localStorage.setItem("settingvalueeducation","");   

    alert("You just logged out.");

    location.reload();

     //clean home page
    //  localStorage.setItem("community", "" );
    //  localStorage.setItem("academics", "" );
    //  localStorage.setItem("tasks", "" );
    //  localStorage.setItem("eventsandworkshops", "" );
     localStorage.setItem("monthname", "");

     this.toastrService.success('Logout Successful!');

     this.router.navigateByUrl('/');
  }

}
