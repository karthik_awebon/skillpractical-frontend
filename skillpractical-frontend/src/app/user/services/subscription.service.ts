import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';

import { ApiSettings } from '../../ApiSettings';

import {Router} from '@angular/router';
import { InstaMojoModel } from 'app/user/shared/instamojo.model';
import { IResultMsgUpgrade } from 'app/user/shared/resultmessageupgrade.interface';
import { UserNameModel } from 'app/user/shared/username.model';
import { ISubscriptionandBilling } from 'app/user/shared/subscriptionandbilling.interface';
import { UserIdModel } from 'app/community/userid.model';


@Injectable()
export class SubscriptionService {

  constructor (private http:Http,private router:Router) {}


  private extractData(res: Response) {
    let body = res.json();
    return body.fields || { };
  }


  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  getsubscriptionbilling(userIdModel: UserIdModel) : Observable<any> {
    console.log("model subscription"+JSON.stringify(userIdModel));
      let body = JSON.stringify(userIdModel);
      let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
      let options = new RequestOptions({headers: headers});

      return this.http.post(ApiSettings.API_ENDPOINT + '/rest/user/reqSelectSubscriptionDetailsByUserid.on', body, options)
                      .map((response: Response) => <ISubscriptionandBilling> response.json().subscription_details )
                      .do(data => console.log('All Subscription and billing: ' +  JSON.stringify(data)))
                      .catch(this.handleError); 

}

}