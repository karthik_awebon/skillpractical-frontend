import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';

import { ApiSettings } from '../../ApiSettings';

import {Router} from '@angular/router';
import { InstaMojoModel } from 'app/user/shared/instamojo.model';
import { IResultMsgUpgrade } from 'app/user/shared/resultmessageupgrade.interface';
import { UserIdModel } from 'app/community/userid.model';
import { IUpgradeStatus } from 'app/user/shared/upgradestatus.interface';


@Injectable()
export class UpgradeService {

  constructor (private http:Http,private router:Router) {}


  private extractData(res: Response) {
    let body = res.json();
    return body.fields || { };
  }


  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }

  upgradeUser(InstaMojoModel: InstaMojoModel) : Observable<any> {
    console.log("model upgrade"+JSON.stringify(InstaMojoModel));
      let body = JSON.stringify(InstaMojoModel);
      let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
      let options = new RequestOptions({headers: headers});

      return this.http.post(ApiSettings.API_ENDPOINT + '/rest/user/reqUpgradeUser.on', body, options)
                      .map((response: Response) => <IResultMsgUpgrade[]> response.json().payment_url)
                      .do(data => console.log('All upgrade response: ' +  JSON.stringify(data)))
                      .catch(this.handleError); 

}
selectUserSubscriptionStatus(userIdModel: UserIdModel) : Observable<any> {
  console.log("model upgrade status"+JSON.stringify(userIdModel));
    let body = JSON.stringify(userIdModel);
    let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
    let options = new RequestOptions({headers: headers});

    return this.http.post(ApiSettings.API_ENDPOINT + '/rest/user/reqSelectSubscriptionStatusByUserid.on', body, options)
                    .map((response: Response) => <IUpgradeStatus> response.json().subscription_status)
                    .do(data => console.log('All upgrade response status: ' +  JSON.stringify(data)))
                    .catch(this.handleError); 

}


}