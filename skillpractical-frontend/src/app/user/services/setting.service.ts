import {Injectable} from "@angular/core";
import { UserModel } from '../user.model'
import {Observable} from 'rxjs/Observable';
import { Http, Response, Headers, RequestOptions } from '@angular/http'


import { ApiSettings } from '../../ApiSettings';
import { UpdatePasswordModel } from "app/user/shared/updatepassword.model";
import { GeneralModel } from "app/user/shared/general.model";
import { EducationModel } from "app/user/shared/education.model";
import { ResumeLinksModel } from "app/user/shared/resumelinks.model";
import { UserSkillsModel } from "app/user/shared/userskills.model";

@Injectable()
export class SettingsService {
  constructor (private http:Http) {}

  private extractData(res: Response) {
        let body = res.json();
        return body.fields || { };
    }
  
  private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    private updateUsergeneralurl = ApiSettings.API_ENDPOINT + '/rest/user/reqUpdateUser.on';
    postUpdateUsergeneralForm(generalModel: GeneralModel) : Observable<any> {
        let body = JSON.stringify(generalModel);
        let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.updateUsergeneralurl, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
    
  }

  private updateUsereducationurl = ApiSettings.API_ENDPOINT + '/rest/user/reqUpdateUserEducation.on';
  postUpdateUserEducationForm(educationModel: EducationModel) : Observable<any> {
      let body = JSON.stringify(educationModel);
      let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
      let options = new RequestOptions({ headers: headers });

      return this.http.post(this.updateUsereducationurl, body, options)
                      .map(this.extractData)
                      .catch(this.handleError);
  
}
private updateUserresumelinksurl = ApiSettings.API_ENDPOINT + '/rest/user/reqUpdateUserResumeLinks.on';
postUpdateUserResumeLinksForm(resumeLinksModel: ResumeLinksModel) : Observable<any> {
    console.log("user resume liks model"+resumeLinksModel)
    let body = JSON.stringify(resumeLinksModel);
    let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.updateUserresumelinksurl, body, options)
                    .map(this.extractData)
                    .catch(this.handleError);

}
private updateUserSkillsurl = ApiSettings.API_ENDPOINT + '/rest/user/reqUpdateUserSkills.on';
postUpdateUserskillsForm(userSkillsModel: UserSkillsModel) : Observable<any> {
      let body = JSON.stringify(userSkillsModel);
      let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
      let options = new RequestOptions({ headers: headers });

      return this.http.post(this.updateUserSkillsurl, body, options)
                      .map(this.extractData)
                      .catch(this.handleError);
  
}
    private updateUserPasswordurl = ApiSettings.API_ENDPOINT + '/rest/user/reqUpdateUserPassword.on';
    postUpdateUserPassword(SettingModel: UpdatePasswordModel) : Observable<any> {
      let body = JSON.stringify(SettingModel);
      let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
      let options = new RequestOptions({ headers: headers });

      return this.http.post(this.updateUserPasswordurl, body, options)
                      .map(this.extractData)
                      .catch(this.handleError);

    }


}