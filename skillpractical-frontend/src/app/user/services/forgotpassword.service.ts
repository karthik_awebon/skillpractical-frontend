import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';

import {ForgotPasswordModel} from '../shared/forgotpassword.model';

import { ApiSettings } from '../../ApiSettings';

import {Router} from '@angular/router';
import {IResultMsg} from '../shared/result_message.interface'

@Injectable()
export class ForgotPasswordService {

  constructor (private http:Http,private router:Router) {}


  private extractData(res: Response) {
    let body = res.json();
    return body.fields || { };
  }


  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
  
  private postforgotpasswordurl = ApiSettings.API_ENDPOINT + '/users/user/forgotPasswordEmailSending.on';
  forgotPassword(forgotPasswordModel: ForgotPasswordModel) : Observable<any> {
      console.log("Forgot password model"+JSON.stringify(forgotPasswordModel));
     let body = JSON.stringify(forgotPasswordModel);
     let headers = new Headers({ 'Content-Type': 'application/json'});
     let options = new RequestOptions({ headers: headers });

     return this.http.post(this.postforgotpasswordurl, body, options)
        .map((response: Response) => <IResultMsg[]> response.json().result_msg)
        .do(data => console.log('All: ' +  JSON.stringify(data)))
        .catch(this.handleError);  
 }

}
