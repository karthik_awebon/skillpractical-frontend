import { Component, OnInit } from '@angular/core';
import { IResultMsgUpgrade } from 'app/user/shared/resultmessageupgrade.interface';
import { InstaMojoModel } from 'app/user/shared/instamojo.model';
import { UpgradeService } from 'app/user/services/upgrade.service';
import {  ActivatedRoute, Router } from '@angular/router';
import { Subscription }       from 'rxjs/Subscription';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
  subscription_type:string;
  pricing:string;
 
  // b:number;
  c:string;
  private sub: Subscription;
 paymenturl: string;
  constructor(private _route: ActivatedRoute, private _router: Router, private upgradeService: UpgradeService) { }
  ngOnInit() {
    this.sub = this._route.params.subscribe(
      params => {
        let subscription_type = params['subscription_type'];
        let pricing = params['pricing']
          this.subscription_type = subscription_type;
          this.pricing =  pricing;
});  
   this.c = this.pricing
  //  +this.b;  
  }

restultmessageupgrade:IResultMsgUpgrade;
 
model = new InstaMojoModel('','','','','','','','')
isValidForm() {
    return this.isValid;
}
isValid:boolean=true;
checkoutclicked:boolean;

     Upgrade(form: NgForm) {
      this.isValid = false;
      this.checkoutclicked = true;
      this.model.amount = this.pricing;
        this.model.description = this.subscription_type;
        this.model.userid  = localStorage.getItem("userid");
        this.model.username = localStorage.getItem("currentUserName");
      this.upgradeService.upgradeUser(this.model)  
          .subscribe(
            data => {  
              this.restultmessageupgrade = data;
             this.paymenturl = this.restultmessageupgrade.paymenturl;
           window.location.href=this.paymenturl;
           this.isValid = true;
           this.checkoutclicked = false;
              this.isValid = true;
            },
            err => console.log('error: ', err),
          );
          console.log(this.model);
     }
     
}