import { Component, Input } from '@angular/core'
import { UpgradeService } from '../services/upgrade.service';
import { InstaMojoModel } from '../shared/instamojo.model';
import { IResultMsgUpgrade } from '../shared/resultmessageupgrade.interface';

@Component({
   selector: 'upgrade-page',
   templateUrl:'upgrade.component.html',
   styleUrls: ['upgrade.component.css']
})
export class UpgradeComponent {

 constructor(private upgradeService:UpgradeService){
    
 }
 pricinghalfyear:number=999;
 subscription_type_halfyear:string= "SkillPractical Stuffed Plan";

 pricingfullyear:number=1800;
 subscription_type_fullyear:string= "SkillPractical Fully Stuffed Plan";

}
