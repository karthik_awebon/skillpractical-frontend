import {CanActivateChild } from "@angular/router";
import {Injectable} from "@angular/core";
 
import {LoginService} from '../services/login.service'

import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';


@Injectable()
export class LoginGuard implements CanActivate, CanActivateChild{
 
    // constructor(private loginService:LoginService){

    // }


    constructor(private router: Router) { }

  canActivate() {
    if (localStorage.getItem('currentUserName')) {
      // logged in so return true
      return true;
      }

    // not logged in so redirect to login page with the return url
      this.router.navigate(['/landingpage']);
      return false;
  }

  canActivateChild() {
    if (localStorage.getItem('currentUserName')) {
      // logged in so return true
      return true;
      }

    // not logged in so redirect to login page with the return url
      this.router.navigate(['/landingpage']);
      return false;
  }

}