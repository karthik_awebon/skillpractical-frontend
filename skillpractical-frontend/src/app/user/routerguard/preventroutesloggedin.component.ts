import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { LoginService } from 'app/user';

// this to restrict routes after login

@Injectable()
export class PreventLoggedInAccess implements CanActivate {

  constructor(private loginservice: LoginService) {}

  canActivate() {
    // return !this.loginservice.checkLogin();
    if (!localStorage.getItem('currentUserName')) {
      // logged in so return true
      return true;
      }
  }
} 