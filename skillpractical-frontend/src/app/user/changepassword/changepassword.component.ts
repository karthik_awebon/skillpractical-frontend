import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SettingsService } from 'app/user/services/setting.service';
import { UpdatePasswordModel } from 'app/user/shared/updatepassword.model';
import { ToastrService, ToastrConfig } from 'toastr-ng2';
import { UserNameModel } from 'app/user/shared/username.model';
import { UserService } from 'app/community/user.service';
import { IUser } from 'app/user/shared/userinterface';
import { concat } from 'rxjs/operators/concat';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  errorMessage: string;
  currentpassword:string;
  userid:string;
  user:IUser; 
  
  
  currentusername:string;
  modelusername = new UserNameModel ('');
  
  typespassword:string;

  constructor(private settingsService:SettingsService,private toastrService: ToastrService,
    private userService:UserService,private toastrConfig: ToastrConfig) { 
    toastrConfig.timeOut = 2000;      
    this.userid = localStorage.getItem("userid");

  }
  modelpassword = new UpdatePasswordModel('','','','');

  ngOnInit() {
    this.currentusername = localStorage.getItem("currentUserName");
    this.modelusername = new UserNameModel(this.currentusername); 
    this.userService.getUserByName(this.modelusername).subscribe(
      user => {
        this.user = user;
        this.currentpassword = this.user.password;
        },
        error => console.log(error)
      ) 
  }
  isValid:boolean = true;

  isValidForm() {
     return this.isValid;
 }
  submitForm1(form1: NgForm) {

    this.isValid = false;
      this.modelpassword.userid = this.userid;
      // this.typespassword = this.modelpassword.currentpassword;
      console.log("current password"+this.currentpassword);
      console.log("typed password"+this.modelpassword.currentpassword)
     

     if(this.currentpassword == this.modelpassword.currentpassword && this.currentpassword != this.modelpassword.password){
            this.settingsService.postUpdateUserPassword(this.modelpassword)   
      
              .subscribe(
                data => {

                  this.toastrService.success('Your New Password Successfully Updated! ');
                  
                  this.isValid = true;
                  form1.reset();
                  for(var name in form1.controls) {
                    form1.controls[name].setErrors(null);
                  }
                  this.ngOnInit();
                
                },
                err => console.log('error: ', err)     
              );
              
              console.log(this.modelpassword);
            }else{
              console.log("passwords not matched")
              this.toastrService.error('Current Password should Match & New Password shouldnt be your old password');
              this.isValid = true;
            }
      }
     
    }
   
  

