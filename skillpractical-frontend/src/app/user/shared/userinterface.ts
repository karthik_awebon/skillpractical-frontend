// This interface is for login purpose main interface is in community (user interface)

export interface IUser {
  
    userid: string;
    firstname: string;
    lastname: string;
    username: string;
    password: string;
    emailid:string;
    phonenumber:string;
    dob:string;
    address:string;
    state:string;
    district:string;
    created: Date;
    subscription_status:string;
}

