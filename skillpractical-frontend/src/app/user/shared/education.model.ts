
export class EducationModel{
  
    constructor(
    public rollnumber:string,
    public collegename: string,
    public collegecode: string,
    public fieldofstudy: string,
    public branch:string,
    public year:string,
    public userid: string
    
    ) {
  
    }
  
  }
  