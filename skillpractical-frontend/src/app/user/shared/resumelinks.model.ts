
export class ResumeLinksModel{
  
    constructor(
   
    public resume:string,
    public facebook: string,
    public linkedin: string,
    public twitter: string,
    public googleplus: string,
    public userid: string
    ) {
  
    }
  
  }
  