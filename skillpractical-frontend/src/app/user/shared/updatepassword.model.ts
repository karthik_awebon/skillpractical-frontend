export class UpdatePasswordModel{
    
    constructor(
    public currentpassword: string,
    public password: string,
    public confirmpassword: string,
    public userid: string
    ) {
  
    }
  
}