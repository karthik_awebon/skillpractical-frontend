
export class GeneralModel{
  
  constructor(
  public firstname: string,
  public lastname: string,
  public username: string,
  public emailid:string,
  public phonenumber:string,
  public address:string,
  public state:string,
  public district:string,
  public userid:string,
  public status:boolean
  ) {

  }

}
