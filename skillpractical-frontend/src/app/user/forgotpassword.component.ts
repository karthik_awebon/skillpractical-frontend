import {Component,trigger, state, style, transition, animate} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {LoginService} from './services/login.service'

import {ForgotPasswordService} from './services/forgotpassword.service';

import {ForgotPasswordModel} from './shared/forgotpassword.model';
import {Router} from '@angular/router';


import { ToastrService } from 'toastr-ng2';
import { ToastrConfig } from 'toastr-ng2';

import { NgForm } from '@angular/forms';
import { error } from 'selenium-webdriver';

@Component({
  selector: 'forgot-password',
  templateUrl: 'forgotpassword.component.html',
  styleUrls: ['forgotpassword.component.css']
})
export class ForgotPassword {
   model = new ForgotPasswordModel('');

   public emailNotExists: boolean =false;
   public forgetPasswordEmailSent: boolean;

   result_msg:string;

  constructor (private forgotPasswordService :ForgotPasswordService,private toastrConfig: ToastrConfig,
    private toastrService: ToastrService ) {
    toastrConfig.timeOut = 2000;
  }

  isValid:boolean = true;

  isValidForm() {
     return this.isValid;
 }

  forgotpasswordclicked:boolean;

  submitForm(form: NgForm) {

    this.isValid = false;
    
    this.forgotpasswordclicked = true;

    this.forgetPasswordEmailSent = false;
  	this.emailNotExists = false;

    this.forgotPasswordService.forgotPassword(this.model)
        .subscribe(
            data => {
              this.result_msg = data;
                            if(this.result_msg == "emailnotExist"){
                                   this.emailNotExists = true;
                                   console.log(this.result_msg);
                            }
                            if(this.result_msg == "SUCCESS"){
                                this.forgetPasswordEmailSent = true;
                                this.forgotpasswordclicked = false
                                form.reset();
                                this.isValid = true;
                            }         
            },
            error => {
              this.toastrService.error("Incorrect username or password");
            }
          )
        }
    }
