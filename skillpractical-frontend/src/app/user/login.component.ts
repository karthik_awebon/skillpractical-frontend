import {Component,trigger, state, style, transition, animate} from '@angular/core';
import { Input, ViewContainerRef } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {LoginService} from './services/login.service'
import {UserNameModel} from './shared/username.model'
import {UserLoginModel} from './shared/userlogin.model';
import {IToken} from './shared/token.interface'
import {Router} from '@angular/router';
import {IUser} from './shared/userinterface'
import { UserService } from 'app/community/user.service';

import { ToastrService } from 'toastr-ng2';
import { ToastrConfig } from 'toastr-ng2';
import { NavBarComponent, NavBarService } from 'app/nav';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'login-user',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class Login {
 

  @ViewChild('closeBtn') closeBtn: ElementRef;

   model = new UserLoginModel('','');
   currentUserName:string;
   currentusername:string;

   modelusername = new UserNameModel ('');
   user:IUser; 
   userid:string;
   subscription_status:string;

   public loginError:boolean = false;
   public loggedIn = false;
   responce:string;
  tokeninfo : IToken;
  constructor (private navbarservice:NavBarService,private navBarComponent:NavBarComponent,
     private toastrConfig: ToastrConfig,private toastrService: ToastrService,private loginService: LoginService,private router:Router,private userService:UserService) {
    toastrConfig.timeOut = 1000;
  }
  isValid:boolean = true;

  isValidForm() {
     return this.isValid;
 }
 
  submitForm(form: NgForm) {

    this.isValid = false;
    this.loginService.sendCredentials(this.model).subscribe(
      data => {
                this.tokeninfo = data;

        localStorage.setItem("token", JSON.parse(JSON.stringify(this.tokeninfo.token || null)));
        this.loginService.sendToken(localStorage.getItem("token")).subscribe(
          data => {
            this.currentUserName = this.model.username;
            
            // for closing bootstrap modal
            this.closeBtn.nativeElement.click();
            this.toastrService.success('Login Successful! ');
            this.isValid = true;

            console.log("user info" + data);
           
            localStorage.setItem("currentUserName", this.model.username);
            // this.model.username="";
            // this.model.password="";

            this.currentusername = localStorage.getItem("currentUserName");
            this.modelusername = new UserNameModel(this.currentusername); 
                                    
            this.userService.getUserByName(this.modelusername).subscribe(
                              user => {
                                this.user = user;
                                this.userid = this.user.userid;
                                this.subscription_status = this.user.subscription_status;
                                localStorage.setItem("userid", JSON.stringify(this.userid));                   
                               
                                // location.reload();
                                this.navBarComponent.ngOnInit();

                                this.router.navigateByUrl('/homepagecomponent');

                                console.log("user detail from home page"+JSON.stringify(this.user));
                                console.log("user id from homepage"+this.userid);
                                },
                                error => console.log(error)
                              ) 
            console.log("token display "+ localStorage.getItem("token") );
          },
          error => {
            this.loggedIn = false;
            this.loginError = true;

            this.toastrService.error("Incorrect username or password");

            this.isValid = true;
          }
        )
      }
     
    )
     console.log("login modell"+JSON.stringify(this.model));
  }


  signup = false;
  toggleSignup(){
    this.signup = true;
  }
  signin = false;
  toggleLogin(){
     this.signin = true;
  }

}
