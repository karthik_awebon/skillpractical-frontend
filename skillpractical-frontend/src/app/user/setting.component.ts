import { Component,Input } from '@angular/core';

import { RegisterService } from './services/register.service'
import { UserModel } from './user.model'

import { NgForm } from '@angular/forms';

import {UserService } from '../community/user.service'

import {UserNameModel } from '../community/username.model'

import {SettingsService} from './services/setting.service'
import { ToastrService } from 'toastr-ng2';
import { UpdatePasswordModel } from 'app/user/shared/updatepassword.model';
import { IUser } from 'app/user/shared/userinterface';

@Component({
  selector: 'setting-user',
  styleUrls: ['./setting.component.css'],
  templateUrl: 'setting.component.html'
})

export class SettingUserComponent {

  completion:any;

  value:number

  value1:number;
  value2:number;
  value3:number;
  value4:number;

  success:boolean;

  constructor(){
       this.checkprogressvalues();
   }

   checkprogressvalues(){
  //   if(localStorage.getItem("settingvalueeducation") == "null" || localStorage.getItem("settingvalueeducation") == "" ||
  //   localStorage.getItem("settingvaluegeneral") == "null" || localStorage.getItem("settingvaluegeneral") == "" ||
  //   localStorage.getItem("settingvalueuserskills") == "null" || localStorage.getItem("settingvalueuserskills") == "" ||
  //   localStorage.getItem("settingvalueresumelinks") == "null" || localStorage.getItem("settingvalueresumelinks") == ""
  //  ){
      setTimeout(() => {
          this.ngOnInit();
      },1000)

      console.log("called this");
  //  }

  }


  ngOnInit() {
    
    console.log("called this")
    this.value1 = parseInt(localStorage.getItem("settingvalueeducation"));
    this.value2 = parseInt(localStorage.getItem("settingvaluegeneral"));
    this.value3 =  parseInt(localStorage.getItem("settingvalueuserskills"));
    this.value4 = parseInt(localStorage.getItem("settingvalueresumelinks"));

    this.value = this.value1+this.value2+this.value3+this.value4

    console.log("progress value"+this.value)

  if(this.value == 25){
    this.completion = {
      'width':'25%'
    }  
  }else if(this.value == 50){
    this.completion = {
      'width':'50%'
    }
  } else if(this.value == 75){
    this.completion = {
      'width':'75%'
    }
  } else if(this.value == 100){
    
    this.success = true;
    this.completion = {
      'width':'100%'
    }
  }

    }

}