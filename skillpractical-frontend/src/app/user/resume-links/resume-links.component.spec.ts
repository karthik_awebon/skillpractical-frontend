import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumeLinksComponent } from './resume-links.component';

describe('ResumeLinksComponent', () => {
  let component: ResumeLinksComponent;
  let fixture: ComponentFixture<ResumeLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumeLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumeLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
