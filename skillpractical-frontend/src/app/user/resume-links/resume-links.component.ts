import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'app/user/services/setting.service';
import { NgForm } from '@angular/forms';
import { ResumeLinksModel } from 'app/user/shared/resumelinks.model';
import { UserIdModel } from 'app/community/userid.model';
import { UserService } from 'app/community/user.service';
import { IUserResumeAndLinks } from 'app/community/interfaces/userInterface';
import { ToastrService, ToastrConfig } from 'toastr-ng2';
import { SettingUserComponent } from '../setting.component';

@Component({
  selector: 'app-resume-links',
  templateUrl: './resume-links.component.html',
  styleUrls: ['./resume-links.component.css']
})
export class ResumeLinksComponent implements OnInit {
  userid: string;
  noChangeYet:boolean = true;
  modeluserid = new UserIdModel('');

  resumeandlinks:IUserResumeAndLinks;
  
  model = new ResumeLinksModel('','','','','','');

  settingvalueresumelinks:number;
  filledstatustyleresumelinks:any;

  constructor(private settingsService:SettingsService,private userService: UserService,private toastrService: ToastrService,private toastrConfig: ToastrConfig,
    private settingUserComponent:SettingUserComponent) { 
    toastrConfig.timeOut = 2000;

  }

  ngOnInit() {
    this.userid = localStorage.getItem("userid");
    this.modeluserid.userid = this.userid;
     this.userService.getResumeAndLinksByUserId(this.modeluserid).subscribe(
      resumeandlinks => {
                         this.resumeandlinks = resumeandlinks; 
                         this.model.facebook = this.resumeandlinks.facebook;
                         this.model.googleplus = this.resumeandlinks.googleplus;
                         this.model.linkedin = this.resumeandlinks.linkedin;
                         this.model.twitter = this.resumeandlinks.twitter;

                         if(this.resumeandlinks.facebook == "null" || this.resumeandlinks.facebook == "" || this.resumeandlinks.facebook == null || this.resumeandlinks.googleplus == "null" || this.resumeandlinks.googleplus == "" || this.resumeandlinks.googleplus == null ||
                          this.resumeandlinks.linkedin == "null" || this.resumeandlinks.linkedin == "" ||  this.resumeandlinks.linkedin == null || this.resumeandlinks.twitter == "null" || this.resumeandlinks.twitter == "" || this.resumeandlinks.twitter == null
                       ){
                             this.settingvalueresumelinks = 0;
                             this.filledstatustyleresumelinks = {
                             'color': 'red'
                               }
                               localStorage.setItem("settingvalueresumelinks", JSON.stringify(this.settingvalueresumelinks)); 
                         } else {
                           this.settingvalueresumelinks = 25;
                           this.filledstatustyleresumelinks = {
                             'color': 'green'
                             }
                             localStorage.setItem("settingvalueresumelinks", JSON.stringify(this.settingvalueresumelinks)); 
                         }

                       

                           console.log("resumeandlinks by userid"+JSON.stringify(this.resumeandlinks));
                         },
                         error => console.log(error)
         )
  }
  isValid:boolean = true;

  isValidForm() {
     return this.isValid;
 }
  submitForm(form: NgForm) {
    this.isValid = false;
    this.model.userid = this.userid;
    this.settingsService.postUpdateUserResumeLinksForm(this.model)  
   
        .subscribe(
          data => {  
            this.toastrService.success('Your Resume and Social Media Settings is Successfully Updated! ');
            // form.reset();
            this.isValid = true;

            this.ngOnInit();

            setTimeout(() => {
              this.settingUserComponent.ngOnInit();
          },1000)


          },
          err => console.log('error: ', err),
        );
        console.log(this.model);
   }
   changeVal(){ // triggers on change of any field(s)
    console.log("change");
    this.noChangeYet = false;
  }
}
