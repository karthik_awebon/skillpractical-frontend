import { Component, Input, ViewContainerRef } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import { RegisterService } from './services/register.service'
import { UserModel } from './user.model'
import {UserRegisterModel} from './userregister.model'
import {CheckUserForRegistrationModel} from './shared/checkUserForRegistration.model'

import {IUserId} from './shared/userid.interface'
import { NgForm } from '@angular/forms';

import { Router } from '@angular/router';
import {NavBarService} from '../nav/navbar.service'

import { ToastrService, ToastrConfig } from 'toastr-ng2';

@Component({
  selector: 'register-user',
  styleUrls: ['./register.component.css'],
  templateUrl: 'register.component.html'
})

export class RegisterComponent {

  @ViewChild('closeBtn') closeBtn: ElementRef;

  private emailSent: boolean =false;
  private usernameExists:boolean;
  private emailExists:boolean;

  result_msg:string;

  model = new UserRegisterModel('','','');
  modelcheckregistrationstatus = new CheckUserForRegistrationModel('','');
  registered: boolean = false;

  userid:IUserId;

  private loggedIn = false;
  private loggedInMember = false;
  
  

 constructor (private toastrService: ToastrService,private toastrConfig: ToastrConfig,
      private navbarservice: NavBarService,private router:Router,private registerService: RegisterService){
        toastrConfig.timeOut = 2000;
    
 }

 registereclicked:boolean;
 isValid:boolean = true;

 isValidForm() {
    return this.isValid;
}
  submitForm(form: NgForm) {
  this.isValid = false;
    this.registereclicked = true;

    this.usernameExists = false;
  	this.emailExists = false;
  	this.emailSent = false;
    
    this.registerService.postRegisterUserForm(this.model)
      
        .subscribe(
          data => {
              this.result_msg = data;

              if(this.result_msg == "SUCCESS"){

                      this.registereclicked = false;

                     this.emailSent = true;
                     
                     this.toastrService.success('Registration Successful! you will receive password via mail');
                     form.reset();
                     this.isValid = true;

                    //  setTimeout(() => {
                      this.closeBtn.nativeElement.click();
                      this.emailSent = false;
                    // },2000)
                      this.loginclicked = true;
                     console.log(this.result_msg);
              }
              if(this.result_msg == "usernameExists"){
                  this.usernameExists = true;
                  this.registereclicked = false;
                  
              }
              if(this.result_msg == "emailExists"){
                  this.emailExists = true;
                  this.registereclicked = false;
                  
              }

          },
          err => console.log('error: ', err)
          
        );
         console.log("register user"+JSON.stringify(this.model));
  }

   

  // registrationchecksuccess:boolean;
  // userdosentexist:boolean;
  
  // submitFormCheckRegistrationStatus(form: NgForm) {
  //   this.isValid = false;       
  //       this.registerService.checkUserForRegistration(this.modelcheckregistrationstatus)
  //           .subscribe(
  //               data => {

  //                     this.userid = data;

  //                     if(this.userid.userid != "0"){
  //                         this.registrationchecksuccess = true;
  //                     }
  //                     else {
  //                       this.userdosentexist = true;
  //                     }
  //                     this.isValid = true;
  //                     console.log(this.userid);
                    
  //               },
  //               err => console.log('error: ', err)
                
                
  //             ),
  //              console.log("check user for registration"+JSON.stringify(this.modelcheckregistrationstatus));
  //   }


    //maked true to enable login first
  loginclicked:boolean = true;
  forgotpasswordclicked : boolean;
  clickedlogin(){
      this.loginclicked = true;
   
      console.log("clicked login")
  }

  clickedsignup(){
    this.loginclicked = false;
    // this.registrationchecksuccess = false;
  }
  
  clickedForgotPassword(){
    this.router.navigate(['/forgotPassword']);
  
  }

  // nextclicked:boolean;
  // next(){
  //     this.nextclicked = true;
  // }

  // Prev(){
  //   this.nextclicked = false;
  // }
  
}
