
export class UserModel{
  
  constructor(
  public firstname: string,
  public lastname: string,
  public username: string,
  public emailid:string,
  public phonenumber:string,
  public dob:string,
  public password: string,
  public confirmpassword: string,
  public state:string,
  public district:string,
  public college:string,
  public branch:string,
  public year:string,
  public rollnumber:string,
  public expirationdate:string,
  public subscriptiontype:string
  ) {

  }

}
