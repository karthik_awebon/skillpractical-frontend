import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriptionAndBillingComponent } from './subscription-and-billing.component';

describe('SubscriptionAndBillingComponent', () => {
  let component: SubscriptionAndBillingComponent;
  let fixture: ComponentFixture<SubscriptionAndBillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscriptionAndBillingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionAndBillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
