import { Component, OnInit } from '@angular/core';
import { UserModel } from 'app/user';
import { SubscriptionService } from 'app/user/services/subscription.service';
import { UserNameModel } from 'app/user/shared/username.model';
import { UserIdModel } from 'app/community/userid.model';
import { ISubscriptionandBilling } from 'app/user/shared/subscriptionandbilling.interface';

@Component({
  selector: 'app-subscription-and-billing',
  templateUrl: './subscription-and-billing.component.html',
  styleUrls: ['./subscription-and-billing.component.css']
})
export class SubscriptionAndBillingComponent implements OnInit {
  modeluser:string;
  subscriptionandbilling:ISubscriptionandBilling;
  userIdModel = new UserIdModel(null); 
  subscription_type:string;
  subscription_expiration_date:string;
  subscription_status:string;
  userid:string;

  constructor(private subscriptionService: SubscriptionService) {
    
  }
  
  ngOnInit() {
    this.userid = localStorage.getItem("userid");
    this.userIdModel.userid = this.userid;
    this.subscriptionService.getsubscriptionbilling(this.userIdModel).subscribe(
      data => {
                        this.subscriptionandbilling = data; 
                        this.subscription_type = this.subscriptionandbilling.subscription_type;
                        this.subscription_expiration_date = this.subscriptionandbilling.subscription_expiration_date;
                        this.subscription_status = this.subscriptionandbilling.subscription_status;
     
                          console.log("subscription and billing"+JSON.stringify(this.subscriptionandbilling));
                        },
                        error => console.log(error)
        )
  }

}
