import { Component, OnInit,Input,OnChanges } from '@angular/core';
import { DataService } from '../../service/data.service';


@Component({
  selector: 'app-diyprojectcarddisplay',
  templateUrl: './diyprojectcarddisplay.component.html',
  styleUrls: ['./diyprojectcarddisplay.component.css']
})
export class DIYProjectCardDisplayComponent implements OnInit,OnChanges {
  @Input() projectInfo: any;
  @Input() userProjectInfo : any;
  @Input() userFavInfo: any;
  @Input() userLikeInfo: any;
  userFavorite:boolean = false;
  userLiked:boolean = false;
  projectModules:any;
  percentageCompleted: number = 0;
  likedCount: number = 0;

  constructor(private dataService: DataService) {

  }
  ngOnInit() {
    if(!this.isString(this.userFavInfo))
      this.userFavorite = true;
    if(!this.isString(this.userLikeInfo)){
      this.userLiked = true;
    }
    this.likedCount = this.projectInfo.likes;
    
     if(!this.isString(this.userProjectInfo))
        this.percentageCompleted = Math.round((this.userProjectInfo.modules_completed/this.userProjectInfo.total_modules) * 100);
  }
  ngOnChanges(){
  }
  
  private isString (value) {
    return typeof value === 'string' || value instanceof String;
  };

  clickFavouriteFlag(flag:string){
    this.dataService.saveUserFavoriteTest(this.dataService.USER_ID,this.projectInfo.c_id,flag).subscribe(res => {
      if(res.result_msg === "SUCCESS" ){
        if(flag === "N")
          this.userFavorite = false;
        else
          this.userFavorite = true;
      }
    });
  }

  clickLikedFlag(flag:string){
    this.dataService.saveUserLikedProject(this.dataService.USER_ID,this.projectInfo.c_id,flag).subscribe(res => {
      if(res.result_msg === "SUCCESS" ){
        if(flag === "N"){
          this.userLiked = false;
          this.likedCount-=1;
        }
        else{
          this.userLiked = true;
          this.likedCount+=1;
        }
      }
    });
  }
  getProjectModules(){
    this.projectModules = [];
    this.dataService.getProjectModules(this.projectInfo.c_id).subscribe(res => {
      if(res.result_msg === "SUCCESS" ){
          this.projectModules = res.result;
      }
    });
  }
}
