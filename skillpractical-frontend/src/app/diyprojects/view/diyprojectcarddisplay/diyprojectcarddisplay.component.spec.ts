import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DIYProjectCardDisplayComponent } from './diyprojectcarddisplay.component';

describe('TestCardDisplayComponent', () => {
  let component: DIYProjectCardDisplayComponent;
  let fixture: ComponentFixture<DIYProjectCardDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DIYProjectCardDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DIYProjectCardDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
