import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DIYModuleDetailComponent } from './diymoduledetail.component';

describe('TestDetailComponent', () => {
  let component: DIYModuleDetailComponent;
  let fixture: ComponentFixture<DIYModuleDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DIYModuleDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DIYModuleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
