import { Component, OnInit,Inject,ViewChild } from '@angular/core';
import {DataService} from '../../service/data.service';
import { ActivatedRoute } from '@angular/router';
import { PopoverContent } from 'ngx-popover'

@Component({
  selector: 'app-diymoduledetail',
  templateUrl: './diymoduledetail.component.html',
  styleUrls: ['./diymoduledetail.component.css']
})
export class DIYModuleDetailComponent implements OnInit {

  moduleInfo: any = null;
  projectModules:any;
  userAnswers:any = {};
  testCompleted:boolean = false;
  nextModuleId:number = 0;
  previousModuleId:number = 0;
  currentModuleId:number = 0;
  @ViewChild('modulesPopover') modulesPopover: PopoverContent = null;

  constructor(private dataService: DataService,private route:ActivatedRoute) {

  }
  ngOnInit() {    
    this.route.params.subscribe((params) => {
      this.dataService.getModuleInfo(this.dataService.USER_ID,params.id).subscribe(res => {
        if(this.modulesPopover && this.modulesPopover.popover){
          this.modulesPopover.hide();
        }
        this.moduleInfo = res.result;
        this.getProjectModules();
        this.currentModuleId = this.moduleInfo.diy_module_info.module_id;

        if(this.moduleInfo.diy_module_question_answer_info){
          this.moduleInfo.diy_module_question_answer_info.forEach(question => {
            if(this.moduleInfo.diy_user_module_test_info){
              this.testCompleted= true;
              let questionUserAnswer = this.moduleInfo.diy_user_module_test_info.diy_user_module_test_answer_info.filter((user_answer)=>{
                return question.question_id == user_answer.question_id
              })
              if(questionUserAnswer[0]){
                this.userAnswers[question.question_id] = questionUserAnswer[0].answers;
              }else{
                this.userAnswers[question.question_id] = '';
              }
            }else{
              this.userAnswers[question.question_id] = '';
            }
          });
        }
      });
    });

  }

  getProjectModules(){
    this.projectModules = [];
    this.dataService.getProjectModules(this.moduleInfo.diy_module_info.project_id).subscribe(res => {
      if(res.result_msg === "SUCCESS" ){
          this.projectModules = res.result;
          if(this.currentModuleId){
            let currentModuleIndex = this.projectModules.findIndex((module) =>{
              return this.currentModuleId == module.module_id;
            });
            if(currentModuleIndex >0){
              this.previousModuleId = this.projectModules[currentModuleIndex-1].module_id;
              if(this.projectModules.length-1 == currentModuleIndex){
                this.nextModuleId = 0;
              }else{
                this.nextModuleId = this.projectModules[currentModuleIndex+1].module_id;
              }
            }else{
              this.previousModuleId = 0;
              this.nextModuleId = this.projectModules[currentModuleIndex+1].module_id;
            }
          }
      }
    });
  }

  submitTest(){
    this.dataService.submitTest(this.moduleInfo.diy_module_info.module_id,this.dataService.USER_ID,this.userAnswers).subscribe(res => {
      this.testCompleted= true;
    });
  }

  isChecked(answerId,questionId){
    if(this.userAnswers[questionId] != ''){
      let selectedAnswers  = String(this.userAnswers[questionId]).split(",");
      if(selectedAnswers.indexOf(String(answerId)) != -1){
        return true;
      }
      return false;
    }
  }

  selectAnswer(event,questionId){
    let selectedQuestion = this.moduleInfo.diy_module_question_answer_info.filter((question) =>{
      return question.question_id == questionId;
    })
    if(selectedQuestion[0].question_type == 'single'){
      this.userAnswers[selectedQuestion[0].question_id] = event.value;
    }else if(selectedQuestion[0].question_type == 'multiple'){
      if (event.checked) {
        if(this.userAnswers[selectedQuestion[0].question_id] == ''){
          this.userAnswers[selectedQuestion[0].question_id] = event.source.name;
        }else{
          this.userAnswers[selectedQuestion[0].question_id] = this.userAnswers[selectedQuestion[0].question_id]+','+event.source.name;
        }
      }else{
        let selectedAnswers  = this.userAnswers[selectedQuestion[0].question_id].split(",");
        var unSelectedElement = selectedAnswers.indexOf(event.source.name);
        selectedAnswers.splice(unSelectedElement, 1);
        this.userAnswers[selectedQuestion[0].question_id] = selectedAnswers.join(",");
      }
    }
  }

}
