import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DIYProjectDetailComponent } from './diyprojectdetail.component';

describe('TestDetailComponent', () => {
  let component: DIYProjectDetailComponent;
  let fixture: ComponentFixture<DIYProjectDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DIYProjectDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DIYProjectDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
