import { Component, OnInit,Inject } from '@angular/core';
import {DataService} from '../../service/data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-diyprojectdetail',
  templateUrl: './diyprojectdetail.component.html',
  styleUrls: ['./diyprojectdetail.component.css']
})
export class DIYProjectDetailComponent implements OnInit {

  projectInfo: any = null;
  userFavorite:boolean = false;
  userLiked:boolean = false;
  percentageCompleted: number = 0;
  viewCount: number = 0;
  likedCount: number = 0;
  projectTags: any=[];

  constructor(private dataService: DataService,private route:ActivatedRoute) {

  }
  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.dataService.getProjectInfo(this.dataService.USER_ID,params.id).subscribe(res => {
        this.projectInfo = res.result;
        if(this.projectInfo.diy_project_info){
          this.viewCount = this.projectInfo.diy_project_info.views;
          this.dataService.UpdateViewCountForProject(params.id).subscribe(res => {
            this.viewCount+=1;
          })
          this.likedCount = this.projectInfo.diy_project_info.likes;          
        }
        if(this.projectInfo.diy_user_favorite_project_info){
          this.userFavorite = true;
        }
        if(this.projectInfo.diy_user_liked_project_info){
          this.userLiked = true;
        }
        if(this.projectInfo.diy_user_project_info)
          this.percentageCompleted = Math.round((this.projectInfo.diy_user_project_info.modules_completed/this.projectInfo.diy_user_project_info.total_modules) * 100);

        if(this.projectInfo.diy_project_info.tags != ''){
          this.projectTags = this.projectInfo.diy_project_info.tags.split(',');
        }
      });
    });
  }
  clickFavouriteFlag(flag:string){
    this.dataService.saveUserFavoriteTest(this.dataService.USER_ID,this.projectInfo.diy_project_info.c_id,flag).subscribe(res => {
      if(res.result_msg === "SUCCESS" ){
        if(flag === "N")
          this.userFavorite = false;
        else
          this.userFavorite = true;
      }
    });
  }

  clickLikedFlag(flag:string){
    this.dataService.saveUserLikedProject(this.dataService.USER_ID,this.projectInfo.diy_project_info.c_id,flag).subscribe(res => {
      if(res.result_msg === "SUCCESS" ){
        if(flag === "N"){
          this.userLiked = false;
          this.likedCount-=1;
        }
        else{
          this.userLiked = true;
          this.likedCount+=1;
        }
      }
    });
  }
}
