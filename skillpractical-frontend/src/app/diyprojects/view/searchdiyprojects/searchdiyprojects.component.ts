import { Component, OnInit,Inject } from '@angular/core';
import {DataService} from '../../service/data.service';
import { ActivatedRoute } from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-searchdiyprojects',
  templateUrl: './searchdiyprojects.component.html',
  styleUrls: ['./searchdiyprojects.component.css']
})
export class SearchDIYProjectsComponent implements OnInit {

  branches:any;
  subjects:any;
  selectedBranch: string = "";
  selectedSubject: string = "";
  search_text: string = "";
  selectedProjectLevel : string = "";
  completed: boolean;
  resumed: boolean;
  savedtest: boolean;
  loading: boolean;
  itemsPerPage: number = 12;
  isTextSearch: boolean = false;
  searchResult: Observable<any[]>;
  total: number;
  p: number = 1;

  linkToShare:any;
  starredclickstyle: any;
  
  constructor(private dataService: DataService,private route:ActivatedRoute) {

  }
  ngOnInit() {
    this.route.params.subscribe((params) => {
      if(params.searchtext){
        this.search_text = params.searchtext
      }
    })
    this.dataService.getBranches().subscribe(res => {
      this.branches = res.branch_list;
      this.dataService.getSubjects(this.branches.map((branch)=>{ return branch.branch_id}).join(",")).subscribe(res => {
        this.subjects = res.subject_list;
      });        
  });
  this.getPage(1);
  }

  clearBranchSelection(){
    this.selectedBranch = "";
    this.selectedSubject = "";
    this.search_text = "";
    this.dataService.getSubjects(this.branches.map((branch)=>{ return branch.branch_id}).join(",")).subscribe(res => {
      this.subjects = res.subject_list;
    });   
    this.getPage(1);
  }

  isBranchSelected(branchId): boolean {
    if(!this.selectedBranch) {
        return false;
    }
    var selectedBranchs  = this.selectedBranch.split(",");
    for(var x=0;x<selectedBranchs.length;x++){
      if(selectedBranchs[x] == branchId)
        return true;
    }
    return false;
  }

  loadSubjects(selectedBranch:string){
    this.selectedBranch = String(selectedBranch);
    this.dataService.getSubjects(this.selectedBranch).subscribe(res => {
        this.subjects = res.subject_list;
        this.getPage(1);
        this.selectedSubject = "";
    });
  }
  isSubjectSelected(subjectId): boolean {
    if(!this.selectedSubject) {
        return false;
    }
    var selectedSubjects  = this.selectedSubject.split(",");
    for(var x=0;x<selectedSubjects.length;x++){
      if(selectedSubjects[x] == subjectId)
        return true;
     }
    return false;
  }

  selectSubject(subjectId:string){
    if(this.selectedSubject != ""){
      var selectedSubjects  = this.selectedSubject.split(",");
      var inSelectedList = false;
      for(var x=0;x<selectedSubjects.length;x++){
        if(selectedSubjects[x] == subjectId){
          inSelectedList = true;
        }
      }
      if(!inSelectedList)         
        this.selectedSubject+=","+subjectId;
      else{
        var unSelectedElement = selectedSubjects.indexOf(String(subjectId));
        selectedSubjects.splice(unSelectedElement, 1);
        this.selectedSubject = selectedSubjects.join(",");
      }
    }else{
      this.selectedSubject = String(subjectId);
    }
    this.getPage(1);
  }
  
  isProjectLevelSelected(testLevel): boolean {
    if(!this.selectedProjectLevel) {
        return false;
    }
    var selectedTestLevels  = this.selectedProjectLevel.split(",");
    for(var x=0;x<selectedTestLevels.length;x++){
      if(selectedTestLevels[x] == testLevel)
        return true;
     }
    return false;
  }

  selectProjectLevel(testLevel:string){
    if(this.selectedProjectLevel != ""){   
      var selectedTestLevels  = this.selectedProjectLevel.split(",");
      var inSelectedList = false;
      for(var x=0;x<selectedTestLevels.length;x++){
        if(selectedTestLevels[x] == testLevel){
          inSelectedList = true;
        }
      }
      if(!inSelectedList)         
        this.selectedProjectLevel+=","+testLevel;
      else{
        var unSelectedElement = selectedTestLevels.indexOf(testLevel);
        selectedTestLevels.splice(unSelectedElement, 1);
        this.selectedProjectLevel = selectedTestLevels.join(",");
      }
    }else{
      this.selectedProjectLevel = testLevel;
    }       
    this.getPage(1);
  }

  getPage(page: number) {
    this.loading = true;
    var offset = (page-1)*(this.itemsPerPage);
    
    if(!this.isTextSearch && (this.selectedBranch || this.selectedSubject)){
      this.search_text = '';
      var selectedSubjects = this.selectedSubject;
      if(this.selectedSubject == ""){
        selectedSubjects = this.subjects.map(subject=>{
            return subject.subject_id;
        }).join(",");
      }
      this.searchResult = this.dataService.searchTest(selectedSubjects,
        this.selectedProjectLevel,this.dataService.USER_ID,(this.completed)?"Y":"N",(this.resumed)?"Y":"N",
        (this.savedtest)?"Y":"N",offset,this.itemsPerPage)
          .do(res => {
            if(res.result_info.total_records > 0){
              this.total = res.result_info.total_records;
            }else{
              this.total = 0;
            }
              this.p = page;
              this.loading = false;
          })
          .map((res) => {
            if(res.result_info.total_records > 0){
              return res.result_info.test_info;
            }else{
              return [];
            }          
            }
          );
      }else{
        this.searchResult = this.dataService.searchTestByText(this.dataService.USER_ID,offset,this.itemsPerPage,this.search_text)
            .do(res => {
              if(res.result_info.total_records > 0){
                this.total = res.result_info.total_records;
              }else{
                this.total = 0;
              }
                this.p = page;
                this.loading = false;
                this.isTextSearch = false;
            })
            .map((res) => {
              if(res.result_info.total_records > 0){
                return res.result_info.diyproject_info;
              }else{
                return [];
              }          
              }
            );
      }
  }

  textSearch(){
    this.isTextSearch=true;
    this.clearFilter();
    this.getPage(1);
  }

  clearFilter(){
    this.selectedBranch = '';
    this.selectedSubject = '';
    this.selectedProjectLevel = '';
    this.completed = false;
    this.resumed = false;
    this.savedtest = false;
    this.dataService.getSubjects(this.branches.map((branch)=>{ return branch.branch_id}).join(",")).subscribe(res => {
      this.subjects = res.subject_list;
    });  
  }

  clearAllFilters(){
    this.clearFilter();
    this.search_text = "";
    this.getPage(1);
  }
}
