import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchDIYProjectsComponent } from './searchdiyprojects.component';

describe('SearchDIYProjectsComponent', () => {
  let component: SearchDIYProjectsComponent;
  let fixture: ComponentFixture<SearchDIYProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchDIYProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchDIYProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
