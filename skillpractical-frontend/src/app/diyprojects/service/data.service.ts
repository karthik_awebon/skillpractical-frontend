import { Injectable, EventEmitter } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import { ApiSettings } from '../../ApiSettings';


//const BASE_HREF = 'http://ec2-18-218-116-236.us-east-2.compute.amazonaws.com:10080/sp/rest/';


@Injectable()
export class DataService {

  public testCompleted: EventEmitter<any> = new EventEmitter<any>();
  public timerTick: EventEmitter<any> = new EventEmitter<any>();

  public USER_ID = localStorage.getItem("userid");
  public head = new HttpHeaders().set('Authorization', 'Bearer '+localStorage.getItem("token"));

  public httpOptions = {
    headers: this.head
  };

  constructor(private http: HttpClient) {}

  getBranches(): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+ '/rest/diyprojects/reqDIYBranchList.on', {}, this.httpOptions);
  }
  getSubjects(branchId): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+ '/rest/diyprojects/reqDIYSubjectByBranchId.on', {diybranch_id:branchId}, this.httpOptions);
  }

  searchTest(subjectId,projectLevel,userId,completed='N',resumed='N',savedtest='N',offset=1,limit=10): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+ '/rest/diyprojects/reqSearchDIYProject.on', 
    {subject_id:subjectId,project_level:projectLevel,user_id:userId,completed:completed,resumed:resumed,savedtest:savedtest,offset:offset,limit:limit}
    , this.httpOptions);
  }
  
  searchTestByText(userId,offset=1,limit=10,search_text=""): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+ '/rest/diyprojects/reqSearchDIYProjectByText.on', 
    {offset:offset,limit:limit,search_text:search_text,user_id:userId,}
    , this.httpOptions);
  }
  
  saveUserFavoriteTest(userId,projectId,flag="Y"): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+'/rest/diyprojects/reqDIYUserFavProject.on',{user_id:userId,diyproject_id:projectId,flag:flag}, this.httpOptions);
  }

  saveUserLikedProject(userId,projectId,flag="Y"): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+'/rest/diyprojects/reqDIYLikeProject.on',{user_id:userId,diyproject_id:projectId,flag:flag}, this.httpOptions);
  }
  getProjectModules(projectId): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+'/rest/diyprojects/reqDIYProjectModules.on',{diyproject_id:projectId}, this.httpOptions);
  }
  
  getProjectInfo(userId,projectId): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+'/rest/diyprojects/reqDIYProjectDetails.on',{user_id:userId,diyproject_id:projectId}, this.httpOptions);
  }

  getModuleInfo(userId,moduleId): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+'/rest/diyprojects/reqDIYProjectModuleDetails.on',{user_id:userId,diymodule_id:moduleId}, this.httpOptions);
  }

  UpdateViewCountForProject(projectId): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+'/rest/diyprojects/reqDIYUpdateViewCountForProject.on',{diyproject_id:projectId}, this.httpOptions);
  }

  submitTest(moduleId,userId,userAnswers): Observable<any> {
    return this.http.post(ApiSettings.API_ENDPOINT+'/rest/diyprojects/reqDIYSubmitModuleTest.on',{diymodule_id:moduleId,user_id:userId,user_answers:userAnswers}, this.httpOptions);
  }
}
