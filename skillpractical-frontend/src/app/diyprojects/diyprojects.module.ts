import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgxPaginationModule } from 'ngx-pagination';

import { MaterialModule } from './material/material.module';
import {DataService} from './service/data.service';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { diyprojectsRoutes } from './diyprojects.routes';
import { CommonModule } from '@angular/common';
import { PopoverModule } from 'ngx-popover'
import {ShareButtonsModule} from 'ngx-sharebuttons';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { DIYProjectDetailComponent } from './view/diyprojectdetail/diyprojectdetail.component';
import { DIYModuleDetailComponent } from './view/diymoduledetail/diymoduledetail.component';
import { SearchDIYProjectsComponent } from './view/searchdiyprojects/searchdiyprojects.component';
import { DIYProjectCardDisplayComponent } from './view/diyprojectcarddisplay/diyprojectcarddisplay.component';

@NgModule({
  declarations: [
    DIYProjectDetailComponent,
    SearchDIYProjectsComponent,
    DIYModuleDetailComponent,
    DIYProjectCardDisplayComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    RouterModule.forChild(diyprojectsRoutes),
    HttpClientModule,
    // BrowserAnimationsModule,
    NgxPaginationModule,
    PopoverModule,
    ShareButtonsModule.forRoot(),
    ScrollToModule.forRoot() 
  ],
  entryComponents: [],
  providers: [DataService]
})
export class DIYProjectsModule { }
