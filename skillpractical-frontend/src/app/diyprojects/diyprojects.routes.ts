import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DIYProjectDetailComponent } from './view/diyprojectdetail/diyprojectdetail.component';
import { DIYModuleDetailComponent } from './view/diymoduledetail/diymoduledetail.component';
import { SearchDIYProjectsComponent } from './view/searchdiyprojects/searchdiyprojects.component';

export const diyprojectsRoutes = [
  {path: 'diyprojectdetail/:id', component:DIYProjectDetailComponent},
  {path: 'diymoduledetail/:id', component:DIYModuleDetailComponent}, 
  { path: 'searchdiyprojects', component:SearchDIYProjectsComponent},
  { path: 'searchdiyprojects/:searchtext', component:SearchDIYProjectsComponent} 
];