import { Component,ElementRef, Renderer  } from '@angular/core'
import { LoginService } from '../user/services/login.service'
import {Router} from '@angular/router';


import { HomePageService } from '../homepage/shared/homepage.services';

import {ICountTasks} from '../homepage/shared/Icount.tasks.interface'
import {ICountUpdates} from '../homepage/shared/Icount.updates.interfaces'
import {ICountTrending} from '../homepage/shared/Icount.trending.interface'

import {UserIdModel} from '../homepage/shared/userid.model'

import {IMessages} from '../homepage/shared/messages.interface'
import {IMessagesCount} from '../homepage/shared/messagescount.interface'
import {NavBarService} from './navbar.service'

import { Observable } from 'rxjs/Observable';
import { UserNameModel } from '../user/shared/username.model';
import { IUser } from '../user/shared/userinterface';
import { UserService } from '../community/user.service';
import { UpgradeService } from '../user/services/upgrade.service';
import { IUpgradeStatus } from '../user/shared/upgradestatus.interface';
import { ColdObservable } from 'rxjs/testing/ColdObservable';

@Component({
    selector:'nav-bar',
    templateUrl:'navbar.component.html',
      styleUrls: ['navbar.component.css']

})
export class NavBarComponent{
 
  currentusername:string;

  errorMessage:string;

  counttrending:ICountTrending;
  countupdates:ICountUpdates;
  counttasks:ICountTasks;

  trending:number;
  tasks:number;
  updates:number;
  
  userid:string;
   useridmodel = new UserIdModel('');
   messagestoday:IMessages[];
   allmessages:IMessages[];
   messagescount:IMessagesCount[];

   public loggedIn = false;
   private loggedInMember = false;

   user:IUser; 
   subscription_status:string;

   upgradeStatus:IUpgradeStatus;

   constructor (private navbarservice: NavBarService,private loginService:LoginService,
    private router:Router,private homepageservice:HomePageService,private el: ElementRef, private renderer: Renderer,
    private userService:UserService,private upgradeService:UpgradeService
  ) {
        // this.currentusername =  localStorage.getItem("currentUserName");
  }

  onClick() {
    if (this.loginService.checkLogin()) {
      this.loginService.logout();
    } 
  }

  onClickMember(){
    window.location.href='https://www.member.skillpractical.com/';
  }

  ngOnInit() {
 
            this.userid =  localStorage.getItem("userid");
            this.useridmodel.userid = this.userid;
            this.upgradeService.selectUserSubscriptionStatus(this.useridmodel).subscribe(
                      user => {
                        this.upgradeStatus = user;
                        this.subscription_status = this.upgradeStatus.subscription_status;
                       
                        console.log("user subscription status"+this.subscription_status);
                    
                        },
                        error => console.log(error)
                      ) 

        
        console.log("logged in or not"+this.loggedIn);

        this.navbarservice.checkSession(localStorage.getItem("token")).subscribe(
            res => {   
                this.loggedIn = true;
            },
            
            err => {
                this.loggedIn =false;
            }
        );

        if(localStorage.getItem("currentUserName")){
            this.navbarupdates();
        }
        // this.navbarupdates();

        this.currentusername =  localStorage.getItem("currentUserName");
      
   }


   checkSubscriptionStatus(){
    if(this.subscription_status == "active"){
        return true;
    } else {
        return false;
    }
   }

    checkLogin() {
    if (localStorage.getItem("currentUserName")!="" && localStorage.getItem("token")!="") {
      return true;
    } else {
      return false;
    }


  }

  navbarupdates(){
        this.homepageservice.getTrendingCount()
        .subscribe(
            (data) => {
                this.counttrending = data;
                                    this.trending = this.counttrending.COUNT;                
                    },
                error => this.errorMessage = <any>error);  
      
        this.homepageservice.getUpdatesCount()
        .subscribe(
            (data) => {
                this.countupdates = data;
                this.updates = this.countupdates.COUNT;                         
                    },
                error => this.errorMessage = <any>error);  
   
        this.homepageservice.getTaskCount()
        .subscribe(
            (data) => {
                this.counttasks = data;
                this.tasks = this.counttasks.COUNT;             
                    },
                error => this.errorMessage = <any>error);  

                this.userid =  localStorage.getItem("userid");
                this.useridmodel.userid = this.userid;
                this.homepageservice.getMessages(this.useridmodel)
                        .subscribe(
                            (data) => {
                                this.allmessages = data;                    
                                    },
                                error => this.errorMessage = <any>error);  
                    

                this.homepageservice.getMessagesToday(this.useridmodel)
                    .subscribe(
                        (data) => {
                            this.messagestoday = data;   
                            },
                        error => this.errorMessage = <any>error);  
                            

                        this.homepageservice.getMessagesCountToday(this.useridmodel)
                                .subscribe(
                                    (data) => {
                                        this.messagescount = data;
                                            },
                                        error => this.errorMessage = <any>error);  
                            
             }

             onMenuClick() {
                this.renderer.setElementClass(this.el.nativeElement.querySelector('#navHeaderCollapse'), 'in', false);        
            }
    }