import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { ApiSettings } from '../ApiSettings';
import {Router} from '@angular/router';
import { IContactUs } from 'app/nav/footer/Contact Us/contactus';
import { ContactUsModel } from 'app/nav/footer/Contact Us/contactus.model';
import { Subject, Observable} from 'rxjs/RX';

@Injectable()
export class NavBarService {
  tokenmember: string;
  constructor (private http:Http,private router:Router) {

  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
 
  private extractData(res: Response) {
      let body = res.json();
      return body.fields || { };
  }

  checkSession(token) {
    let userUrl = ApiSettings.API_ENDPOINT + '/rest/user/requsersInfo.on';
    let headers2 = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+token});

    

    return this.http.get(userUrl, {headers: headers2});
  }

                                                // contactus service


  private postContactUsurl =  ApiSettings.API_ENDPOINT + '/users/user/contactUsSending.on';
  postContactUs(contactUsModel: ContactUsModel) : Observable<any> {
          console.log('contact us model'+JSON.stringify(contactUsModel));
          let body = JSON.stringify(contactUsModel);
          let headers = new Headers({ 'Content-Type': 'application/json' });
          let options = new RequestOptions({ headers: headers });
   
          return this.http.post(this.postContactUsurl, body, options)
                          .map(this.extractData)
                          .catch(this.handleError);
      }
}
