import { Component, Input } from '@angular/core'
import { ContactUsModel } from 'app/nav/footer/Contact Us/contactus.model';
import { IContactUs } from 'app/nav/footer/Contact Us/contactus';
import { NavBarService } from 'app/nav';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'toastr-ng2';

@Component({
   selector: 'ContactUs-page',
   templateUrl:'ContactUs.component.html',
   styleUrls: ['ContactUs.component.css']
})
export class ContactUsComponent {

    model = new ContactUsModel('','','','')
 constructor(private toastrService: ToastrService, private navbarservice: NavBarService){
    
 }
 errorMessage:string;
 
 contactus:IContactUs[];

 isValid:boolean = true;

 isValidForm() {
    return this.isValid;
}

  
 ngOnInit() {
 
      }
    
      submitForm(form: NgForm){

          this.isValid = false;
       
             this.navbarservice.postContactUs(this.model)
            
               .subscribe(
                  data => {
                      console.log('success: ', data)
                  this.toastrService.success('Submit Successful! ');
                  form.reset();

                  this.isValid = true;

                },
                  
                  err => console.log('error: ', err)
              );
              //  console.log("create new project model"+JSON.stringify(this.model));
          }
}