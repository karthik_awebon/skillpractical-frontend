import { Component } from '@angular/core'
import { LoginService } from '../user/services/login.service'
import {Router} from '@angular/router';

@Component({
    selector:'landing-page',
    templateUrl:'landingpage.component.html',
      styleUrls: ['./navbar.component.css']

})
export class LandingComponent{
 
  currentusername:string;

   constructor (private loginService:LoginService,private router:Router) {
        this.currentusername =  localStorage.getItem("currentUserName");
  }

  ngOnInit() {


  }

  learnmoreusertrackingtrueorfalse:boolean;
  learnmoreusertracking(){
    this.learnmoreusertrackingtrueorfalse = true;
  }

  learnmoreprojectstrueorfalse : boolean;
  learnmoreprojects(){
    this.learnmoreprojectstrueorfalse = true;
  }

  learnmoreeventstrueorfalse : boolean;
  learnmoreevents(){
    this.learnmoreeventstrueorfalse = true;
  }

  learnmorecommunitytrueorfalse : boolean;
  learnmorecommunity(){
    this.learnmorecommunitytrueorfalse = true;
  }


}