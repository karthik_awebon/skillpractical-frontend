export class RequestWorkshopModel{
    constructor(
        public workshop_id: string,
        public workshop_title: string,
        public requester_name: string,
        public requester_email: string,
        public requester_phone: string,
        public workshop_date: string,
        public workshop_venue: string,
        public attend_count: string,
        public price: number,
        public tutor_id:string,
        public tutor_name:string,
        public paytype:string,
        public type:string
    ){}
  }
    