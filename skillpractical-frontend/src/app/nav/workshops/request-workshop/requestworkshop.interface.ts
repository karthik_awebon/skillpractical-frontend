export interface IRequestWorkshop {
    workshop_id: string;
    workshop_title : string;
    requester_name :string;
    requester_email:string;
    requester_phone :string;
    workshop_date :string;
    workshop_venue :string;
    attendtype :string;
    attend_count :string;
    price :string;
    
  }