export class CheckConformedTutorsByWorkshopIdModel{
    constructor(
        public workshop_id: string,
        public workshop_date: string
    ){}
  }
    