import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestWorkshopComponent } from './request-workshop.component';

describe('RequestWorkshopComponent', () => {
  let component: RequestWorkshopComponent;
  let fixture: ComponentFixture<RequestWorkshopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestWorkshopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestWorkshopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
