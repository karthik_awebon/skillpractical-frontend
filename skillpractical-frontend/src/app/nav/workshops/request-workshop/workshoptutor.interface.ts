
export interface IWorkshopTutor {

    tutor_id:string;
    workshop_id:string;
    tutor_name:string;
    tutor_designation;
    tutor_description:string;
    tutor_workshop_count:string;
    tutor_rating:string;
    tutor_education:string;
    tutor_certification:string;
    tutor_price:string;
    

}