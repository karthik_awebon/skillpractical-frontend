import { Component, OnInit, Input } from '@angular/core';
import { WorkshopService } from 'app/nav/workshops/workshop.service';
import { RequestWorkshopModel } from 'app/nav/workshops/request-workshop/requestworkshop.model';
import { ToastrService } from 'toastr-ng2';
import { IRequestWorkshop } from 'app/nav/workshops/request-workshop/requestworkshop.interface';
import { WorkshopIdModel } from 'app/nav/workshops/workshop-detail/workshopid.model';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { IWorkshop } from 'app/nav/workshops/book-workshop/workshop.interface';
import { IWorkshopTutor } from './workshoptutor.interface';
import { Directive, ElementRef} from '@angular/core';
import { ViewChild } from '@angular/core';
import { WorkshopTutorAvailabilityModel } from './checkTutorAvailability.model';
import { ICheckTutorAvailability } from './checktutoravailability.interface';
import { CheckConformedTutorsByWorkshopIdModel } from './checkConformedTutorsByWorkshopId.model';

import { DatePipe } from '@angular/common'
import {Router} from '@angular/router';

@Component({
  selector: 'app-request-workshop',
  templateUrl: './request-workshop.component.html',
  styleUrls: ['./request-workshop.component.css']
})
export class RequestWorkshopComponent implements OnInit {
@Input() workshop:IWorkshop[];

@ViewChild('closeBtn') closeBtn: ElementRef;

  subscription_type:string;
  private sub: Subscription;
  constructor(private router:Router,private _route: ActivatedRoute,private toastrService: ToastrService,private workshopService: WorkshopService,
    public datepipe: DatePipe) { }
  model = new RequestWorkshopModel('','','','','','','','',null,null,'','null','');
  requestworkshop: IRequestWorkshop[];
  modelworkshopid = new WorkshopIdModel("");
  checkConformedTutorsByWorkshopIdModel = new CheckConformedTutorsByWorkshopIdModel("","");
  modeltutoravailability = new  WorkshopTutorAvailabilityModel('','','');
  haspaytypeError = false;
  hasattendcountError = false;
  errorMessage: string;
  noofteams = false;
  noofstudentinclass = false;
  inclass : string;
  teamofmembers : string;
  class:string;
  disableattend_count:boolean;
  disableprice:boolean;
  price : number;
  workshop_id : string;
  workshop_title :string;
  type:string;

  workshoptutors:IWorkshopTutor[];
  conformedtutors:IRequestWorkshop[];
  workshoptutoravailability: ICheckTutorAvailability;

  selectedtutor:string;

  checkTutorAvalabilitytutor_id:string;

  hiretutorstyle:any;
  
  status:string = "available";

  ngOnInit() {
    this.sub = this._route.params.subscribe(
      params => {
        let workshop_id = params['workshop_id']
        let workshop_title = params['workshop_title']
        let type = params['type']
          this.workshop_id = workshop_id;
          this.workshop_title = workshop_title;
          this.type = type;
          console.log("id"+workshop_id);
          console.log("title"+workshop_title);
   });     
                
    this.modelworkshopid.workshop_id = this.workshop_id;
      this.workshopService.getWorkshopTutorsById(this.modelworkshopid)
      .subscribe(
      (data) => {
          this.workshoptutors = data; 

        //  for(let result of this.workshoptutors){
        //     // ids.push(result.tutor_id);
        //  }

          console.log("model workshopid from workshop tutor component"+this.modelworkshopid);
          console.log("workshop id"+this.workshop_id);
        }); 
  }

  date : Date;

  showhirebutton : boolean = false;
  callWorkshopDate(value){
      // this.checkConformedTutorsByWorkshopIdModel.workshop_date = value;
      
      //formatting date with date pipe
           this.date=new Date();
          let latest_date =this.datepipe.transform(this.date, 'yyyy-MM-dd');

        if(value < latest_date){
          this.toastrService.error('Workshop date shouldnt be lesser than current date!');
        }else {
          this.modeltutoravailability.workshop_date = value;
          this.showhirebutton = true;
        }
          console.log("displaydate"+ latest_date);
          console.log("selected date"+value);
      // this.checkConformedTutors();
  }

  // checkConformedTutors(){
  //   this.checkConformedTutorsByWorkshopIdModel.workshop_id = this.workshop_id;
  //   this.workshopService.checkConformedTutorsByWorkshopId(this.checkConformedTutorsByWorkshopIdModel)
  //   .subscribe(
  //   (data) => {
  //       this.conformedtutors = data; 

  //       console.log("conformed tutors" + this.conformedtutors);
  //     }); 
  // }

  tutoravailability:boolean;

  selectedtutor_id:string;
  selectedtutor_name:string;

 selectedtutorprice:any;

 tutorselected:boolean = false;

    checkTutorAvalability(tutor_id,tutor_name,tutor_price){
          this.modeltutoravailability.workshop_id = this.workshop_id;
          this.modeltutoravailability.tutor_id = tutor_id;
          this.workshopService.getWorkshopTutorAvailabilityByWorkshopIdAndTutorIdAndWorkshopDate(this.modeltutoravailability)
          .subscribe(
          (data) => {
              this.workshoptutoravailability = data; 
              this.checkTutorAvalabilitytutor_id = this.workshoptutoravailability.tutor_id;

              if(this.workshoptutoravailability.tutor_id == ""){
                  this.tutoravailability = true;
                  console.log("Tutor is available..");
                  this.selectTutor(tutor_id,tutor_name);

                  this.selectedtutorprice = tutor_price;

                  this.tutorselected = true;

              }
              else {
                this.tutoravailability = false;
                console.log("Tutor is not available");
                this.toastrService.error("Tutor "+ tutor_name + " is not Available on "+this.modeltutoravailability.workshop_date);
              }
              
              console.log("workshop tutor availability check tutor id" + this.checkTutorAvalabilitytutor_id);
          });
      }

    
    
    selectTutor(tutor_id,tutor_name){
      this.model.tutor_id = tutor_id;
      this.model.tutor_name = tutor_name;

      this.selectedtutor = tutor_name;
      console.log(this.model);

      this.closeBtn.nativeElement.click();
}

validatepaystype(value){
    this.disableattend_count = true;
    if (value === 'default')
    this.haspaytypeError = true;
    else
    this.haspaytypeError = false;
  }

  validateattend_count(value){
    this.disableprice = true;
    
    if (value === 'default')
    this.hasattendcountError = true;
    else
    this.hasattendcountError = false;
  }

  callTypeattend_count(value){
    console.log("value display"+value);
  
      this.price = value * this.selectedtutorprice;
        console.log("price"+this.price);
    

    }


  submitForm(form: NgForm) {
    this.model.price = this.price;
    this.model.workshop_id = this.workshop_id;
    this.model.workshop_title = this.workshop_title;
    this.model.type = this.type;
        this.workshopService.postRequestWorkshopForm(this.model)
                      .subscribe(
                        data => {               
                          this.toastrService.success('Request Successful!');
                          form.reset();
                          console.log('success: ')

                          this.router.navigate(['/bookworkshop']);

                        },
                        err => console.log('error: ', err)
                      );
                      console.log("request workshop form model"+JSON.stringify(this.model));
  }
 
}
