import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import {Router} from '@angular/router';
import { IContactUs } from 'app/nav/footer/Contact Us/contactus';
import { ContactUsModel } from 'app/nav/footer/Contact Us/contactus.model';
import { Observable} from 'rxjs/RX';
import { ApiSettings } from 'app/ApiSettings';
import { IWorkshop } from 'app/nav/workshops/book-workshop/workshop.interface';
import { WorkshopAreaModel } from 'app/nav/workshops/book-workshop/workshop.model';
import { WorkshopIdModel } from 'app/nav/workshops/workshop-detail/workshopid.model';
import { IImageslideurl } from './workshop-detail/imageslideurl.interface';
import { IFeedbackurl } from './workshop-detail/userfeedbackworkshop.interface';
import { IRequestWorkshop } from 'app/nav/workshops/request-workshop/requestworkshop.interface';
import { RequestWorkshopModel } from 'app/nav/workshops/request-workshop/requestworkshop.model';
import { IWorkshopTutor } from './request-workshop/workshoptutor.interface';
import { WorkshopTutorAvailabilityModel } from './request-workshop/checkTutorAvailability.model';
import { ICheckTutorAvailability } from './request-workshop/checktutoravailability.interface';
import { CheckConformedTutorsByWorkshopIdModel } from './request-workshop/checkConformedTutorsByWorkshopId.model';


@Injectable()
export class WorkshopService {
  
    tokenmember: string;
    constructor (private http:Http,private router:Router,private _http: Http) {

    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
    
    private extractData(res: Response) {
        let body = res.json();
        return body.fields || { };
    }

  
    private _workshopUrl = ApiSettings.API_ENDPOINT+'/workshops/bookworkshops/getAllWorkshopList.on';

    getWorkshops(): Observable<IWorkshop[]> {
        console.log("workshops model ..");          
        let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '});
        let options = new RequestOptions({headers: headers});
        return this._http.get(this._workshopUrl,options)
            .map((response: Response) => <IWorkshop[]> response.json().allworkshops_list)
            .do(data => console.log('All workshops: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    private urlworkshopsbyarea = ApiSettings.API_ENDPOINT + '/workshops/bookworkshops/getWorkshopListByArea.on';
    getWorkshopsByArea(workshopAreaModel:WorkshopAreaModel): Observable<any> {
        console.log("workshop by branch model .."+JSON.stringify(workshopAreaModel));
         let body = JSON.stringify(workshopAreaModel);
            let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '});
            let options = new RequestOptions({headers: headers});
        return this._http.post(this.urlworkshopsbyarea,body,options)
            .map((response: Response) => <IWorkshop[]> response.json().Workshops_list_By_Area)
            .do(data => console.log('workshops by Area ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    private url = ApiSettings.API_ENDPOINT + '/workshops/bookworkshops/getWorkshopDetailsByWorkshopId.on';
    getWorkshopById(workshopIdModel:WorkshopIdModel): Observable<any> {
        console.log("model.."+JSON.stringify(workshopIdModel));
         let body = JSON.stringify(workshopIdModel);
            let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '});
            let options = new RequestOptions({headers: headers});
        return this._http.post(this.url,body,options)
            .map((response: Response) => <IWorkshop[]> response.json().WorkshopDetailById)
            .do(data => console.log('Workshop details by id: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    private workshopimageurl = ApiSettings.API_ENDPOINT + '/workshops/bookworkshops/getWorkshopSlideImageurlsByWorkshopId.on';
    getworkshopimage(workshopIdModel:WorkshopIdModel): Observable<any> {
      console.log("model.."+JSON.stringify(workshopIdModel));
       let body = JSON.stringify(workshopIdModel);
          let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '});
          let options = new RequestOptions({headers: headers});
      return this._http.post(this.workshopimageurl,body,options)
          .map((response: Response) => <IImageslideurl[]> response.json().WorkShopSlideImageUrls)
          .do(data => console.log('Workshop image by id: ' +  JSON.stringify(data)))
          .catch(this.handleError);      
    }

    private feedbackurl = ApiSettings.API_ENDPOINT + '/workshops/bookworkshops/getWorkshopUserFeedbackByWorkshopId.on';
    getfeedbackbyworkshopid(workshopIdModel:WorkshopIdModel): Observable<any> {
        console.log("model.."+JSON.stringify(workshopIdModel));
        let body = JSON.stringify(workshopIdModel);
            let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '});
            let options = new RequestOptions({headers: headers});
        return this._http.post(this.feedbackurl,body,options)
            .map((response: Response) => <IFeedbackurl[]> response.json().WorkshopUserFeedBack)
            .do(data => console.log('Workshop feedback by id: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    private requestworkshopurl = ApiSettings.API_ENDPOINT + '/workshops/bookworkshops/requestBookWorkshop.on';
    postRequestWorkshopForm(requestWorkshopModel: RequestWorkshopModel) : Observable<any> {
        let body = JSON.stringify(requestWorkshopModel);
        let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '});
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.requestworkshopurl, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
    }
    

    private urlworkshoptutorbyid = ApiSettings.API_ENDPOINT + '/workshops/bookworkshops/getWorkshopTutorsByWorkshopId.on';
    getWorkshopTutorsById(workshopIdModel:WorkshopIdModel): Observable<any> {
        console.log("model workshopidmodel.."+JSON.stringify(workshopIdModel));
        let body = JSON.stringify(workshopIdModel);
            let headers = new Headers({'Content-Type': 'application/json'});
            let options = new RequestOptions({headers: headers});
        return this._http.post(this.urlworkshoptutorbyid,body,options)
            .map((response: Response) => <IWorkshopTutor[]> response.json().WorkshopTutorByWorkshopId)
            .do(data => console.log('Workshop tutors by workshopid: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    private urlWorkshopTutorAvailabilityByWorkshopIdAndTutorIdAndWorkshopDate = ApiSettings.API_ENDPOINT + '/workshops/bookworkshops/checkTutorAvailabilityByWorkshopIdAndTutorId.on';
    getWorkshopTutorAvailabilityByWorkshopIdAndTutorIdAndWorkshopDate(WorkshopTutorAvailabilityModel:WorkshopTutorAvailabilityModel): Observable<any> {
        console.log("model WorkshopTutorAvailabilityModel.."+JSON.stringify(WorkshopTutorAvailabilityModel));
        let body = JSON.stringify(WorkshopTutorAvailabilityModel);
            let headers = new Headers({'Content-Type': 'application/json'});
            let options = new RequestOptions({headers: headers});
        return this._http.post(this.urlWorkshopTutorAvailabilityByWorkshopIdAndTutorIdAndWorkshopDate,body,options)
            .map((response: Response) => <ICheckTutorAvailability> response.json().TutorAvailability)
            .do(data => console.log('Workshop tutors availability by workshopid, tutorid and workshop date service:' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }
    


    private urlcheckConformedTutorsByWorkshopId = ApiSettings.API_ENDPOINT + '/workshops/bookworkshops/checkConformedTutorsByWorkshopId.on';
    checkConformedTutorsByWorkshopId(CheckConformedTutorsByWorkshopIdModel:CheckConformedTutorsByWorkshopIdModel): Observable<any> {
        console.log("check workshop tutor availability.."+JSON.stringify(CheckConformedTutorsByWorkshopIdModel));
        let body = JSON.stringify(CheckConformedTutorsByWorkshopIdModel);
            let headers = new Headers({'Content-Type': 'application/json'});
            let options = new RequestOptions({headers: headers});
        return this._http.post(this.urlcheckConformedTutorsByWorkshopId,body,options)
            .map((response: Response) => <IRequestWorkshop[]> response.json().ConformedTutorByWorkshopId)
            .do(data => console.log('check conformed tutors by workshop id and date.' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

}
