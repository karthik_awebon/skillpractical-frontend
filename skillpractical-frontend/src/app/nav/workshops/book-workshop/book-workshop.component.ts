import { Component, OnInit } from '@angular/core';
import { IWorkshop } from 'app/nav/workshops/book-workshop/workshop.interface';
import { WorkshopAreaModel } from 'app/nav/workshops/book-workshop/workshop.model';
import { WorkshopService } from 'app/nav/workshops/workshop.service';

@Component({
  selector: 'app-book-workshop',
  templateUrl: './book-workshop.component.html',
  styleUrls: ['./book-workshop.component.css']
})
export class BookWorkshopComponent implements OnInit {
  errorMessage: string;
  title:string;
  workshop:IWorkshop[];
  selectedTag:string = 'All';
  modelworkshoparea = new WorkshopAreaModel("","");
  constructor(private workshopService: WorkshopService) { }
  visibleworkshop : IWorkshop[];
  listFilter:any;
  selectedevent:string;

  p: number = 1;

  ngOnInit() {

    this.filterByall();

   
  }
  filterByall(){
    this.title = 'All';
    this.AllWorkshops();
    console.log("all clicked")
}

filterBysoftware(){
   this.title = 'Software';
   this.filterByArea();
}

filterByelectrical(){
  this.title = 'Electrical';
  this.filterByArea();
}

filterByelectronics(){
  this.title = 'Electronics';
   this.filterByArea();  
}

filterBymechanical(){
  this.title = 'Mechanical';
   this.filterByArea();
}

filterBycivil(){
    this.title = 'Civil';   
     this.filterByArea();
}

filterByrobotics(){
  this.title = 'Robotics';  
 this.filterByArea();
}

filterByIot(){
  this.title = 'IOT';
 this.filterByArea();
}

filterByaeromodelling(){
  this.title = 'Aeromodelling';  
 this.filterByArea();
}

filterBymechatronics(){
  this.title = 'Mechatronics';
 this.filterByArea();
}

filterByautomobile(){
  this.title = 'Automobile';
 this.filterByArea();
}

filterByArea(){
  this.modelworkshoparea.workshop_area = this.title
  this.workshopService.getWorkshopsByArea(this.modelworkshoparea)
      .subscribe(
          (data) => {
          this.visibleworkshop = data;
          console.log("workshops by area....."+JSON.stringify(this.visibleworkshop));                  
          },
      error => this.errorMessage = <any>error) 
}

AllWorkshops(){
  this.workshopService.getWorkshops()
  .subscribe(
      (data) => {
      this.visibleworkshop = data;
      console.log(" All workshops....."+JSON.stringify(this.visibleworkshop))                  
      },
  error => this.errorMessage = <any>error)  
}

}
