import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookWorkshopComponent } from './book-workshop.component';

describe('BookWorkshopComponent', () => {
  let component: BookWorkshopComponent;
  let fixture: ComponentFixture<BookWorkshopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookWorkshopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookWorkshopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
