export interface IWorkshop {
    workshop_area: string;
    workshop_id: string;
    workshop_title : string;
    workshop_description :string;
    days:string;
    start_price:string;
    kit :string;
    certificate :string;
    topicscovered :string;
    outcomes :string;
    workshop_imageurl : string;
    type:string;
    
  }