import { PipeTransform, Pipe } from '@angular/core'
import { IWorkshop } from 'app/nav/workshops/book-workshop/workshop.interface';




@Pipe({
    name: 'workshopFilter'
})
export class WorkshopFilterPipe implements PipeTransform {

         transform(value: IWorkshop[], filterBy: string): IWorkshop[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        
        return filterBy ? value.filter((workshop: IWorkshop) =>
        workshop.workshop_title.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }
}