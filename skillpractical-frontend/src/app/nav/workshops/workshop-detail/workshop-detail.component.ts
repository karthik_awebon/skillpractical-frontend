import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { IWorkshop } from 'app/nav/workshops/book-workshop/workshop.interface';
import { WorkshopIdModel } from 'app/nav/workshops/workshop-detail/workshopid.model';
import { WorkshopService } from 'app/nav/workshops/workshop.service';
import { IImageslideurl } from 'app/nav/workshops/workshop-detail/imageslideurl.interface';
import { IFeedbackurl } from 'app/nav/workshops/workshop-detail/userfeedbackworkshop.interface';
@Component({
  selector: 'app-workshop-detail',
  templateUrl: './workshop-detail.component.html',
  styleUrls: ['./workshop-detail.component.css']
})
export class WorkshopDetailComponent implements OnInit {
  workshop:IWorkshop;
  private sub: Subscription;
  modelworkshopid = new WorkshopIdModel("");
  inclass : string = "inclass";
  teamofmembers : string= "teamofmembers";
  workshop_id: string;
  price: number= 625;
  constructor(private route: ActivatedRoute,private workshopService: WorkshopService) { }
  workshop_imageurl: string;
  imageslideurl:IImageslideurl[];
  feedbackurl:IFeedbackurl[];
  workshop_slide_imageurl : string;
  teamprice:string;
  classprice :string;
  workshop_title: string;
  type:string;

  filterByall:any;
  filterByrobotics:any;
  filterByIot:any;
  filterByelectrical:any;
  filterByelectronics:any;
  filterBymechanical:any;
  filterByaeromodelling:any;
  filterBymechatronics:any;
  filterBycivil:any;
  filterByautomobile:any;


  imageslideurl0:string;
  imageslideurl1:string;
  imageslideurl2:string;

  

  pageUrl = 'http://www.crackingthecodinginterview.com/';
  pageIdentifier = 'pageid1';

  update(pageIdentifier: string, pageUrl: string) {
      this.pageIdentifier = pageIdentifier;
      this.pageUrl = pageUrl;
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(
      params => {
      
          let id = params['id'];
          let workshop_title = params['workshop_title']
          let type = params['type']
          // let subject = params['subject']
          this.workshop_id = id;
          this.workshop_title = workshop_title;
          this.type = type;
          // this.subject = subject;
          console.log("workshop_id.."+this.workshop_id);
          console.log("workshop_title.."+this.workshop_title);
        }) 
          this.modelworkshopid = new WorkshopIdModel(this.workshop_id);
            this.workshopService.getWorkshopById(this.modelworkshopid)
                            .subscribe(
                            (data) => {
                                this.workshop = data; 
    });  
    this.workshopService.getworkshopimage(this.modelworkshopid)
    .subscribe(
      data => {
        this.imageslideurl = data;
         this.imageslideurl0 = this.imageslideurl[0].workshop_slide_imageurl;
         this.imageslideurl1 = this.imageslideurl[1].workshop_slide_imageurl;
         this.imageslideurl2 = this.imageslideurl[2].workshop_slide_imageurl;
            console.log('image slide url: ', data)
      },
      err => console.log('error: ', err)
    );
    this.workshopService.getfeedbackbyworkshopid(this.modelworkshopid)
    .subscribe(
      data => {
    this.feedbackurl = data;
    console.log('feedback url: ', data)
            console.log('success: ', data)
      },
      err => console.log('error: ', err)
    );

        this.update(this.pageIdentifier, this.pageUrl);
 
  }


  goBack() {
    window.history.back();
  }
}
