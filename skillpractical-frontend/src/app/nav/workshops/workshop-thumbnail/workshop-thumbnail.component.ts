import { Component, OnInit, Input } from '@angular/core';
import { WorkshopthumbnailAreaModel } from 'app/nav/workshops/workshop-thumbnail/workshop-thumbnail.model';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { IWorkshop } from 'app/nav/workshops/book-workshop/workshop.interface';

@Component({
  selector: 'app-workshop-thumbnail',
  templateUrl: './workshop-thumbnail.component.html',
  styleUrls: ['./workshop-thumbnail.component.css']
})
export class WorkshopThumbnailComponent implements OnInit {

  @Input() workshop:IWorkshop;
  private sub: Subscription;

  modelallworkshop = new WorkshopthumbnailAreaModel("");
  workshop_id: string;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(
      params => {
      
          let id = params['id'];
          // let subject = params['subject']
          this.workshop_id = id;
          // this.subject = subject;
          console.log("workshop_id.."+this.workshop_id);
          // console.log("subject area.."+this.subject);
      });
  }
 
}
