import {Routes} from '@angular/router'

import {EventListComponent,
        EventDetailsComponent,
        EventListResolver
        ,EventResolver,MyCertificatesComponent
 } from './events/index'

 import {
    
        HomePageComponent,PastReportsComponent,MessagesListComponent, AlltasksComponent
      
    } from './homepage/index'

import {LoginGuard}  from './user/routerguard/loginguard.component'

import {termsandconditionsComponent, privacypolicyComponent, FAQcomponent,ContactUsComponent,LandingComponent,FeaturesComponent,AboutusComponent, MembershipInfoComponent} from './nav/index'

import { Error404Component } from './errors/404.component'

import {SettingUserComponent} from './user/setting.component';
import {ForgotPassword} from './user/forgotpassword.component';
import { MessageDetailComponent } from 'app/homepage/messages/messages-detail/messages-detail.component';
import { PreventLoggedInAccess } from 'app/user/routerguard/preventroutesloggedin.component';
import { GeneralComponent } from 'app/user/general/general.component';
import { EducationComponent } from 'app/user/education/education.component';
import { ResumeLinksComponent } from 'app/user/resume-links/resume-links.component';
import { UserskillsComponent } from 'app/user/userskills/userskills.component';
import { ChangepasswordComponent } from 'app/user/changepassword/changepassword.component';
import { UpgradeComponent } from './user/upgrade/upgrade.component';
import { OrderDetailsComponent } from './user/order-details/order-details.component';
import { BookWorkshopComponent } from 'app/nav/workshops/book-workshop/book-workshop.component';
import { WorkshopDetailComponent } from 'app/nav/workshops/workshop-detail/workshop-detail.component';
import { RequestWorkshopComponent } from './nav/workshops/request-workshop/request-workshop.component';
import { BlogComponent } from './blog/blog/blog.component';
import { BlogDetailComponent } from './blog/blog-detail/blog-detail.component';
import { SearchTestComponent } from 'app/Tests/view/searchtest/searchtest.component';
import { TestComponent } from 'app/Tests/view/home/home.component';
import { TestInstructionComponent } from 'app/Tests/view/testinstruction/testinstruction.component';
import { TestDetailComponent } from 'app/Tests/view/testdetail/testdetail.component';



export const appRoutes:Routes = [
    
    {path: 'events' , children:[
            {path: '',canActivate: [LoginGuard], component: EventListComponent},
            {path: 'events/:id',canActivateChild: [LoginGuard], component: EventDetailsComponent},
            {path:'mycertificates',canActivate: [LoginGuard],component: MyCertificatesComponent}
        ]},
    {path: '404', component: Error404Component},

    { path: 'projects', loadChildren: 'app/projects/projects.module#ProjectsModule'},
    // { path: 'user', loadChildren:'app/user/user.module#UserModule'},
    { path: 'community', loadChildren:'app/community/community.module#CommunityModule'},
    { path: 'tests', loadChildren:'app/Tests/tests.module#TestsModule'},
    { path: 'diyprojects', loadChildren:'app/diyprojects/diyprojects.module#DIYProjectsModule'},
    
    
  
    // {path: 'homepage', loadChildren:'app/homepage/homepage.module#HomepageModule'},
    {path:'landingpage', component:LandingComponent, canActivate: [PreventLoggedInAccess] },
    {path:'features', component:FeaturesComponent,canActivate: [PreventLoggedInAccess]},
    {path:'aboutus', component:AboutusComponent,canActivate: [PreventLoggedInAccess]},
    {path: 'faqs', component:FAQcomponent,canActivate: [PreventLoggedInAccess]},
    {path:'contactus', component:ContactUsComponent,canActivate: [PreventLoggedInAccess]},
    {path: 'privacypolicy', component: privacypolicyComponent,canActivate: [PreventLoggedInAccess]},
    {path: 'termsandConditions', component: termsandconditionsComponent,canActivate: [PreventLoggedInAccess]},
    {path:'forgotPassword', component:ForgotPassword,canActivate: [PreventLoggedInAccess]},
    {path: 'membershipinfo', component:MembershipInfoComponent,canActivate: [PreventLoggedInAccess]},
    {path: 'blogs', component: BlogComponent, canActivate: [PreventLoggedInAccess]},
    {path: 'blogdetail/:blogpost_title', component: BlogDetailComponent,canActivate: [PreventLoggedInAccess]},
    {path: 'bookworkshop', component:BookWorkshopComponent,canActivate: [PreventLoggedInAccess]},
    {path: 'upgradeuser', component:UpgradeComponent},
    {path: 'homepagecomponent', canActivate:[LoginGuard], component: HomePageComponent},
    {path: 'homepagecomponent/viewpastreportsgraph', canActivate:[LoginGuard], component:PastReportsComponent},
    {path:'homepagecomponent/viewallmessages',canActivate:[LoginGuard], component:MessagesListComponent},
    {path: 'viewalltasks',canActivate:[LoginGuard], component:AlltasksComponent},
    {path: 'settinguser', component:SettingUserComponent, canActivate:[LoginGuard] },
    {path: 'generalform', component:GeneralComponent,  canActivate:[LoginGuard]},
    {path: 'educationform', component:EducationComponent,  canActivate:[LoginGuard]},
    {path: 'resumelinksform', component:ResumeLinksComponent,  canActivate:[LoginGuard]},
    {path: 'userskillsform', component: UserskillsComponent,  canActivate:[LoginGuard]},
    {path: 'changepasswordform',component:ChangepasswordComponent,  canActivate:[LoginGuard]},
    {path: 'messagedetail/:id',component:MessageDetailComponent,  canActivate:[LoginGuard]},
    {path: 'orderdetails/:subscription_type/:pricing', component:OrderDetailsComponent },
    {path: 'workshopdetail/:id/:workshop_title/:type', component: WorkshopDetailComponent},
    {path: 'requestworkshop/:workshop_title/:workshop_id/:type' , component: RequestWorkshopComponent},
   // { path: '', redirectTo: '/landingpage', pathMatch: 'full' },

    { path: '', redirectTo: '/homepagecomponent', canActivate:[LoginGuard], pathMatch: 'full' }
]