import { IPost } from './interfaces/postInterface';

export class UserCommunity {
  public userid: number;
  public firstName: string;
  public lastName: string;
  public userName: string;
  public password: string;
  public created: Date;
  // public postList: IPost[];
  // public likedPostList: IPost[];
}
