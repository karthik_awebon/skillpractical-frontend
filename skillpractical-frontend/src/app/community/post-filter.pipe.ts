import { PipeTransform, Pipe } from '@angular/core'

import { IPost } from './interfaces/postInterface';

@Pipe({
    name: 'postsFilter'
})
export class PostsFilterPipe implements PipeTransform {

         transform(value: IPost[], filterBy: string): IPost[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        
        return filterBy ? value.filter((post: IPost) =>
            post.postName.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }
}