export class NewPostModel{
    constructor(
        
        public postName: string,
        public branch: string,
        public description: string,
        public username: string,
        public userid: string,
        public tag: string,
        //this is for extra safety for post insert
        public tagid:string,
        public sharewith: string,
        public key1: string,
        public signedUrl1:string
    
    ){
        // this.postfileurl = 'https://s3-us-west-2.amazonaws.com/communityyy/amazon-logo-tiny._CB138348653_.gif';
    }
}