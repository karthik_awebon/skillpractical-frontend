import { Component, Input, ViewContainerRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import {Post} from './post';
// import {UploadPhotoService} from '../services/upload-photo.service';
import {AddPostService} from './myposts/add-post.service';
import {UserService} from './user.service';
import {UserCommunity} from './usercommunity';
import { NgForm } from '@angular/forms';
import { UserNameModel } from './username.model'
import { NewPostModel } from './newpost.model'
import { IUser } from './interfaces/userInterface'
import {PostService} from './post.service';
import {Ikeyandurlforfile} from './interfaces/postresponse.interface';
import {Router} from '@angular/router';
import {HomeComponent} from './home.component'
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import 'aws-sdk/dist/aws-sdk';
import { Subject, Observable} from 'rxjs/RX';
import { HttpClient, HttpResponse, HttpEventType } from '@angular/common/http';
import { ApiSettings } from '../ApiSettings';
import { Directive, ElementRef} from '@angular/core'
import { ITag } from 'app/community/interfaces/posttagInterface';
import { ToastrService, ToastrConfig } from 'toastr-ng2';
import { TagBranchModel } from 'app/community/tagbranch.model';
import { TagModel } from 'app/community/tag.model';
import { UserIdModel } from '../homepage';
import { UpgradeService } from '../user/services/upgrade.service';
import { IUpgradeStatus } from '../user/shared/upgradestatus.interface';
@Component({
  selector: 'add-post',
  templateUrl: 'add-post.component.html',
  styleUrls: ['add-post.component.css']
})
export class AddPost {
  @Input() height: '100px';
  @ViewChild('closeBtn') closeBtn: ElementRef;
  
  branches = ['Electrical','Electronics','Computer Science','Civil','Mechanical','other'];
  tagname: any;

  postAdded: boolean = false;
  user: IUser;
  userid: string;
  currentusername:string;
  modelusername = new UserNameModel('');
  model = new NewPostModel('','','','',null,'',null,'','','');

  selectedFiles: FileList
  currentFileUpload: File
  fileuploaded:boolean;
  private uploadError:boolean = false;
  keyandurlforfile: Ikeyandurlforfile;
  fileurl:string;
  key:string;
  htmlContent:any;

  // branch:string;
  hasTagError = false;
  hasBranchError = false;
  tags:ITag[];
  errorMessage:string;
  modeltagbybarnch = new TagBranchModel('');
  modeltag = new TagModel('');

  useridmodel = new UserIdModel('');

  upgradeStatus:IUpgradeStatus;

  subscription_status:string;
  
  constructor ( private toastrService: ToastrService,private router:Router,private addPostservice: AddPostService, 
    private userService:UserService,private postService:PostService,private upgradeService:UpgradeService,
   private homecomponent:HomeComponent,private http : Http,private toastrConfig: ToastrConfig) {
    toastrConfig.timeOut = 2000;
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
  };

  }
    
  validateTag(value) {
    if (value === 'default')
      this.hasTagError = true;
    else
      this.hasTagError = false;
  }
  validateBranch(value){
    if (value === 'default')
    this.hasBranchError = true;
    else
    this.hasBranchError = false;
  }

  callTypeTagId(value){
    console.log("value display"+value);


  }

  gettagidbytag(value){
      
  }

  callType(value){
    console.log("seleted branch"+value);
      this.gettagsbybranch(value);
  
  }

  gettagsbybranch(value){
    this.modeltagbybarnch.tag_branch = value;
    this.postService.getAllTagsByBranch(this.modeltagbybarnch)
    .subscribe(
       (data) => {
          this.tags = data;
          
           console.log("tags....."+JSON.stringify(this.tags))                             
             },
          error => this.errorMessage = <any>error);
  }
  ngOnInit():void {

    let mathjax=document.getElementsByClassName('mathjax');
    eval('MathJax.Hub.Queue(["Typeset",MathJax.Hub, mathjax])');
   eval('MathJax.Hub.Queue(["Typeset",MathJax.Hub, mathjax])');
   
    
          this.currentusername =  localStorage.getItem("currentUserName");
     
        this.userid =  localStorage.getItem("userid");
        this.useridmodel.userid = this.userid;
        this.upgradeService.selectUserSubscriptionStatus(this.useridmodel).subscribe(
                  user => {
                    this.upgradeStatus = user;
                    this.subscription_status = this.upgradeStatus.subscription_status;
                   
                    console.log("user subscription status from new post"+this.subscription_status);
                
                    },
                    error => console.log(error)
                  ) 

  }
      // selectFile(event) {
      //   this.selectedFiles = event.target.files;
      // }
      
      // upload() {
      //   this.currentFileUpload = this.selectedFiles.item(0)
      //   this.addPostservice.pushFileToStorage(this.currentFileUpload).subscribe( 
      //     (data) => {
      //       this.keyandurlforfile = data;
            
      //       this.fileurl = this.keyandurlforfile.signedUrl1;
      //       this.key = this.keyandurlforfile.key1;
      //       this.fileuploaded = true;
      //       console.log("signed url  "+this.keyandurlforfile.signedUrl1);
      //       console.log("key   "+this.keyandurlforfile.key1);
      //   }, err => 
      //    {
      //     this.uploadError = true;
      //    }
          
      // )
      //   this.selectedFiles = undefined
      // }
    // postsuccess:boolean;

    checkSubscriptionStatus(){
      if(this.subscription_status == "active"){
          return true;
      } else {
          return false;
      }
     }

    submitForm(form: NgForm) {
      
        this.model.username = this.currentusername;
        this.model.userid = this.userid;
       this.model.key1 = this.key;
       this.model.signedUrl1 = this.fileurl;
        
        this.addPostservice.sendPost(this.model)
            .subscribe(
              data =>{
                this.postAdded = true;
                // this.postsuccess = true;
                this.closeBtn.nativeElement.click();
                this.toastrService.success('Your Question is Successfully Posted! ');
               
                this.homecomponent.ngOnInit(); //to update posts after creation.
                console.log('success: ', data)
                form.reset();
              } ,
              
              err => console.log('error: ', err)
            );
            console.log("New post model"+JSON.stringify(this.model));
      }
      closemodel(form){
        form.reset();
      }

      goToUpgrade() {
        this.closeBtn.nativeElement.click();
        
        this.router.navigate(['/upgradeuser']);
      }

      
}