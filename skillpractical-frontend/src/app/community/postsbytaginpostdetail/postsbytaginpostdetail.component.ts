import {Component, Input} from '@angular/core';
import {PostService} from '../post.service';
import {UserService} from '../user.service';
// import {UserCommunity} from '../usercommunity';
import {IPost} from '../interfaces/postInterface';

// import { UserNameModel } from '../username.model'
// import { UserIdModel } from '../userid.model'

import {  ActivatedRoute, Router } from '@angular/router';
import { Subscription }       from 'rxjs/Subscription';

import { NgForm } from '@angular/forms';
import { IUser } from '../interfaces/userInterface'

import {PostTagModel } from '../postsbytag/tag.model';
import { window } from 'rxjs/operator/window';
@Component({
  selector: 'posts-by-tag-inpostdetail',
  templateUrl: 'postsbytaginpostdetail.component.html',
  styleUrls: ['postsbytaginpostdetail.component.css']
})
export class PostsByTagInPostDetail {
  @Input() tag: string;

  post : IPost[];
  errorMessage:string;

    posttag:string;
    private sub: Subscription;
    modeltag = new PostTagModel('');
  
constructor (private router: Router, private route: ActivatedRoute,private userService: UserService, private postService: PostService, private _router: Router){

    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
  };
  
}
   ngOnInit():void {       

            this.modeltag = new PostTagModel(this.tag);
            
            this.postService.getPostsListByTagAndTrending(this.modeltag)
                    .subscribe(
                        (data) => {
                          this.post = data;
                          console.log("posts from post tag....."+JSON.stringify(this.post))                  
                          },
                      error => this.errorMessage = <any>error)  
  
   }

  //  onclickrelatedpost(): void{
  //     location.reload();
  //  }
}
