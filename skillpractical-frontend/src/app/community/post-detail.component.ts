import {Component, Input} from '@angular/core';
import {IPost} from './interfaces/postInterface';
import {PostService} from './post.service';
import {PostComments} from './post-comments.component';
import {UserService} from './user.service';
import {  ActivatedRoute, Router } from '@angular/router';
import { Subscription }       from 'rxjs/Subscription';
import { PostIdModel} from './postid.model';
import { UserNameModel } from './username.model';
import { IUser } from './interfaces/userInterface';

import {SavePostModel} from './savepost.model';

import {UnSavePostModel} from './unsavepost.model';

import {CheckSavedPostModel} from './checksavedpost.model';

import {ICheckSavedPost} from './checksavedpost.interface';


@Component({
  selector: 'image-detail',
  styleUrls: ['post-detail.component.css'],
  templateUrl: 'post-detail.component.html'
  
})
export class PostDetail {
 
  post:IPost;
  like: string;
  user: IUser;
  postid:number;
  tag:string;
  private sub: Subscription;
  currentusername: string;
  model = new PostIdModel(null);
  model1 = new UserNameModel('');

  modelusername = new UserNameModel('');

  savepostmodel = new SavePostModel(null,'','','','','','','','','','');

  unsavepostmodel = new UnSavePostModel(null,'');

  checksavedpostmodel = new CheckSavedPostModel(null,'');

  checksavedpost: ICheckSavedPost;

  checksavedpostid: number;

  userid:string;

  attachment:boolean = false;

  constructor ( private _route: ActivatedRoute, private _router: Router,private postService: PostService, private userService: UserService){
    
  }

  ngOnInit():void {

                        this.sub = this._route.params.subscribe(
                            params => {
                                let id = +params['id'];
                                let tag = params['tag']

                                this.postid = id;
                                this.tag =  tag;
                    });  

                    this.currentusername = localStorage.getItem("currentUserName");
                    this.userid = localStorage.getItem("userid");

                    this.model = new PostIdModel(this.postid);
                    this.postService.getPostById(this.model).subscribe(
                      post => {
                        this.post = post;

                        if(post.signedUrl1){
                          this.attachment = true;
                        }
                        console.log("postbyid post detail..."+JSON.stringify(this.post));
                        
                                            this.checkpostsavedmethod();
                                         
                                              console.log("user from event details"+JSON.stringify(this.user));
                          
                             

                      },
                      error => console.log(error)
                    );    
    }

  goBack() {
    window.history.back();
  }

  // likeDisplay() {
  //   if (this.like =="Like") {
  //     this.like="Unlike";
  //     // this.user.likedPostList.push(this.post);
  //     this.post.likes+=1;
  //     // this.userService.updateUser(this.user).subscribe();
  //     // this.postService.updatePost(this.post).subscribe();
  //   } else {
  //     this.like="Like";
    
  //     // for (let i=0; i<this.UserCommunity.likedPostList.length; i++) {
  //     //   if (this.UserCommunity.likedPostList[i].postid == this.post.postid) {
  //     //       this.UserCommunity.likedPostList.splice(i, 1);
  //     //   }
  //     // }   
  //     // this.post.likes-=1;
  //     // this.userService.updateUser(this.UserCommunity).subscribe();
  //     // this.postService.updatePost(this.post).subscribe();
  //   }
  // }

  onclicksharecount(){
    this.postService.PostSharesCount(this.model)
                    .subscribe(
                      data => {
                           this.ngOnInit();
                            console.log('success: ', data)
                      },
                      err => console.log('error: ', err)
                    );  
  }



  savepost(){
        console.log("save post clicked");
   
        this.savepostmodel.postid = this.post.postid;
        this.savepostmodel.postName = this.post.postName;
        this.savepostmodel.username = this.currentusername;
        this.savepostmodel.userid = this.userid;
        this.savepostmodel.branch = this.post.branch;
        this.savepostmodel.tag = this.post.tag;
        this.savepostmodel.description = this.post.description;
        this.savepostmodel.shares = this.post.shares;
        this.savepostmodel.views = this.post.views;
        this.savepostmodel.Created = this.post.Created;
        this.savepostmodel.commentcount = this.post.commentcount;

        this.postService.postSavePost(this.savepostmodel)                         
                                  .subscribe(
                                    data => {

                                      this.ngOnInit();

                                      console.log('success: ', data)
                                    },
                                    err => console.log('error: ', err)
                                  );
                console.log("save post model"+JSON.stringify(this.savepostmodel));
  }

  unsavepost(){
        console.log("unsave post clicked");

          this.unsavepostmodel.postid = this.post.postid;
          this.unsavepostmodel.userid = this.userid;
          this.postService.postUnSavePost(this.unsavepostmodel)                         
                                  .subscribe(
                                    data => {

                                      this.ngOnInit();

                                      console.log('success: ', data)
                                    },
                                    err => console.log('error: ', err)
                                  );
                console.log("un save post model"+JSON.stringify(this.unsavepostmodel));    
  }

   checkpostsavedmethod(){
        
       // check for registered events
              this.checksavedpostmodel = new CheckSavedPostModel(this.post.postid,this.userid);
              this.postService.checksavedpostwithuserid(this.checksavedpostmodel)                           
                                  .subscribe(
                                      (data) => {
                                         this.checksavedpost = data;
                                         this.checksavedpostid = this.checksavedpost.postid;
                                        
                                      },
                                      err => console.log('error: ', err)
                                  );
                    console.log("check saved project: "+this.checksavedpostmodel);
    }

  
}
