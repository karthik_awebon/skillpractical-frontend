import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { communityRoutes } from './community.routes';
import {  PostList } from './post-list.component';
import { HomeComponent } from './home.component';
import { SidePanel } from './side-panel.component';
import { PostService } from './post.service';
import { MyPosts } from './myposts/myposts.component';
import {UserService} from './user.service';
import { AddPost } from './add-post.component'
import {PostComments} from './post-comments.component'
import {PostDetail} from './post-detail.component'
import { AddPostService } from './myposts/add-post.service'
import { CommentService }  from './comment.service'
import {ShareButtonsModule} from 'ngx-sharebuttons';
import { PostsByTag } from './postsbytag/postsbytag.component';
import {PostsFilterPipe} from './post-filter.pipe';
import {CommunityMessage} from './communitymessage/communitymessage.component';
import { PostsByTagInPostDetail } from 'app/community/postsbytaginpostdetail/postsbytaginpostdetail.component';
import { TrendingPosts } from 'app/community/trendingposts/trendingposts.component';
import { TagsFilterPipe } from 'app/community/tag-filter.pipe';
import {ProjectBySubjectInPostDetail} from './projectsbysubjectinpostdetail/projectsbysubjectinpostdetail.component'
import { TrendingProjects } from 'app/community/trendingprojects/trendingprojects.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {TooltipModule} from "ng2-tooltip";
import { ProjectService } from 'app/projects';
import { ProjectQueries } from 'app/community/projectqueries/projectqueries/projectqueries.list.component';
import { ProjectQueryDetail } from 'app/community/projectqueries/projectqueries/projectqueriesdetail.component';
import { ProjectQueriesComments } from 'app/community/projectqueries/projectqueries/projectqueries-comments.component';
import { NewProjectQueryDetail } from 'app/community/projectqueries/projectqueries/newprojectqueriesdetail.component';
import { NewProjectQueries } from 'app/community/projectqueries/projectqueries/newprojectqueries.list.component';
import { NewProjectQueriesComments } from 'app/community/projectqueries/projectqueries/newprojectqueries-comment.component';
import { ProjectQueriesService } from 'app/community/projectqueries/projectqueries/projectqueries.service';
import { NewProjectQueryFormComponent } from 'app/community/projectqueries/projectqueries/newprojectqueryform.component';
import { ProjectqueriesAllListComponent } from 'app/community/projectqueries/projectqueries/projectqueries-all-list.component';
import { PostTagByBranch } from 'app/community/posttagsbybranch/posttagbybranch.component';
import { CapitalizePipe } from 'app/community/capitalize.pipe';
import { NgxEditorModule } from 'ngx-editor';
import { MyAnswersComponent } from 'app/community/my-answers/my-answers.component';
import { QuillEditorModule } from 'ngx-quill-editor';
import { MathJaxGuideComponent } from '../common/MathjaxGuide/mathjaxguide.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
    imports:[
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(communityRoutes),
        ShareButtonsModule.forRoot(),
        NgxPaginationModule,
        TooltipModule,
        NgxEditorModule,
        QuillEditorModule
    ],
    declarations:[CapitalizePipe,
        PostList,HomeComponent,SidePanel,MyPosts,MyAnswersComponent,AddPost,PostComments,PostDetail,
        PostsByTag,PostsFilterPipe,TagsFilterPipe,CommunityMessage,PostsByTagInPostDetail,TrendingPosts,ProjectBySubjectInPostDetail,
        TrendingProjects,PostTagByBranch,
        // project queries
        ProjectQueries, ProjectQueryDetail,ProjectQueriesComments,
        //new project queries
        NewProjectQueries,NewProjectQueryDetail,NewProjectQueriesComments,
        NewProjectQueryFormComponent,ProjectqueriesAllListComponent,
        MathJaxGuideComponent
    ],
    providers:[
     PostService,UserService,AddPostService,CommentService,
    ProjectQueriesService,HomeComponent,
    ProjectService,NewProjectQueries,//newproject query
    PostComments
    ]
})
export class CommunityModule { }