import {Component, Input} from '@angular/core';

import {  ActivatedRoute, Router } from '@angular/router';
import { Subscription }       from 'rxjs/Subscription';

import { NgForm } from '@angular/forms';
import { ITag } from 'app/community/interfaces/posttagInterface';

@Component({
  selector: 'posts-tag-by-branch',
  templateUrl: 'posttagbybranch.component.html',
  styleUrls: ['posttagbybranch.component.css']
})
export class PostTagByBranch {
  @Input() tagbybranch : ITag[];
    
    constructor (){
    
    }

}
