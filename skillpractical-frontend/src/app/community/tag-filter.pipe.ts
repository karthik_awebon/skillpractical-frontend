import { PipeTransform, Pipe } from '@angular/core'
import { ITag } from 'app/community/interfaces/posttagInterface';



@Pipe({
    name: 'tagsFilter'
})
export class TagsFilterPipe implements PipeTransform {

         transform(value: ITag[], filterBy: string): ITag[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        
        return filterBy ? value.filter((tag: ITag) =>
            tag.tag.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }
}