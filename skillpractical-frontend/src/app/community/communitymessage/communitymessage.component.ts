import { Component, Input } from '@angular/core'
import {PostService} from '../post.service'
import {Router} from '@angular/router';
import {Observable}     from 'rxjs/Observable';

import {ICommunityMessage} from '../interfaces/communitymessage.interface'

@Component({

  selector: 'community-message',
  styleUrls: ['communitymessage.component.css'],
  templateUrl: 'communitymessage.component.html'
})
export class CommunityMessage {

  constructor (private postService: PostService, private _router: Router) {
                        
  }

  errorMessage:string;

  communitymessages: ICommunityMessage[];

  ngOnInit(){
        this.postService.getCommunityMessage()
            .subscribe(
            (data) => {
            this.communitymessages = data;
                console.log("community message....."+JSON.stringify(this.communitymessages))
                
            },
        error => this.errorMessage = <any>error);        
  }

}
