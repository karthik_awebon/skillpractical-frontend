import {Component, Input} from '@angular/core';
import {PostService} from '../post.service';
import {UserService} from '../user.service';
// import {UserCommunity} from '../usercommunity';
import {IPost} from '../interfaces/postInterface';

// import { UserNameModel } from '../username.model'
// import { UserIdModel } from '../userid.model'

import {  ActivatedRoute, Router } from '@angular/router';
import { Subscription }       from 'rxjs/Subscription';

import { NgForm } from '@angular/forms';
import { IUser } from '../interfaces/userInterface'

import {PostTagModel } from '../postsbytag/tag.model';
import { UserIdModel } from 'app/community/userid.model';
import { PostTagAndUserIdModel } from 'app/community/postsbytag/useridandtag.model';
import { PostIdModel } from '../postid.model';
@Component({
  selector: 'posts-by-tag',
  templateUrl: 'postsbytag.component.html',
  styleUrls: ['postsbytag.component.css']
})
export class PostsByTag {

  public listFilter:any;

  post : IPost[];
  errorMessage:string;

    posttag:string;
    private sub: Subscription;
    modeltag = new PostTagModel('');
  
    filterBy: string = 'newest';

    userid:string;

    userIdModel = new UserIdModel(null);

    posttaganduseridmodel = new PostTagAndUserIdModel('','');

    model = new PostIdModel(null);

      //initializing p to one
      p: number = 1;

  constructor (private _route: ActivatedRoute,private userService: UserService, private postService: PostService, private _router: Router){
    
  }

  onclickViewsCount(postid){
    this.model.postid = postid;
    this.postService.UpdateViewsCount(this.model)
                    .subscribe(
                      data => {
                    
                            console.log('success: ', data)
                      },
                      err => console.log('error: ', err)
                    );  
  }


   ngOnInit():void {     
      
            this.sub = this._route.params.subscribe(
            params => {
                let tag = params['tag'];
                this.posttag = tag;
           });    

            this.modeltag = new PostTagModel(this.posttag);
            
            this.postService.getPostsByTag(this.modeltag)
                    .subscribe(
                        (data) => {
                          this.post = data;
                          console.log("posts from post tag....."+JSON.stringify(this.post))                  
                          },
                      error => this.errorMessage = <any>error)  
  
   }


   filterByunanswered(){
    this.filterBy = 'unanswered';  //to know that this button is clicked.
    this.postService.getPostsWhereUnansweredAndTag(this.modeltag)
              .subscribe(
                  (data) => {
                    this.post = data;
                    console.log("posts from post unanswered....."+JSON.stringify(this.post))                  
                    },
                error => this.errorMessage = <any>error)             
    }

    filterBynewest(){
      this.filterBy = 'newest';
      this.postService.getPostsWhereNewestAndTag(this.modeltag)
                  .subscribe(
                      (data) => {
                        this.post = data;
                        console.log("posts from post Newest....."+JSON.stringify(this.post))                  
                        },
                    error => this.errorMessage = <any>error)   
    }

    filterBylastweek(){
      this.filterBy = 'lastweek';
        this.postService.getPostsWhereLastweekAndTag(this.modeltag)
                  .subscribe(
                      (data) => {
                        this.post = data;
                        console.log("posts from post Lastweek....."+JSON.stringify(this.post))                  
                        },
                    error => this.errorMessage = <any>error)  
      
    }

    filterBysuggested(){
      this.filterBy = 'suggested';
        this.postService.getPostsWhereInterestedAndSuggestedAndTag(this.modeltag)
        .subscribe(
            (data) => {
              this.post = data;
              console.log("posts from Interested and suggested....."+JSON.stringify(this.post))                  
              },
          error => this.errorMessage = <any>error)  
      
    }

 //filter by starred
  filterBystarred(){
  
          this.userid =  localStorage.getItem("userid");
          
          this.posttaganduseridmodel = new PostTagAndUserIdModel(this.posttag,this.userid);
  
          this.postService.getPostsStarredByUserAndTag(this.posttaganduseridmodel)
                          .subscribe(
                              (data) => {
                              this.post = data;
                              console.log("posts starred....."+JSON.stringify(this.post))                  
                              },
                          error => this.errorMessage = <any>error)  
      }


}
