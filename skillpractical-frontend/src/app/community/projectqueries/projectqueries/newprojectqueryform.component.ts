import { Component, Input, ViewChild, ElementRef } from '@angular/core';
import { NgForm, FormGroup } from '@angular/forms';
import { ToastrService, ToastrConfig } from 'toastr-ng2';
import { UserNameModel, ProjectService } from 'app/projects';
import { NewProjectQueryFormModel } from 'app/community/projectqueries/projectqueries/newprojectqueryform.model';
import { IUser } from 'app/community/interfaces/userInterface';
import { UserService } from 'app/community/user.service';
import { NewProjectQueries } from 'app/community/projectqueries/projectqueries/newprojectqueries.list.component';
import { ProjectqueriesAllListComponent } from 'app/community/projectqueries/projectqueries/projectqueries-all-list.component';
import { Router } from '@angular/router';
@Component({
  selector: 'new-project-query-form',
  templateUrl:'newprojectqueryform.component.html', 
  styleUrls: ['newprojectqueryform.component.css']
  
})
export class NewProjectQueryFormComponent {
  @ViewChild('closeBtn') closeBtn: ElementRef;
  
     hasWhoswithError = false;
     hasBranchError = false;
     hasSpecilizationError = false;
  
    currentusername:string;
    model = new NewProjectQueryFormModel('','','default','default','','','default');
    modelusername = new UserNameModel('');
    user: IUser;
    userid: string;
    username: string;
   constructor(private toastrService: ToastrService,private projectService: ProjectService, private router: Router,
    private userService:UserService,private toastrConfig: ToastrConfig, private newProjectQueries:NewProjectQueries,
    private ProjectqueriesAllListComponent:ProjectqueriesAllListComponent) {
      toastrConfig.timeOut = 2000;
      this.router.routeReuseStrategy.shouldReuseRoute = function() {
        return false;
    };
   }
    ngOnInit() {
      
      //this.modalForm.reset();
            this.currentusername =  localStorage.getItem("currentUserName");
            this.modelusername = new UserNameModel(this.currentusername);
          
            this.userService.getUserByName(this.modelusername).subscribe(
                  user => {
                    this.user = user;
                      console.log("user from new project queryform component"+JSON.stringify(this.user));
                    
                  },
                  error => console.log(error)
                )
                //call this wherever you want to close modal
                
    }

    isValid:boolean = true;
    isValidForm() {
       return this.isValid;
   }
   
    submitForm(form: NgForm) {
      
      this.isValid = false;
                this.model.userid = this.user.userid;
                this.model.username = this.user.username;
                // this.model = new QueryFormModel(this.user.userid,this.user.username,'','','')
                 this.projectService.postNewProjectQueryForm(this.model)
                                  .subscribe(
                                    data => { 
                                      
                                      this.isValid = true;
                                      this.closeBtn.nativeElement.click();
                                      this.toastrService.success('Request Successful!');

                                      this.router.navigate(['/community/projectqueries']);

                                      // window.location.reload();
                                      
                                      form.reset();
                                   
                                     
                                    },
                                    err => console.log('error: ', err)
                                    
                                  );
                                 
                                  console.log("new project post form model"+JSON.stringify(this.model));
                                 
              }
            
              validateWhoswith(value) {
                if (value === 'default')
                  this.hasWhoswithError = true;
                else
                  this.hasWhoswithError = false;
              }
              validateBranch(value){
                if (value === 'default')
                this.hasBranchError = true;
                else
                this.hasBranchError = false;
              }
              validateSpecialization(value){
                if (value === 'default')
                this.hasSpecilizationError = true;
                else
                this.hasSpecilizationError = false;
              }
              closemodel(form){
                form.reset();
              }
}