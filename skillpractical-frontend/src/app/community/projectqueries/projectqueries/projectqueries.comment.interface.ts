export interface IProjectQueriesComment {
  
  commentId: number;
  username: string;
  content: string;
  postid: number;

}