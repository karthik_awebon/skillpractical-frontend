export interface INewProjectQueriesComment {
  
  commentId: number;
  username: string;
  content: string;
  postid: number;

}