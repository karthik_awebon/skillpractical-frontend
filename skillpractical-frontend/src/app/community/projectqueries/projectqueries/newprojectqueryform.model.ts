export class NewProjectQueryFormModel{
    constructor(
         public userid:string,
         public username:string,
         public branch: string,
         public specialization: string,
         public title:string,
         public idea:string,
         public projectgrouporindividual: string
    ){}
}