import {Component, Input} from '@angular/core';

import { ProjectQueryIdModel } from '../projectqueries/projectqueryid.model';


import { NgForm } from '@angular/forms';
import { ProjectQueriesCommentModel } from '../projectqueries/projectqueries.comment.model';
import { IProjectQueries } from '../projectqueries/projectQueries.interface';


import { ProjectQueriesService } from '../projectqueries/projectqueries.service';
import { IProjectQueriesComment } from './projectqueries.comment.interface';

import { ToastrService, ToastrConfig } from 'toastr-ng2';
import { UserCommunity } from 'app/community/usercommunity';
import { IUser } from 'app/community/interfaces/userInterface';
import { UserNameModel } from 'app/community/username.model';
import { DeleteProjectQueryCommentModel } from 'app/community/projectqueries/projectqueries/deletprojectequerycomments.model';
import { UserService } from 'app/community/user.service';
import { CommentService } from 'app/community/comment.service';
import { PostService } from 'app/community/post.service';

@Component ({
  selector: 'projectqueries-comments',
  templateUrl: 'projectqueries-comments.component.html'
})
export class ProjectQueriesComments {
 
  @Input() projectqueriesdetail: IProjectQueries;

  myLocalStorage = localStorage;
  userid: string;
  errorMessage: string;
  model = new UserCommunity();

  user: IUser;
  modelprojectqueryid = new ProjectQueryIdModel(null);
  currentusername:string;
  modelusername = new UserNameModel('');
  projectquerycomment:IProjectQueriesComment[];

  model1 = new ProjectQueriesCommentModel('',null,'','');
  modeldeleteprojectquerycomments = new DeleteProjectQueryCommentModel(null,null);

  constructor (private toastrService: ToastrService,private userService: UserService,
    private projectQueriesService: ProjectQueriesService, 
    private commentService: CommentService, private postService: PostService,private toastrConfig: ToastrConfig) {
      toastrConfig.timeOut = 2000;
    }

  ngOnInit():void { 
        this.userid =  localStorage.getItem("userid");

        this.currentusername = localStorage.getItem("currentUserName");
        this.modelusername = new UserNameModel(this.currentusername);
        this.modelprojectqueryid = new ProjectQueryIdModel(this.projectqueriesdetail.queryid);
        
        // this.model1 = new PostCommentModel('',this.post.postid,this.currentusername);

        this.projectQueriesService.getCommentsByProjectQueryId(this.modelprojectqueryid).subscribe(
              projectquerycomment => {
                this.projectquerycomment = projectquerycomment;
              },
              error => console.log(error)
            )
            console.log("project query id model"+JSON.stringify(this.modelprojectqueryid));
            console.log("comments from projects query id ..."+JSON.stringify(this.projectquerycomment));

        //     this.userService.getUserByName(this.modelusername).subscribe(
        //       user =>  {
        //         this.user = user;
        //       },
        //       error => console.log(error)
        //     )
        // console.log("username model"+this.modelusername);
        // console.log("user..."+this.user);
      }

        submitForm(form: NgForm) {
          // this.model1 = new ProjectQueriesCommentModel('',this.projectqueriesdetail.queryid,this.currentusername);
            this.model1.queryid = this.projectqueriesdetail.queryid;
            this.model1.username = this.currentusername;
            this.model1.userid = this.userid;
              this.projectQueriesService.PostProjectQueryNewComment(this.model1)
                    .subscribe(
                      data => {
                           this.ngOnInit();
                            console.log('success: ', data)
                            form.reset();
                      },
                      err => console.log('error: ', err)
                    );        
        }

//    onclickComment(){
//      this.commentService.PostCommentCount(this.modelprojectqueryid)
//                     .subscribe(
//                       data => {
//                            window.location.reload();
//                             console.log('success: ', data)
//                       },
//                       err => console.log('error: ', err)
//                     );   
//    }
deleteProjectQueryComment(commentId){

  // this.modeldeletepost = new DeletePostModel(post.key1,post.postid,this.userid)

  this.modeldeleteprojectquerycomments.commentId = commentId;
  this.modeldeleteprojectquerycomments.userid = this.userid;
  this.projectQueriesService.deleteProjectQueryComment(this.modeldeleteprojectquerycomments)
  .subscribe(
      (data) => {
                 this.ngOnInit();
                this.toastrService.success('Your Query is Successfully Deleted! ');
        },
    error => this.errorMessage = <any>error)    
  }
}
