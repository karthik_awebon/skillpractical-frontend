export interface INewProjectQueries {
  queryid: number
  userid: string
  title: string
  idea:string
  specialization:string
  projectgrouporindividual:string
  created: Date
  username:string
}