import {Component, Input} from '@angular/core';

import { ProjectQueryIdModel } from '../projectqueries/projectqueryid.model';


import { NgForm } from '@angular/forms';
import { NewProjectQueriesCommentModel } from '../projectqueries/newprojectqueries.comment.model';
import { INewProjectQueries } from '../projectqueries/newprojectQueries.interface';


import { ProjectQueriesService } from '../projectqueries/projectqueries.service';
import { INewProjectQueriesComment } from './newprojectqueries.comment.interface';
import {NewProjectQueryIdModel} from './newprojectqueryid.model'

import { ToastrService, ToastrConfig } from 'toastr-ng2';
import { UserCommunity } from 'app/community/usercommunity';
import { IUser } from 'app/community/interfaces/userInterface';
import { UserNameModel } from 'app/community/username.model';
import { DeleteNewQueryCommentModel } from 'app/community/projectqueries/projectqueries/deletenewquerycomment.model';
import { UserService } from 'app/community/user.service';
import { CommentService } from 'app/community/comment.service';
import { PostService } from 'app/community/post.service';

@Component ({
  selector: 'newprojectqueries-comments',
  templateUrl: 'newprojectqueries-comment.component.html'
})
export class NewProjectQueriesComments {
  errorMessage: string;
  @Input() newprojectqueriesdetail: INewProjectQueries;

  myLocalStorage = localStorage;

  model = new UserCommunity();

  user: IUser;
  modelnewprojectqueryid = new NewProjectQueryIdModel(null);
  currentusername:string;
  modelusername = new UserNameModel('');
  newprojectquerycomment:INewProjectQueriesComment[];
  userid:string;
  model1 = new NewProjectQueriesCommentModel('',null,'','');
  modeldeletenewquerycomments= new DeleteNewQueryCommentModel('',null);
  
  constructor (private toastrService: ToastrService,private userService: UserService,private projectQueriesService: ProjectQueriesService, 
    private commentService: CommentService, private postService: PostService,private toastrConfig: ToastrConfig) {
      toastrConfig.timeOut = 2000;
  }

  ngOnInit():void { 

         this.userid =  localStorage.getItem("userid");

        this.currentusername = localStorage.getItem("currentUserName");
        this.modelusername = new UserNameModel(this.currentusername);
        this.modelnewprojectqueryid = new NewProjectQueryIdModel(this.newprojectqueriesdetail.queryid);
        
        // this.model1 = new PostCommentModel('',this.post.postid,this.currentusername);

        this.projectQueriesService.getCommentsBynewProjectQueryId(this.modelnewprojectqueryid).subscribe(
              newprojectquerycomment => {
                this.newprojectquerycomment = newprojectquerycomment;
              },
              error => console.log(error)
            )
            console.log("new project query id model from comments"+JSON.stringify(this.modelnewprojectqueryid));
            console.log("comments from projects query id ..."+JSON.stringify(this.newprojectquerycomment));

        //     this.userService.getUserByName(this.modelusername).subscribe(
        //       user =>  {
        //         this.user = user;
        //       },
        //       error => console.log(error)
        //     )
        // console.log("username model"+this.modelusername);
        // console.log("user..."+this.user);
      }

        submitForm(form: NgForm) {
         
          // this.model1 = new ProjectQueriesCommentModel('',this.projectqueriesdetail.queryid,this.currentusername);
            this.model1.queryid = this.modelnewprojectqueryid.queryid;
            this.model1.username = this.currentusername;
            this.model1.userid = this.userid;
              this.projectQueriesService.PostnewProjectQueryNewComment(this.model1)
                    .subscribe(
                      data => {
                           this.ngOnInit();
                            console.log('success: ', data)
                            form.reset();
                      },
                      err => console.log('error: ', err)
                    );        
        }

//    onclickComment(){
//      this.commentService.PostCommentCount(this.modelprojectqueryid)
//                     .subscribe(
//                       data => {
//                            window.location.reload();
//                             console.log('success: ', data)
//                       },
//                       err => console.log('error: ', err)
//                     );   
//    }
deleteNewProjectQueryComment(commentId){

  // this.modeldeletepost = new DeletePostModel(post.key1,post.postid,this.userid)
 
  this.modeldeletenewquerycomments.commentId = commentId;
  this.modeldeletenewquerycomments.userid = this.userid;
  this.projectQueriesService.deleteNewProjectQueryComment(this.modeldeletenewquerycomments)
  .subscribe(
      (data) => {
                 this.ngOnInit();
                this.toastrService.success('Your Query is Successfully Deleted! ');
        },
    error => this.errorMessage = <any>error)    
  }
}
