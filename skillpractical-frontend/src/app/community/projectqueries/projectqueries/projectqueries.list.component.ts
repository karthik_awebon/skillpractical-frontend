import {Component, Input} from '@angular/core';

import {Router} from '@angular/router';

import { NgForm } from '@angular/forms';

 
import { ProjectQueriesService} from '../projectqueries/projectqueries.service';
 
import { IProjectQueries } from './projectQueries.interface';

import { ToastrService, ToastrConfig } from 'toastr-ng2';
import { IUser } from 'app/community/interfaces/userInterface';
import { UserIdModel } from 'app/community/userid.model';
import { DeleteProjectQueryModel } from 'app/community/projectqueries/projectqueries/deleteprojectquery.model';
import { UserService } from 'app/community/user.service';
@Component({
  selector: 'project-queries',
  templateUrl: 'projectqueries.list.component.html'
})
export class ProjectQueries {
  projectqueries : IProjectQueries[];
  errorMessage:string;
  user: IUser;
  userid: string;
  modeluserid = new UserIdModel(null);
  //initializing p to one
  p: number = 1;
  modeldeleteprojectquery = new DeleteProjectQueryModel(null,'');
 constructor (private toastrService: ToastrService,private userService: UserService, private projectQueriesService: ProjectQueriesService,
   private _router: Router,private toastrConfig: ToastrConfig){
  toastrConfig.timeOut = 2000;
 }
   ngOnInit():void {     
                this.userid =  localStorage.getItem("userid");
                this.modeluserid = new UserIdModel(this.userid);
    
            this.projectQueriesService.getProjectQueriesByUser(this.modeluserid)
                    .subscribe(
                        (data) => {
                          this.projectqueries = data;
                          console.log("project queries from userid id....."+JSON.stringify(this.projectqueries))                  
                          },
                      error => this.errorMessage = <any>error)                
   }


   deleteProjectQuery(projectqueries){

    // this.modeldeletepost = new DeletePostModel(post.key1,post.postid,this.userid)
    
    this.modeldeleteprojectquery.queryid = projectqueries.queryid;
    this.modeldeleteprojectquery.userid = this.userid;
    this.projectQueriesService.deleteProjectQuery(this.modeldeleteprojectquery)
    .subscribe(
        (data) => {
                   this.ngOnInit();
                  this.toastrService.success('Your Post is Successfully Deleted! ');
          },
      error => this.errorMessage = <any>error)    
    }
}