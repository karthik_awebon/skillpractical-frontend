import {Component, Input} from '@angular/core';

import {Router} from '@angular/router';



import { NgForm } from '@angular/forms';

 
import { ProjectQueriesService} from '../projectqueries/projectqueries.service';
 
import { INewProjectQueries } from './newprojectQueries.interface';

import { ToastrService, ToastrConfig } from 'toastr-ng2';
import { IUser } from 'app/community/interfaces/userInterface';
import { DeleteNewQueryModel } from 'app/community/projectqueries/projectqueries/deletenewquery.model';
import { UserNameModel } from 'app/community/username.model';
import { UserIdModel } from 'app/community/userid.model';
import { UserService } from 'app/community/user.service';
@Component({
  selector: 'newproject-queries',
  templateUrl: 'newprojectqueries.list.component.html'
})
export class NewProjectQueries {

  newprojectqueries : INewProjectQueries[];
  errorMessage:string;
  user: IUser;
  userid:string;
  modeldeletenewquery = new DeleteNewQueryModel(null,'');
  modelusername = new UserNameModel('');
  modeluserid = new UserIdModel(null);
  //initializing p to one
  p: number = 1;
  
 constructor (private router: Router,private toastrService: ToastrService,private userService: UserService, private projectQueriesService: ProjectQueriesService,
   private _router: Router,private toastrConfig: ToastrConfig){

  //   this.router.routeReuseStrategy.shouldReuseRoute = function() {
  //     return false;
  // };

    toastrConfig.timeOut = 2000;
 }
   ngOnInit():void {     
          
         this.userid =  localStorage.getItem("userid");
         
         this.modeluserid = new UserIdModel(this.userid);

            this.projectQueriesService.getnewProjectQueriesByUser(this.modeluserid)
                    .subscribe(
                        (data) => {
                          this.newprojectqueries = data;
                          console.log("new project queries from userid id....."+JSON.stringify(this.newprojectqueries))                  
                          },
                      error => this.errorMessage = <any>error)  

   }
   deleteNewQuery(newprojectqueries){

    // this.modeldeletepost = new DeletePostModel(post.key1,post.postid,this.userid)
    
    this.modeldeletenewquery.queryid = newprojectqueries.queryid;
    this.modeldeletenewquery.userid = this.userid;
    this.projectQueriesService.deleteNewQuery(this.modeldeletenewquery)
    .subscribe(
        (data) => {
                   this.ngOnInit();
                  this.toastrService.success('Your Post is Successfully Deleted! ');
          },
      error => this.errorMessage = <any>error)    
    }
}
