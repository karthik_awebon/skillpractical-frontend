export class NewProjectQueriesCommentModel{
    constructor(
        public content:string,
        public queryid:number,
        public username:string,
        public userid: string
    ){}
}