export interface IProjectQueries {
  queryid: number
  userid: string
  title: string
  description: string
  created: Date
  username:string

}