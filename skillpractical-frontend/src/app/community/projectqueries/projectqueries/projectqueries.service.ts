import {Injectable} from '@angular/core';
import { Router } from '@angular/router'
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { Subject, Observable} from 'rxjs/RX';



import { IProjectQueries } from '../projectqueries/projectQueries.interface';
import { ProjectQueryIdModel } from '../projectqueries/projectqueryid.model';


import { ProjectQueriesCommentModel } from '../projectqueries/projectqueries.comment.model';

import {IProjectQueriesComment} from '../projectqueries/projectqueries.comment.interface';


import { INewProjectQueries } from './newprojectQueries.interface';
import {NewProjectQueryIdModel } from './newprojectqueryid.model';
import {INewProjectQueriesComment} from './newprojectqueries.comment.interface';



import { UserIdModel } from 'app/community/userid.model';
import { ApiSettings } from 'app/ApiSettings';
import { DeleteNewQueryModel } from 'app/community/projectqueries/projectqueries/deletenewquery.model';
import { DeleteProjectQueryModel } from 'app/community/projectqueries/projectqueries/deleteprojectquery.model';
import { DeleteNewQueryCommentModel } from 'app/community/projectqueries/projectqueries/deletenewquerycomment.model';
import { DeleteProjectQueryCommentModel } from 'app/community/projectqueries/projectqueries/deletprojectequerycomments.model';

@Injectable()
export class ProjectQueriesService {

  constructor (private http : Http) {}

 private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

private extractData(res: Response) {
        let body = res.json();
        return body.fields || { };
    }


  private _ProjectQueriesByUseridurl = ApiSettings.API_ENDPOINT+ '/rest/projects/reqProjectQueriesByUser.on';

  getProjectQueriesByUser(userIdModel: UserIdModel): Observable<any> {
        console.log("Userid model.."+JSON.stringify(userIdModel));
         let body = JSON.stringify(userIdModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this._ProjectQueriesByUseridurl,body,options)
            .map((response: Response) => <IProjectQueries[]> response.json().projectqueriesbyuser_info)
            .do(data => console.log('Project Info by user id: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }
        


  private _ProjectQueriesByQueryId = ApiSettings.API_ENDPOINT+'/rest/projects/reqProjectQueriesByQueryId.on';

  getProjectQueriesByQueryId(projectQueryIdModel: ProjectQueryIdModel): Observable<any> {
        console.log("query id model.."+JSON.stringify(projectQueryIdModel));
         let body = JSON.stringify(projectQueryIdModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this._ProjectQueriesByQueryId,body,options)
            .map((response: Response) => <IProjectQueries[]> response.json().projectqueriesbyqueryid_info)
            .do(data => console.log('Project queries by query id ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }
        
//   PostSharesCount(postIdModel: PostIdModel) : Observable<any> {
//           console.log("post comment model"+JSON.stringify(postIdModel));
//         let body = JSON.stringify(postIdModel);
//         let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
//         let options = new RequestOptions({ headers: headers });

//         return this.http.post(this.postSharesCount, body, options)
//                         .map(this.extractData)
//                         .catch(this.handleError);
//         }


  getCommentsByProjectQueryId(projectQueryIdModel: ProjectQueryIdModel): Observable<any> {
        console.log(" query id model for get comments.."+JSON.stringify(projectQueryIdModel));
         let body = JSON.stringify(projectQueryIdModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
                 return this.http.post(ApiSettings.API_ENDPOINT+'/rest/projects/reqCommentByQueryId.on',body,options)
                    .map((response: Response) => <IProjectQueriesComment[]> response.json().projectqueries_Comments_info)
                    .do(data => console.log('Comment Info by query id' +  JSON.stringify(data)))
                    .catch(this.handleError);      
    }

  private postProjectQueryComment = ApiSettings.API_ENDPOINT+'/rest/projects/reqCreateCommentProjectQueriesInfo.on';
   
  PostProjectQueryNewComment(projectQueriesCommentModel: ProjectQueriesCommentModel) : Observable<any> {
          console.log("comment model project query"+JSON.stringify(projectQueriesCommentModel));
        let body = JSON.stringify(projectQueriesCommentModel);
        let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.postProjectQueryComment, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
         }



                                          // new project queries


  private _newProjectQueriesByUseridurl = ApiSettings.API_ENDPOINT+'/rest/projects/reqNewProjectQueriesByUser.on';

  getnewProjectQueriesByUser(userIdModel: UserIdModel): Observable<any> {
        console.log("Userid model.."+JSON.stringify(userIdModel));
         let body = JSON.stringify(userIdModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this._newProjectQueriesByUseridurl,body,options)
            .map((response: Response) => <INewProjectQueries[]> response.json().newprojectqueriesbyuser_info)
            .do(data => console.log('Project Info by user: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }
        

  private _newProjectQueriesByQueryId = ApiSettings.API_ENDPOINT+'/rest/projects/reqNewProjectQueriesByQueryId.on';

  getnewProjectQueriesByQueryId(NewProjectQueryIdModel: NewProjectQueryIdModel): Observable<any> {
        console.log("new project query id model.."+JSON.stringify(NewProjectQueryIdModel));
         let body = JSON.stringify(NewProjectQueryIdModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this._newProjectQueriesByQueryId,body,options)
            .map((response: Response) => <INewProjectQueries[]> response.json().newprojectqueriesbyqueryid_info)
            .do(data => console.log('new Project queries by query id ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


   getCommentsBynewProjectQueryId(NewProjectQueryIdModel: NewProjectQueryIdModel): Observable<any> {
        console.log("new project query id model for get comments.."+JSON.stringify(NewProjectQueryIdModel));
         let body = JSON.stringify(NewProjectQueryIdModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
                 return this.http.post(ApiSettings.API_ENDPOINT+'/rest/projects/reqCommentNewProjectQueriesByQueryId.on',body,options)
                    .map((response: Response) => <INewProjectQueriesComment[]> response.json().newprojectqueries_Comments_info)
                    .do(data => console.log('Comment Info by query id' +  JSON.stringify(data)))
                    .catch(this.handleError);      
    }

  private postnewProjectQueryComment = ApiSettings.API_ENDPOINT+'/rest/projects/reqCreateCommentNewProjectQueriesInfo.on';
   
  PostnewProjectQueryNewComment(projectQueriesCommentModel: ProjectQueriesCommentModel) : Observable<any> {
          console.log("project query comment model"+JSON.stringify(projectQueriesCommentModel));
        let body = JSON.stringify(projectQueriesCommentModel);
        let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.postnewProjectQueryComment, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
         }

                                              // deleting 

         deleteNewQuery(deleteNewQueryModel: DeleteNewQueryModel) : Observable<any> {
            console.log("delete model New Project Query"+JSON.stringify(deleteNewQueryModel));
          let body = JSON.stringify(deleteNewQueryModel);
          let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
          let options = new RequestOptions({ headers: headers });
  
          return this.http.post(ApiSettings.API_ENDPOINT +'/rest/projects/reqDeleteNewProjectQueries.on', body, options)
                          .map(this.extractData)
                          .catch(this.handleError);
           }

           deleteProjectQuery(deleteProjectQueryModel: DeleteProjectQueryModel) : Observable<any> {
            console.log("Project Query delete model"+JSON.stringify(deleteProjectQueryModel));
          let body = JSON.stringify(deleteProjectQueryModel);
          let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
          let options = new RequestOptions({ headers: headers });
  
          return this.http.post(ApiSettings.API_ENDPOINT +'/rest/projects/reqDeleteProjectQueries.on', body, options)
                          .map(this.extractData)
                          .catch(this.handleError);
           }


         deleteNewProjectQueryComment(deleteNewQueryCommentModel: DeleteNewQueryCommentModel) : Observable<any> {
            console.log(" delete model New Project Query Comment"+JSON.stringify(deleteNewQueryCommentModel));
          let body = JSON.stringify(deleteNewQueryCommentModel);
          let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
          let options = new RequestOptions({ headers: headers });
  
          return this.http.post(ApiSettings.API_ENDPOINT +'/rest/projects/reqDeleteCommentNewProjectQueriesByQueryId.on', body, options)
                          .map(this.extractData)
                          .catch(this.handleError);
           }

           
           deleteProjectQueryComment(deleteProjectQueryCommentModel: DeleteProjectQueryCommentModel) : Observable<any> {
            console.log("Project Query Comment delete model"+JSON.stringify(deleteProjectQueryCommentModel));
          let body = JSON.stringify(deleteProjectQueryCommentModel);
          let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
          let options = new RequestOptions({ headers: headers });
  
          return this.http.post(ApiSettings.API_ENDPOINT +'/rest/projects/reqDeleteCommentByQueryId.on', body, options)
                          .map(this.extractData)
                          .catch(this.handleError);
           }

}
