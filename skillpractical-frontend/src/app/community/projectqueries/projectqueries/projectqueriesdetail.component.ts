import {Component, Input} from '@angular/core';
import { IProjectQueries } from '../projectqueries/projectQueries.interface';

import {  ActivatedRoute, Router } from '@angular/router';
import { Subscription }       from 'rxjs/Subscription';
import { ProjectQueryIdModel } from '../projectqueries/projectqueryid.model';

import { ProjectQueriesService } from '../projectqueries/projectqueries.service';
import { IUser } from 'app/community/interfaces/userInterface';
import { UserNameModel } from 'app/community/username.model';
import { UserService } from 'app/community/user.service';

@Component({                                                    
  selector: 'project-query-detail',
  styleUrls: ['projectqueriesdetail.component.css'],
  templateUrl: 'projectqueriesdetail.component.html'
  
})
export class ProjectQueryDetail {
 
  projectqueriesdetail:IProjectQueries;
  like: string;
  user: IUser;
  queryid:number;
  private sub: Subscription;
  currentusername: string;
  model = new ProjectQueryIdModel(null);
  model1 = new UserNameModel('');

  constructor ( private _route: ActivatedRoute, private _router: Router,private projectQueriesService: ProjectQueriesService, 
    private userService: UserService){
    
  }

    ngOnInit():void {
        this.sub = this._route.params.subscribe(
            params => {
                let id = +params['id'];
                this.queryid = id;
                console.log("query id from project query detail"+id);
         });    
   
    this.model = new ProjectQueryIdModel(this.queryid);
    this.model1 = new UserNameModel(this.currentusername);
    this.projectQueriesService.getProjectQueriesByQueryId(this.model).subscribe(
      
      projectqueriesdetail => {
        this.projectqueriesdetail = projectqueriesdetail;
        console.log("model query id"+ JSON.stringify(this.model));
        console.log("project queries detail by query id ..."+JSON.stringify(this.projectqueriesdetail));
        // this.userService.getUserByName(this.model1).subscribe(
        //   user => {
        //     this.user = user;
        //     console.log("userbyusername..."+JSON.stringify(this.user));
        //     // if (this.user.likedPostList.filter(post => post.postid == this.post.postid)[0]) {
        //     //   this.like="Unlike";
        //     // } else {
        //     //   this.like="Like";
        //     // }
        //   },
        //   error => console.log(error)
        // )
      },
      error => console.log(error)
    );    
    }

  goBack() {
    window.history.back();
  }

  // likeDisplay() {
  //   if (this.like =="Like") {
  //     this.like="Unlike";
  //     // this.user.likedPostList.push(this.post);
  //     this.post.likes+=1;
  //     // this.userService.updateUser(this.user).subscribe();
  //     // this.postService.updatePost(this.post).subscribe();
  //   } else {
  //     this.like="Like";
    
  //     // for (let i=0; i<this.UserCommunity.likedPostList.length; i++) {
  //     //   if (this.UserCommunity.likedPostList[i].postid == this.post.postid) {
  //     //       this.UserCommunity.likedPostList.splice(i, 1);
  //     //   }
  //     // }   
  //     // this.post.likes-=1;
  //     // this.userService.updateUser(this.UserCommunity).subscribe();
  //     // this.postService.updatePost(this.post).subscribe();
  //   }
  // }

//   onclicksharecount(){
//     this.postService.PostSharesCount(this.model)
//                     .subscribe(
//                       data => {
//                            window.location.reload();
//                             console.log('success: ', data)
//                       },
//                       err => console.log('error: ', err)
//                     );  
//   }
  
}
