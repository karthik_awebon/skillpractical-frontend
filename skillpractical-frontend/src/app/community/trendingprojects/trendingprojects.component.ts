import {Component, Input} from '@angular/core';
import {  ActivatedRoute, Router } from '@angular/router';
import { Subscription }       from 'rxjs/Subscription';

import { NgForm } from '@angular/forms';

import { window } from 'rxjs/operator/window';
import { IProject } from 'app/projects';

@Component({
  selector: 'projects-trending',
  templateUrl: 'trendingprojects.component.html',
  styleUrls: ['trendingprojects.component.css']
})
export class TrendingProjects {

  @Input() projects : IProject[];
    
  
constructor (){
   
}

}
