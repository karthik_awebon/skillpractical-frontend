export class SavePostModel{
    constructor(
        public postid:number,
        public postName:string,
        public username:string,
        public userid:string,
        public branch:string,
        public tag:string,
        public description,
        public Created,
        public shares,
        public views,
        public commentcount
    ){}
}