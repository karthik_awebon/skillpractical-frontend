import {Component, Input, ViewChild, ElementRef} from '@angular/core';
import {UserCommunity} from './usercommunity';
import {UserService} from './user.service';
import {CommentService} from './comment.service';
import {PostService} from './post.service';
import { UserNameModel } from './username.model';
import { PostIdModel} from './postid.model';
import { NgForm } from '@angular/forms';
import { PostCommentModel } from './postcomment.model'
import { IPost } from './interfaces/postInterface'
import { IUser } from './interfaces/userInterface'
import { Ikeyandurlforfile } from 'app/community/interfaces/commentresponse.interface';
import { DeleteCommentModel } from 'app/community/deletecomment.model';
import { ToastrService } from 'toastr-ng2';
import { ToastrConfig } from 'toastr-ng2';
import { IComment } from 'app/community/interfaces/commentInterface';
@Component ({
  selector: 'post-comments',
  templateUrl: 'post-comments.component.html',
  styleUrls: ['post-comments.component.css']
})
export class PostComments {
  @ViewChild('closeBtn') closeBtn: ElementRef;
  @Input('post') post: IPost;
  myLocalStorage = localStorage;
  // MathJax:any;
  model = new UserCommunity();
  user: IUser;
  modelpostid = new PostIdModel(null);
  currentusername:string;
  userid: string;
  modelusername = new UserNameModel('');
  comment:IComment[];
  selectedFiles: FileList
  currentFileUpload: File
  fileuploaded:boolean;
  private uploadError:boolean = false;
  keyandurlforfile: Ikeyandurlforfile;
  fileurl:string;
  key:string;
  attachment:boolean = false;
  model1 = new PostCommentModel('',null,'','','','','','');
  modeldeletecomment = new DeleteCommentModel('','');
  // comments:string;
  items:any;
  htmlContent:any;
  subscriptionstatus: boolean = false;

  constructor (private toastrService: ToastrService,toastrConfig: ToastrConfig,private userService: UserService, private commentService: CommentService, private postService: PostService) {
    toastrConfig.timeOut = 2000;
  }
  
  isValid:boolean = true;

  isValidForm() {
     return this.isValid;
 }

 
  ngOnInit():void { 

    let mathjax=document.getElementsByClassName('mathjax');
       eval('MathJax.Hub.Queue(["Typeset",MathJax.Hub, mathjax])');
      eval('MathJax.Hub.Queue(["Typeset",MathJax.Hub, mathjax])');


        this.userid = localStorage.getItem("userid");
        this.currentusername = localStorage.getItem("currentUserName");
        this.modelusername = new UserNameModel(this.currentusername);
        this.modelpostid = new PostIdModel(this.post.postid);
        this.model1 = new PostCommentModel('',this.post.postid,this.post.tag,this.currentusername,this.userid,'','','');
        this.commentService.getCommentsByPost(this.modelpostid).subscribe(
              comment => {
                this.comment = comment;
                // if(comment.signedUrl1){
                //   this.attachment = true;
                // }
                // if( this.comment.filter(x => x.userid == this.userid)) {
                //   this.comments = 'Delete';
                // }
              //   if(localStorage.getItem("subscription_status") == "ended"){
              //     this.subscriptionstatus = true;
              //     this.isValid = false;

              // }else {
              //     this.subscriptionstatus = false;
              //     this.isValid = true;

              // }
            
              },
              error => console.log(error) 
            )
            console.log("post id model"+this.modelpostid);
            console.log("comments..."+this.comment);
      }
      // selectFile(event) {
      //   this.selectedFiles = event.target.files;
      // }
      
      // upload() {
      //   this.currentFileUpload = this.selectedFiles.item(0)
      //   this.commentService.pushFileToStorage(this.currentFileUpload).subscribe( 
      //     (data) => {
      //       this.keyandurlforfile = data;
            
      //       this.fileurl = this.keyandurlforfile.signedUrl1;
      //       this.key = this.keyandurlforfile.key1;
      //       this.fileuploaded = true;
      //       console.log("signedurl  "+this.keyandurlforfile.signedUrl1);
      //       console.log("key   "+this.keyandurlforfile.key1);
      //   }, err => 
      //    {
      //     this.uploadError = true;
      //    }
          
      // )
      //   this.selectedFiles = undefined
      // }
      submitForm(form: NgForm) {
                          this.isValid = false;

            // MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
          this.model1.key1 = this.key;
          this.model1.signedUrl1 = this.fileurl;
              this.commentService.PostNewComment(this.model1)
                    .subscribe(
                      data => {
                           this.toastrService.success('Your Comment is successfully Submitted');
                           this.ngOnInit();
                          this.isValid = true;

                           form.reset();
                            console.log('success: ', data)
                      },
                      err => console.log('error: ', err)
                    );        
                    console.log("comment model"+ form);
        }
   onclickComment(){
     this.commentService.PostCommentCount(this.modelpostid)
                    .subscribe(
                      data => {
                            this.ngOnInit();
                            console.log('success: ', data)
                      },
                      err => console.log('error: ', err)
                    );   
   }
   onClickRestoreCommentCount(){
        this.commentService.RestoreCommentCount(this.modelpostid)
        .subscribe(
          data => {
                this.ngOnInit();
                console.log('success: ', data)
          },
          err => console.log('error: ', err)
        );   
   }
   
   onBodyTextEditorKeyUp(event){
     console.log("Text is change: " + event);
   }
   
   deleteComment(commentId){
        console.log("comment id to delete"+commentId);
        
        this.modeldeletecomment.commentId = commentId;
        this.modeldeletecomment.userid = this.userid;
        this.commentService.PostDeleteComment(this.modeldeletecomment)
        .subscribe(
          data => {
                //write restore comment count
                this.onClickRestoreCommentCount();

                this.ngOnInit();
                console.log('success: ', data)
                this.closeBtn.nativeElement.click();
                this.toastrService.success('Your Comment is successfully Deleted');
               
          },
          err => console.log('error: ', err)
        );  
   }
}