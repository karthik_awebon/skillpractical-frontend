import {UserCommunity} from './usercommunity';
import {Injectable} from '@angular/core';
import {Post} from './post';
import { Router } from '@angular/router'
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { Observable} from 'rxjs/RX';

import { IPost} from './interfaces/postInterface';
import { UserNameModel } from './username.model';
import { IUser, IUserSkills, IUserResumeAndLinks } from './interfaces/userInterface';

import { ApiSettings } from '../ApiSettings';
import { UserIdModel } from 'app/community/userid.model';

@Injectable()
export class UserService {

  constructor (private http : Http) {}

 private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

private extractData(res: Response) {
        let body = res.json();
        return body.fields || { };
    }

  // getUserByName(username: string) {
  //   let tokenUrl1 = "http://localhost:8080/rest/user/userName";
  //   let headers1 = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
  //   return this.http.post(tokenUrl1, username, {headers: headers1});
  // }

 getUserByName(userNameModel: UserNameModel): Observable<any> {
        console.log("user name model.."+JSON.stringify(userNameModel));
         let body = JSON.stringify(userNameModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/user/reqUserByUserName.on',body,options)
            .map((response: Response) => <IUser> response.json().userByUserName_info)
            .do(data => console.log('user Info by user name: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    getSkillByUserId(UserIdModel: UserIdModel): Observable<any> {
        console.log("user id model.."+JSON.stringify(UserIdModel));
         let body = JSON.stringify(UserIdModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/user/reqUserSkillsByUserid.on',body,options)
            .map((response: Response) => <IUserSkills> response.json().UserSkills_info)
            .do(data => console.log('user skills by user id: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    getResumeAndLinksByUserId(UserIdModel: UserIdModel): Observable<any> {
        console.log("user id model.."+JSON.stringify(UserIdModel));
         let body = JSON.stringify(UserIdModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/user/reqResumeAndLinksByUserid.on',body,options)
            .map((response: Response) => <IUserResumeAndLinks> response.json().UserResumeAndLinks_info)
            .do(data => console.log('user resume links by user id: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

 updateUser(user: UserCommunity) {
    let tokenUrl1 = ApiSettings.API_ENDPOINT +"/rest/user/update";
    let headers1 = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
    return this.http.post(tokenUrl1, JSON.stringify(user), {headers: headers1});
  }
  
}