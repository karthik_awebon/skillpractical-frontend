import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserIdModel } from 'app/community/userid.model';
import { Router } from '@angular/router';
import { ToastrConfig, ToastrService } from 'toastr-ng2';
import { UserService } from 'app/community/user.service';
import { DeletePostModel } from 'app/community/myposts/deletepost.model';
import { IPost } from 'app/community/interfaces/postInterface';
import { PostService } from 'app/community/post.service';
import { IAnswers } from 'app/community/interfaces/answers';
import { DeleteCommentModel } from 'app/community/deletecomment.model';
import { PostComments } from '../post-comments.component';
@Component({
  selector: 'app-my-answers',
  templateUrl: './my-answers.component.html',
  styleUrls: ['./my-answers.component.css']
})
export class MyAnswersComponent implements OnInit {
  
  @ViewChild('closeBtn') closeBtn: ElementRef;
  answers : IAnswers[];
  errorMessage:string;
  userid: string;
  modeldeleteanswer = new DeleteCommentModel('','');
  modeluserid = new UserIdModel('');
  //initializing p to one
  p: number = 1;

    constructor ( private toastrService: ToastrService,private userService: UserService, private postService: PostService,
      private _router: Router,private toastrConfig: ToastrConfig, private postComments:PostComments){
      toastrConfig.timeOut = 2000;
    }

   ngOnInit():void {     
            this.userid =  localStorage.getItem("userid");
            this.modeluserid = new UserIdModel(this.userid);
            this.postService.getAnswersByUser(this.modeluserid)
                    .subscribe(
                        (data) => {
                          console.log(data);
                          this.answers = data;
                        
                          console.log("Answers from User id....."+JSON.stringify(this.answers)+JSON.stringify(this.modeluserid))                  
                          },
                      error => this.errorMessage = <any>error)           
   }


   deleteAnswer(commentId){
        // this.modeldeletepost = new DeletePostModel(post.key1,post.postid,this.userid)
        
        this.modeldeleteanswer.commentId = commentId;
        this.modeldeleteanswer.userid = this.userid;
        this.postService.deleteAnswer(this.modeldeleteanswer)
        .subscribe(
            (data) => {
                          
                      this.postComments.onClickRestoreCommentCount();

                      this.ngOnInit();
                      this.closeBtn.nativeElement.click();
                      this.toastrService.success('Your Answer is Successfully Deleted!');

                      console.log("delete answer log",this.modeldeleteanswer);
              },
          error => this.errorMessage = <any>error)    
        }
  }