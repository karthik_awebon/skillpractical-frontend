import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { Subject, Observable} from 'rxjs/RX';
import { PostIdModel} from './postid.model';
import { IComment } from './interfaces/commentInterface'

import { PostCommentModel } from './postcomment.model';

import { ApiSettings } from '../ApiSettings';
import { Ikeyandurlforfile } from 'app/community/interfaces/commentresponse.interface';
import { DeleteCommentModel } from 'app/community/deletecomment.model';

@Injectable()
export class CommentService {

  constructor (private http:Http) {}

   private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

  private extractData(res: Response) {
        let body = res.json();
        return body.fields || { };
    }

getCommentsByPost(postIdModel: PostIdModel): Observable<any> {
        console.log("postidmodel for comments.."+JSON.stringify(postIdModel));
         let body = JSON.stringify(postIdModel);
            let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
                 return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqCommentByPostId.on',body,options)
                    .map((response: Response) => <IComment[]> response.json().Comments_info)
                    .do(data => console.log('Comment Info by post id' +  JSON.stringify(data)))
                    .catch(this.handleError);      
    }

  pushFileToStorage(file: File) : Observable<any> {
      let formdata: FormData = new FormData();
      formdata.append('file', file);
      let headers = new Headers({ 'Authorization': 'Bearer '+localStorage.getItem("token") });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(ApiSettings.API_ENDPOINT+'/rest/community/requploadcommentfileInfo.on',formdata,options)
                  .map((response: Response) => <Ikeyandurlforfile> response.json().keyandsignedurl)
                  .do(data => console.log('file url after post is uploaded ' +  JSON.stringify(data)))
                  .catch(this.handleError);     
  }

  private postComment = ApiSettings.API_ENDPOINT +'/rest/community/reqCreateComment.on';
   
  PostNewComment(postCommentModel: PostCommentModel) : Observable<any> {
          console.log("post comment model"+JSON.stringify(postCommentModel));
        let body = JSON.stringify(postCommentModel);
        let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.postComment, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
         }


    private postCommentCount = ApiSettings.API_ENDPOINT +'/rest/community/reqUpdateCommentInfo.on';
   
  PostCommentCount(postIdModel: PostIdModel) : Observable<any> {
          console.log("post comment model"+JSON.stringify(postIdModel));
        let body = JSON.stringify(postIdModel);
        let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.postCommentCount, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
         }

        
         private restoreCommentCount = ApiSettings.API_ENDPOINT +'/rest/community/reqRestoreCommentCount.on';
   
         RestoreCommentCount(postIdModel: PostIdModel) : Observable<any> {
                 console.log("post comment model"+JSON.stringify(postIdModel));
               let body = JSON.stringify(postIdModel);
               let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
               let options = new RequestOptions({ headers: headers });
       
               return this.http.post(this.restoreCommentCount, body, options)
                               .map(this.extractData)
                               .catch(this.handleError);
          }
       

         private postDeleteComment = ApiSettings.API_ENDPOINT +'/rest/community/reqDeleteComment.on';
         
        PostDeleteComment(DeleteCommentModel: DeleteCommentModel) : Observable<any> {
                console.log("delete comment model"+JSON.stringify(DeleteCommentModel));
              let body = JSON.stringify(DeleteCommentModel);
              let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
              let options = new RequestOptions({ headers: headers });
      
              return this.http.post(this.postDeleteComment, body, options)
                              .map(this.extractData)
                              .catch(this.handleError);
               }
      
      

}
