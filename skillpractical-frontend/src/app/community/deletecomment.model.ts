export class DeleteCommentModel{
    constructor(
        public userid:string,
        public commentId:string
    ){}
}