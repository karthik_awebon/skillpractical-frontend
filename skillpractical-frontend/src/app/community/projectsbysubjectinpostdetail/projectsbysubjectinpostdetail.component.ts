import {Component, Input} from '@angular/core';
import {PostService} from '../post.service';

import {  ActivatedRoute, Router } from '@angular/router';
import { Subscription }       from 'rxjs/Subscription';

import { NgForm } from '@angular/forms';
import { IUser } from '../interfaces/userInterface'

import {PostTagModel } from '../postsbytag/tag.model';
import { window } from 'rxjs/operator/window';
import { IProject } from 'app/projects';
import { ProjectSubjectModel } from 'app/community/projectsbysubjectinpostdetail/subject.model';


@Component({
  selector: 'projects-by-subject-inpostdetail',
  templateUrl: 'projectsbysubjectinpostdetail.component.html',
  styleUrls: ['projectsbysubjectinpostdetail.component.css']
})
export class ProjectBySubjectInPostDetail {
  @Input() subject: string;

  projects : IProject[];
  errorMessage:string;

    posttag:string;
    private sub: Subscription;
    modelprojectsubject = new ProjectSubjectModel('');
  
constructor (private _route: ActivatedRoute, private postService: PostService, private _router: Router){
   
}
   ngOnInit():void {       

            this.modelprojectsubject = new ProjectSubjectModel(this.subject);
            
            this.postService.getProjectsListBySubjectAndTrending(this.modelprojectsubject)
                    .subscribe(
                        (data) => {
                          this.projects = data;
                          console.log("projects from project subject....."+JSON.stringify(this.projects))                  
                          },
                      error => this.errorMessage = <any>error)  
  
   }

   onclickrelatedproject(): void{
      location.reload();
   }
}
