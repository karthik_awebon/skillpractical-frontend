import {Component, Input} from '@angular/core';
import {PostService} from '../post.service';
import {UserService} from '../user.service';
import {IPost} from '../interfaces/postInterface';

import {  ActivatedRoute, Router } from '@angular/router';
import { Subscription }       from 'rxjs/Subscription';

import { NgForm } from '@angular/forms';
import { IUser } from '../interfaces/userInterface'

import { window } from 'rxjs/operator/window';
import { PostBranchModel } from 'app/community/trendingposts/postbranch.model';
@Component({
  selector: 'posts-trending',
  templateUrl: 'trendingposts.component.html',
  styleUrls: ['trendingposts.component.css']
})
export class TrendingPosts {
  @Input() post : IPost[];
    
  
constructor (){
   
}

}
