import { HomeComponent } from './home.component';
import { MyPosts } from './myposts/myposts.component';
import { PostDetail } from './post-detail.component'
import { AddPost } from './add-post.component';
import { PostsByTag } from './postsbytag/postsbytag.component';

import {LoginGuard}  from '../user/routerguard/loginguard.component'
import { PostsByTagInPostDetail } from 'app/community/postsbytaginpostdetail/postsbytaginpostdetail.component';
import { ProjectQueryDetail } from 'app/community/projectqueries/projectqueries/projectqueriesdetail.component';
import { ProjectqueriesAllListComponent } from 'app/community/projectqueries/projectqueries/projectqueries-all-list.component';
import { NewProjectQueries } from 'app/community/projectqueries/projectqueries/newprojectqueries.list.component';
import { NewProjectQueryDetail } from 'app/community/projectqueries/projectqueries/newprojectqueriesdetail.component';
import { NewProjectQueryFormComponent } from 'app/community/projectqueries/projectqueries/newprojectqueryform.component';
import { MyAnswersComponent } from 'app/community/my-answers/my-answers.component';

export const communityRoutes = [
    {path:'homecomponent', component: HomeComponent,canActivate: [LoginGuard] },
    {path:'myposts', component: MyPosts},
    {path: 'myanswers', component: MyAnswersComponent},
    {path: 'postdetail/:id/:tag', component: PostDetail},
    {path:'addpost', name:'AddPost', component: AddPost },
    {path: 'postsbytag/:tag', component: PostsByTag},
    {path: 'projectqueries', component: ProjectqueriesAllListComponent},
    {path: 'projectqueries/existingnewprojectgeneralquerydetail/:id', component: ProjectQueryDetail},
    
    {path: 'newprojectqueries',component:NewProjectQueries},
    {path: 'newprojectqueries/newprojectquerydetail/:id',component:NewProjectQueryDetail},
    {path: 'projectqueries/newprojectqueryform',component:NewProjectQueryFormComponent},

    
    
//    {path:'login', component: Login}
]