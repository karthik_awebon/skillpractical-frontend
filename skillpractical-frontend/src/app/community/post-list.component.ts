import { Component, Input } from '@angular/core'
import {PostService} from './post.service';
import {IPost} from './interfaces/postInterface';
import {Router} from '@angular/router';
import {Observable}     from 'rxjs/Observable';
import { PostIdModel } from 'app/community/postid.model';


@Component({

  selector: 'post-list',
  styleUrls: ['post-list.component.css'],
  templateUrl: 'post-list.component.html'
})
export class PostList {
@Input() post:IPost

public short:any;

attachment:boolean = false;

model = new PostIdModel(null);

  constructor (private postService: PostService, private _router: Router) {
      
  }

  ngOnInit(){
  }

  onclickViewsCount(postid){
    this.model.postid = postid;
    this.postService.UpdateViewsCount(this.model)
                    .subscribe(
                      data => {
                    
                            // console.log('success: ', data)
                      },
                      err => console.log('error: ', err)
                    );  
  }



}
