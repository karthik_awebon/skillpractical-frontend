import {UserCommunity} from './usercommunity';
import {Injectable} from '@angular/core';
import {Post} from './post';
import { Router } from '@angular/router'
import { Http, Response, Headers, RequestOptions } from '@angular/http'
import { Subject, Observable} from 'rxjs/RX';

import { IPost} from './interfaces/postInterface';
import { PostIdModel } from './postid.model';
import { UserIdModel } from './userid.model';


import { PostCommentModel } from './postcomment.model';
import { PostTagModel } from './postsbytag/tag.model';
import { ITag } from './interfaces/posttagInterface';
import { IUserCount } from './interfaces/usercount.interface';

import { SavePostModel } from './savepost.model';

import {UnSavePostModel} from './unsavepost.model';

import {CheckSavedPostModel} from './checksavedpost.model';

import {ICheckSavedPost} from './checksavedpost.interface';

import {UserNameModel} from './username.model';

import { ApiSettings } from '../ApiSettings';

import { ICommunityMessage } from './interfaces/communitymessage.interface';
import {DeletePostModel} from '././myposts/deletepost.model';
import { PostBranchModel } from 'app/community/trendingposts/postbranch.model';
import { ProjectSubjectModel } from 'app/community/projectsbysubjectinpostdetail/subject.model';
import { IProject } from 'app/projects';
import { ProjectBranchModel } from 'app/community/trendingprojects/projectbranch.model';
import { PostTagAndUserIdModel } from 'app/community/postsbytag/useridandtag.model';
import { TagBranchModel } from 'app/community/tagbranch.model';
import { DeleteCommentModel } from 'app/community/deletecomment.model';
import { IAnswers } from 'app/community/interfaces/answers';
import { OffsetAndLimitModel } from './offsetandlimit.model';

@Injectable()
export class PostService {

  constructor (private http : Http) {}

 private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

private extractData(res: Response) {
        let body = res.json();
        return body.fields || { };
    }
 private _postsUrl = ApiSettings.API_ENDPOINT +'/rest/community/reqAllPosts.on';

   getPosts(): Observable<IPost[]> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token") });
    let options = new RequestOptions({ headers: headers });
        return this.http.get(this._postsUrl,options)
            .map((response: Response) => <IPost[]> response.json().posts_list)
            .do(data => console.log('All Posts: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }


    getPostsByBranch(postBranchModel: PostBranchModel): Observable<any> {
        console.log("Post Branch Model.."+JSON.stringify(postBranchModel));
         let body = JSON.stringify(postBranchModel);
            let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqPostsByBranch.on',body,options)
            .map((response: Response) => <IPost[]> response.json().posts_By_Branch)
            .do(data => console.log('Posts by post branch from service: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    private _tagsUrl = ApiSettings.API_ENDPOINT +'/rest/community/getAllTags.on';
    
    getAllTags(): Observable<ITag[]> {
        let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });
            return this.http.get(this._tagsUrl,options)
                .map((response: Response) => <ITag[]> response.json().alltags)
                .do(data => console.log('All Tags: ' +  JSON.stringify(data)))
                .catch(this.handleError);
        }

private _PostsByUseridurl = ApiSettings.API_ENDPOINT +'/rest/community/reqPostByUser.on';

  getPostsByUser(userIdModel: UserIdModel): Observable<any> {
        console.log("postidmodel.."+JSON.stringify(userIdModel));
         let body = JSON.stringify(userIdModel);
            let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this._PostsByUseridurl,body,options)
            .map((response: Response) => <IPost[]> response.json().Post_info)
            .do(data => console.log('Post Info by Post Id: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }
    private _PostsAnswersurl = ApiSettings.API_ENDPOINT +'/rest/community/reqAnswersByUser.on';

    getAnswersByUser(userIdModel: UserIdModel): Observable<any> {
        console.log("postidmodel.."+JSON.stringify(userIdModel));
         let body = JSON.stringify(userIdModel);
            let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this._PostsAnswersurl,body,options)
            .map((response: Response) => <IAnswers[]> response.json().Answers_info)
            .do(data => console.log('Answers by User Id: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

  getPostById(postIdModel: PostIdModel): Observable<any> {
        console.log("postidmodel.."+JSON.stringify(postIdModel));
         let body = JSON.stringify(postIdModel);
            let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqPostByPostId.on',body,options)
            .map((response: Response) => <IPost[]> response.json().Post_info)
            .do(data => console.log('Post Info by Post Id: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

  private postSharesCount = ApiSettings.API_ENDPOINT +'/rest/community/reqUpdateSharesInfo.on';
   
  PostSharesCount(postIdModel: PostIdModel) : Observable<any> {
          console.log("post comment model"+JSON.stringify(postIdModel));
        let body = JSON.stringify(postIdModel);
        let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.postSharesCount, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
         }


  getPostsByTag(postTagModel: PostTagModel): Observable<any> {
        console.log("posttagmodel.."+JSON.stringify(postTagModel));
         let body = JSON.stringify(postTagModel);
            let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqPostsByPostTag.on',body,options)
            .map((response: Response) => <IPost[]> response.json().posts_ByPostTag)
            .do(data => console.log('Posts by post tag: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    getPostsListByTagAndTrending(postTagModel: PostTagModel): Observable<any> {
        console.log("posttagmodel.."+JSON.stringify(postTagModel));
         let body = JSON.stringify(postTagModel);
            let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqPostsByPostTagAndTrending.on',body,options)
            .map((response: Response) => <IPost[]> response.json().posts_ByPostTagAndTrending)
            .do(data => console.log('Posts by post tag: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }



    getTrendingPostsByBranch(postBranchModel: PostBranchModel): Observable<any> {
        console.log("PostBranchModel.."+JSON.stringify(postBranchModel));
         let body = JSON.stringify(postBranchModel);
            let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqPostsByPostBranch.on',body,options)
            .map((response: Response) => <IPost[]> response.json().posts_ByPostBranch)
            .do(data => console.log('Posts by post branch and trending: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }



     private _postsTrendingByBranchAll = ApiSettings.API_ENDPOINT +'/rest/community/reqPostsByPostBranchAll.on';

    getTrendingPostsByBranchAll(): Observable<IPost[]> {
        let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });
            return this.http.get(this._postsTrendingByBranchAll,options)
                .map((response: Response) => <IPost[]> response.json().posts_ByPostBranchAll)
                .do(data => console.log('Trending Posts by branch all ' +  JSON.stringify(data)))
                .catch(this.handleError);
        }


     private _postsUrlWhereUnanswered = ApiSettings.API_ENDPOINT +'/rest/community/reqPostsWhereUnanswered.on';

   getPostsWhereUnanswered(): Observable<IPost[]> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token") });
    let options = new RequestOptions({ headers: headers });
        return this.http.get(this._postsUrlWhereUnanswered,options)
            .map((response: Response) => <IPost[]> response.json().posts_list)
            .do(data => console.log('Posts Where Unanswered: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

//      private _postsUrlWhereNewest = ApiSettings.API_ENDPOINT +'/rest/community/reqPostsWhereNewest.on';

//    getPostsWhereNewest(): Observable<IPost[]> {
//     let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token") });
//     let options = new RequestOptions({ headers: headers });
//         return this.http.get(this._postsUrlWhereNewest,options)
//             .map((response: Response) => <IPost[]> response.json().posts_list)
//             .do(data => console.log('Posts Where Unanswered: ' +  JSON.stringify(data)))
//             .catch(this.handleError);
//     } 
// OffsetAndLimitModel

   getPostsWhereNewest(OffsetAndLimitModel: OffsetAndLimitModel): Observable<any> {
        console.log("offset and limit model .."+JSON.stringify(OffsetAndLimitModel));
        let body = JSON.stringify(OffsetAndLimitModel);
            let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqPostsWhereNewest.on',body,options)
            .map((response: Response) => <IPost[]> response.json().posts_list)
            .do(data => console.log('Posts Where Newest: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    private _postsUrlWhereLastweek = ApiSettings.API_ENDPOINT +'/rest/community/reqPostsWhereLastweek.on';

    getPostsWhereLastweek(): Observable<IPost[]> {
        let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });
            return this.http.get(this._postsUrlWhereLastweek,options)
                .map((response: Response) => <IPost[]> response.json().posts_list)
                .do(data => console.log('Posts Where Unanswered: ' +  JSON.stringify(data)))
                .catch(this.handleError);
        }


private _postsTagUrl = ApiSettings.API_ENDPOINT +'/rest/community/reqPostsTags.on';

   getPostsTag(): Observable<ITag[]> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token") });
    let options = new RequestOptions({ headers: headers });
        return this.http.get(this._postsTagUrl,options)
            .map((response: Response) => <ITag[]> response.json().posts_Tag)
            .do(data => console.log('Posts Tags: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }


    getAllTagsByBranch(TagBranchModel: TagBranchModel): Observable<any> {
        console.log("branch .."+JSON.stringify(TagBranchModel));
         let body = JSON.stringify(TagBranchModel);
            let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqTagByBranch.on',body,options)
            .map((response: Response) => <ITag[]> response.json().tagbybranch)
            .do(data => console.log('tag by branch in community: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


//   updatePost(post: Post) {
//     let url="http://localhost:8080/rest/photo/update";
//     let header = new Headers ({'Content-Type' : 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});

//     return this.http.post(url, JSON.stringify(post), {headers: header});
//   }


  private _usercounturl = ApiSettings.API_ENDPOINT +'/rest/community/reqgetUserCount.on';

   getUserCount(): Observable<IUserCount> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token") });
    let options = new RequestOptions({ headers: headers });
        return this.http.get(this._usercounturl,options)
            .map((response: Response) =>  <IUserCount> response.json().usercount_info)
            .do(data => console.log('User Count : ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }


   private _saveposturl = ApiSettings.API_ENDPOINT +'/rest/community/reqsavePost.on';

   postSavePost(savePostModel: SavePostModel) : Observable<any> {
        console.log("save post model from post service"+JSON.stringify(savePostModel));
        let body = JSON.stringify(savePostModel);
        let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this._saveposturl, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
    }

     private _unsaveposturl = ApiSettings.API_ENDPOINT +'/rest/community/requnsavePost.on';

   postUnSavePost(unsavePostModel: UnSavePostModel) : Observable<any> {
        console.log("un save post model from post service"+JSON.stringify(unsavePostModel));
        let body = JSON.stringify(unsavePostModel);
        let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this._unsaveposturl, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
    }

    private urlchecksavedpostwithuserid = ApiSettings.API_ENDPOINT +'/rest/community/checksavedpostbyuserid.on';

    checksavedpostwithuserid(checkSavedPostModel:CheckSavedPostModel): Observable<any> {
        console.log("model check saved post from post service.."+JSON.stringify(checkSavedPostModel));
         let body = JSON.stringify(checkSavedPostModel);
            let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this.urlchecksavedpostwithuserid,body,options)
            .map((response: Response) => <ICheckSavedPost> response.json().checksavedpostbyuserid_info)
            .do(data => console.log('Check saved post with user id: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

                                    // starred projects with user id

    getPostsStarredByUser(UserIdModel: UserIdModel): Observable<any> {
        console.log("user id model.."+JSON.stringify(UserIdModel));
         let body = JSON.stringify(UserIdModel);
            let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqselectStarredPostByUser.on',body,options)
            .map((response: Response) => <IPost[]> response.json().StarredPostsByUser_info)
            .do(data => console.log('Post Info by Post Id: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    private _communitymessageUrl = ApiSettings.API_ENDPOINT +'/rest/community/reqCommunityMessages.on';
    
       getCommunityMessage(): Observable<ICommunityMessage[]> {
        let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
        let options = new RequestOptions({headers: headers});
            return this.http.get(this._communitymessageUrl,options)
                .map((response: Response) => <ICommunityMessage[]> response.json().community_messages)
                .do(data => console.log('Community Messages : ' +  JSON.stringify(data)))
                .catch(this.handleError);
        }



        // private deletepost = ApiSettings.API_ENDPOINT +'/rest/community/reqDeletePost.on';
        
        deletPost(deletePostModel: DeletePostModel) : Observable<any> {
               console.log("post delete model"+JSON.stringify(deletePostModel));
             let body = JSON.stringify(deletePostModel);
             let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
             let options = new RequestOptions({ headers: headers });
     
             return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqDeletePost.on', body, options)
                             .map(this.extractData)
                             .catch(this.handleError);
              }

        deleteAnswer(DeleteCommentModel: DeleteCommentModel) : Observable<any> {
                console.log("post answer delete model"+JSON.stringify(DeleteCommentModel));
              let body = JSON.stringify(DeleteCommentModel);
              let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
              let options = new RequestOptions({ headers: headers });
      
              return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqDeleteComment.on', body, options)
                              .map(this.extractData)
                              .catch(this.handleError);
               }

                                    // projects

    getProjectsListBySubjectAndTrending(ProjectSubjectModel: ProjectSubjectModel): Observable<any> {
        console.log("project subject model.."+JSON.stringify(ProjectSubjectModel));
         let body = JSON.stringify(ProjectSubjectModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqProjectsbySubjectAndTrending.on',body,options)
            .map((response: Response) => <IProject[]> response.json().projectsbysubjectandtrending)
            .do(data => console.log('projects by project subject: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    getTrendingProjectsByBranch(ProjectBranchModel: ProjectBranchModel): Observable<any> {
        console.log("PostBranchModel for projects .."+JSON.stringify(ProjectBranchModel));
         let body = JSON.stringify(ProjectBranchModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqProjectsByBranch.on',body,options)
            .map((response: Response) => <IProject[]> response.json().projectByBranch)
            .do(data => console.log('Posts by post branch and trending: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    private _projectsTrendingByBranchAll = ApiSettings.API_ENDPOINT +'/rest/community/reqProjectsByBranchAll.on';
    
    getTrendingProjectsByBranchAll(): Observable<IProject[]> {
            let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token") });
            let options = new RequestOptions({ headers: headers });
                return this.http.get(this._projectsTrendingByBranchAll,options)
                    .map((response: Response) => <IProject[]> response.json().projectsListByBranchAll)
                    .do(data => console.log('Trending Posts by branch all ' +  JSON.stringify(data)))
                    .catch(this.handleError);
            }
    


                                // tags

        getPostsWhereUnansweredAndTag(PostTagModel: PostTagModel): Observable<any> {
        console.log("post tag model .."+JSON.stringify(PostTagModel));
         let body = JSON.stringify(PostTagModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqPostsListWhereUnansweredAndTag.on',body,options)
            .map((response: Response) => <IPost[]> response.json().PostsListWhereUnansweredAndTag)
            .do(data => console.log('posts unanswered by tag: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }
    
    getPostsWhereNewestAndTag(PostTagModel: PostTagModel): Observable<any> {
        console.log("post tag model .."+JSON.stringify(PostTagModel));
         let body = JSON.stringify(PostTagModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqPostsListWhereNewestAndTag.on',body,options)
            .map((response: Response) => <IPost[]> response.json().PostsListWhereNewestAndTag)
            .do(data => console.log('Posts by post branch and trending: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    getPostsWhereLastweekAndTag(PostTagModel: PostTagModel): Observable<any> {
        console.log("post tag model .."+JSON.stringify(PostTagModel));
         let body = JSON.stringify(PostTagModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqPostsListWhereLastweekAndTag.on',body,options)
            .map((response: Response) => <IPost[]> response.json().PostsListWhereLastweekAndTag)
            .do(data => console.log('Posts by post branch and trending: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    getPostsWhereInterestedAndSuggestedAndTag(PostTagModel: PostTagModel): Observable<any> {
        console.log("post tag model .."+JSON.stringify(PostTagModel));
         let body = JSON.stringify(PostTagModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqPostsListWhereInterestedAndSuggestedAndTag.on',body,options)
            .map((response: Response) => <IPost[]> response.json().PostsListWhereInterestedAndSuggestedAndTag)
            .do(data => console.log('Posts by post branch and trending: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    getPostsStarredByUserAndTag(PostTagAndUserIdModel: PostTagAndUserIdModel): Observable<any> {
        console.log("post tag and user id model .."+JSON.stringify(PostTagAndUserIdModel));
         let body = JSON.stringify(PostTagAndUserIdModel);
            let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(ApiSettings.API_ENDPOINT +'/rest/community/reqStarredPostListByUserAndTag.on',body,options)
            .map((response: Response) => <IPost[]> response.json().StarredPostListByUserAndTag)
            .do(data => console.log('Posts by post branch and trending: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }
     
    //views
        
    private updateViewsCount = ApiSettings.API_ENDPOINT +'/rest/community/reqUpdateViewsInfo.on';
   
    UpdateViewsCount(postIdModel: PostIdModel) : Observable<any> {
            console.log("post id model for update views"+JSON.stringify(postIdModel));
          let body = JSON.stringify(postIdModel);
          let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
          let options = new RequestOptions({ headers: headers });
  
          return this.http.post(this.updateViewsCount, body, options)
                          .map(this.extractData)
                          .catch(this.handleError);
     }

}
