export class PostCommentModel{
    constructor(
        public content:string,
        public postid:number,
        public tag:string,
        public username:string,
        public userid:string,
        public key1:string,
        public signedUrl1:string,
        public subscriptionstatus: string
    ){}
}