export class DeletePostModel{
    constructor(
        public key1:string,
        public postid:number,
        public userid:string,
        public tag:string
    ){}
}