import {Component, Input, ViewChild, ElementRef} from '@angular/core';
import {PostService} from '../post.service';
import {UserService} from '../user.service';
import {UserCommunity} from '../usercommunity';
import {IPost} from '../interfaces/postInterface';
import {Router} from '@angular/router';

import { UserNameModel } from '../username.model'
import { UserIdModel } from '../userid.model'

import { NgForm } from '@angular/forms';
import { IUser } from '../interfaces/userInterface';
import {DeletePostModel} from './deletepost.model';

import { ToastrService, ToastrConfig } from 'toastr-ng2';



@Component({
  selector: 'my-posts',
  templateUrl: 'myposts.component.html',
  styleUrls: ['myposts.component.css']
})
export class MyPosts {

  @ViewChild('closeBtn') closeBtn: ElementRef;
  post : IPost[];
  errorMessage:string;
  userid: string;
  modeldeletepost = new DeletePostModel('',null,'','');
  modeluserid = new UserIdModel('');
  //initializing p to one
  p: number = 1;
constructor ( private toastrService: ToastrService,private userService: UserService, private postService: PostService,
   private _router: Router,private toastrConfig: ToastrConfig){
  toastrConfig.timeOut = 2000;
}
   ngOnInit():void {     

            this.userid =  localStorage.getItem("userid");

            this.modeluserid = new UserIdModel(this.userid);

            this.postService.getPostsByUser(this.modeluserid)
                    .subscribe(
                        (data) => {
                          this.post = data;
                        
                          console.log("posts from post id....."+JSON.stringify(this.post))                  
                          },
                      error => this.errorMessage = <any>error)           
   }

   deletePost(post){

        // this.modeldeletepost = new DeletePostModel(post.key1,post.postid,this.userid)
        
        this.modeldeletepost.postid = post.postid;
        this.modeldeletepost.key1 = post.key1;
        this.modeldeletepost.userid = this.userid;
        this.modeldeletepost.tag = post.tag;
        this.postService.deletPost(this.modeldeletepost)
        .subscribe(
            (data) => {
                       this.ngOnInit();
                       this.closeBtn.nativeElement.click();

                      this.toastrService.success('Your Post is Successfully Deleted! ');
              },
          error => this.errorMessage = <any>error)    
        }

    // editPost(postid){

          
    // }

}
