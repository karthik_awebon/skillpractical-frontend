import {Injectable} from "@angular/core";
import {Post} from '../Post';
import {Observable}     from 'rxjs/Observable';

import { NewPostModel } from '../newpost.model'
import { Http, Response, Headers, RequestOptions } from '@angular/http'

import { ApiSettings } from '../../ApiSettings';
import {Ikeyandurlforfile} from '../interfaces/postresponse.interface';

@Injectable()
export class AddPostService {
  constructor (private http: Http) {}

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

  private extractData(res: Response) {
        let body = res.json();
        return body.fields || { 
        };
    }

  // sendPost(newPostModel:NewPostModel) {
  //   let url = "http://localhost:8080/rest/photo/add";
  //   let headers1 = new Headers({'Content-Type': 'application/json', 'Authorization':'Bearer '+localStorage.getItem("token")});
  //   return this.http.post(url, JSON.stringify(newPostModel), {headers: headers1});
  // }


   private newPostUrl = ApiSettings.API_ENDPOINT +'/rest/community/reqCreatePostInfo.on';
   
     sendPost(newPostModel: NewPostModel) : Observable<any> {
          console.log("new post model"+JSON.stringify(newPostModel));
        let body = JSON.stringify(newPostModel);
        let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.newPostUrl, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
    }


    pushFileToStorage(file: File) : Observable<any> {
        let formdata: FormData = new FormData();
        formdata.append('file', file);
        let headers = new Headers({ 'Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(ApiSettings.API_ENDPOINT+'/rest/community/requploadpostfileInfo.on',formdata,options)
                    .map((response: Response) => <Ikeyandurlforfile> response.json().keyandsignedurl)
                    .do(data => console.log('file url after post is uploaded ' +  JSON.stringify(data)))
                    .catch(this.handleError);     
    }

}
