export interface IComment {
  
  commentId: number;
  username: string;
  userid:string;
  content: string;
  postid: number;
  key1:string;
  signedUrl1:string;
  created:Date;
  subscriptionstatus:string;
}