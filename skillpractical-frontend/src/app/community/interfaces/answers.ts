
export interface IAnswers{
    
    content: string;
    tag:string;
    postName:string;
    postid: string;
    commentId: string;
    userid:string;

}