export interface IUser {
  
    userid: string;
    firstname: string;
    lastname: string;
    username: string;
    password: string;
    emailid:string;
    phonenumber:string;
    dob:string;
    address:string;
    state:string;
    district:string;
    created: Date;
    rollnumber:string;
    collegename:string;
    collegecode:string;
    fieldofstudy:string;
    branch:string;
    year:string;
    
}



export interface IUserSkills {
        skill1: string;
        rateyourself1:string;
        skill2: string;
        rateyourself2:string;
        skill3: string;
        rateyourself3:string;

}

export interface IUserResumeAndLinks {
    facebook:string;
    linkedin:string;
    twitter:string;
    googleplus:string;
}