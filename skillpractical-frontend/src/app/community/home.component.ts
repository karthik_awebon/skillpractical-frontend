import {Component} from '@angular/core';
import {PostService} from './post.service';
import {IPost} from './interfaces/postInterface';
import {Router} from '@angular/router';
import {Observable}     from 'rxjs/Observable';

import { ITag } from './interfaces/posttagInterface';
import { IUserCount } from './interfaces/usercount.interface';

import { UserNameModel } from './username.model';
import {UserIdModel} from './userid.model'
import { PostBranchModel } from 'app/community/trendingposts/postbranch.model';
import { IProject } from 'app/projects';
import { ProjectBranchModel } from 'app/community/trendingprojects/projectbranch.model';
import { TagBranchModel } from 'app/community/tagbranch.model';
import { OffsetAndLimitModel } from './offsetandlimit.model';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css']
})
export class HomeComponent {

  public listFilter:any;

  post : IPost[];
  // selectedPost: IPost;
  posttrending:IPost[];
  projectstrending:IProject[];
  errorMessage:string;
  searchTerm:string = "";
  foundPost: IPost[];
//   visiblePosts: IPost[] = [];
  
  usercount:IUserCount;

  posttag: ITag[];

  usernamemodel = new UserNameModel(null);

  userIdModel = new UserIdModel(null);

  userid:string;

  attachment:boolean = false;

  selectedbranch:string;
  
  modelbranch = new PostBranchModel('');

  modelprojectbranch = new ProjectBranchModel('');

       modeltagbranch = new TagBranchModel('');

       tagbybranch:ITag[];
  //initializing p to one
    p: number = 1;

    itemsPerPage: number = 20;
    loading: boolean;
    total: number = 100;

    modeloffsetandlimit = new OffsetAndLimitModel(null,null);
  
    constructor (private postService: PostService, private _router: Router) {
        
    }

     ngOnInit(){

            //   this.postService.getPosts()
            //           .subscribe(
            //               (data) => {
            //                  this.post = data;
            //                   console.log("post....."+JSON.stringify(this.post))                                         
            //                     },
            //                  error => this.errorMessage = <any>error);

            //enable this.
                        this.filterBynewest();   

                        // this.getPage(1);
             
             this.postService.getPostsTag()
                    .subscribe(
                        (data) => {
                            this.posttag = data;
                                // console.log("Post tags.."+JSON.stringify(this.posttag))                                  
                                },
                            error => this.errorMessage = <any>error);  

              this.postService.getUserCount()
                    .subscribe(
                        (data) => {
                            this.usercount = data;
                                console.log("user count .."+JSON.stringify(this.usercount))                                  
                                },
                            error => this.errorMessage = <any>error); 
                            
                            this.trendingprojectsbybranchall();
                            this.trendingpostbybranchall();
      }


      getPage(page: number) {  
        this.loading = true;
        var offset = (page-1)*(this.itemsPerPage);

            this.modeloffsetandlimit.limit = this.itemsPerPage;
            this.modeloffsetandlimit.offset = offset



      }



      postsByBranch(){
        this.modelbranch.branch = this.selectedbranch;   
        this.postService.getPostsByBranch(this.modelbranch)
                .subscribe(
                    (data) => {
                    this.post = data;
                    // console.log("posts by post branch....."+JSON.stringify(this.post))                  
                    },
                error => this.errorMessage = <any>error)  
      }


    //   trending post by branch
      trendingpostbybranch(){
        this.modelbranch.branch = this.selectedbranch;
        
            this.postService.getTrendingPostsByBranch(this.modelbranch)
                    .subscribe(
                        (data) => {
                        this.posttrending = data;
                        // console.log(" trending posts from post branch....."+JSON.stringify(this.post))                  
                        },
                    error => this.errorMessage = <any>error)  
      }


    tagsbybranch(){
        this.modeltagbranch.tag_branch = this.selectedbranch;
        
        this.postService.getAllTagsByBranch(this.modeltagbranch)
                .subscribe(
                    (data) => {
                    this.tagbybranch = data;   
                    },
                error => this.errorMessage = <any>error)  
      }

      
      trendingprojectsbybranch(){
        this.modelprojectbranch.project_branch = this.selectedbranch;
        
            this.postService.getTrendingProjectsByBranch(this.modelprojectbranch)
                    .subscribe(
                        (data) => {
                        this.projectstrending = data;
                        console.log(" trending projects from projects branch....."+JSON.stringify(this.projectstrending))                  
                        },
                    error => this.errorMessage = <any>error)  
                    
      }

      
    filterBy: string = 'all';
   
    dontshowrelatedtags:boolean;

    filterByall(){
        this.filterBy = 'all';

        this.selectedbranch='all'

            this.dontshowrelatedtags = false;

            this.tagbybranch = [];

            this.filterBynewest();  
            this.trendingpostbybranchall();
            this.trendingprojectsbybranchall();
         
    }

    // need to implement based on concept discussion
     filterBysuggested(){
         this.filterBy = 'suggested';
        
    }

     filterByunanswered(){
          this.filterBy = 'unanswered';  //to know that this button is clicked.
          this.postService.getPostsWhereUnanswered()
                    .subscribe(
                        (data) => {
                          this.post = data;
                        //   console.log("posts from post unanswered....."+JSON.stringify(this.post))                  
                          },
                      error => this.errorMessage = <any>error)             
    }

    filterBynewest(){
        
        this.filterBy = 'newest';
        this.postService.getPostsWhereNewest(this.modeloffsetandlimit)
                    .subscribe(
                        (data) => {
                          this.post = data;
                        //   console.log("posts from post Newest....."+JSON.stringify(this.post))                  
                          },
                      error => this.errorMessage = <any>error)   
    }

    filterBylastweek(){
         this.filterBy = 'lastweek';
          this.postService.getPostsWhereLastweek()
                    .subscribe(
                        (data) => {
                          this.post = data;
                        //   console.log("posts from post Lastweek....."+JSON.stringify(this.post))                  
                          },
                      error => this.errorMessage = <any>error)  
         
    }


    // filter by branch

    filterByelectrical(){
        this.selectedbranch = 'electrical'
        this.filterBy = 'electrical';
      
        this.dontshowrelatedtags = true;

        this.postsByBranch()
        this.trendingpostbybranch();
        this.trendingprojectsbybranch();
        this.tagsbybranch();
    }

    filterByelectronics(){
        this.selectedbranch = 'electronics'
         this.filterBy = 'electronics';
       
         this.dontshowrelatedtags = true;

        this.postsByBranch()
        this.trendingpostbybranch();
        this.trendingprojectsbybranch();
        this.tagsbybranch();
    }

    filterBycomputerscience(){
    this.selectedbranch = 'Computer Science'
       this.filterBy = 'computerscience';

       this.dontshowrelatedtags = true;
  
        this.postsByBranch()
        this.trendingpostbybranch();
        this.trendingprojectsbybranch();
        this.tagsbybranch();
    
   }

   filterBymechanical(){
       this.filterBy = 'mechanical';
       this.selectedbranch = 'mechanical'

       this.dontshowrelatedtags = true;
    
        this.postsByBranch()
        this.trendingpostbybranch();
        this.trendingprojectsbybranch();
        this.tagsbybranch();
   }

    filterBycivil(){
       this.filterBy = 'civil';
       this.selectedbranch = 'civil'

       this.dontshowrelatedtags = true;
    
        this.postsByBranch()
        this.trendingpostbybranch();
        this.trendingprojectsbybranch();
        this.tagsbybranch();
   }

   filterByother(){
       this.filterBy = 'other';
       this.selectedbranch = 'other'

       this.dontshowrelatedtags = true;

       this.postsByBranch()
       this.trendingpostbybranch();
       this.tagsbybranch();
   }


   trendingpostbybranchall(){
    this.postService.getTrendingPostsByBranchAll()
    .subscribe(
        (data) => {
          this.posttrending = data;
          console.log(" trending posts from post branch all....."+JSON.stringify(this.post))                  
          },
      error => this.errorMessage = <any>error)  
   }

   trendingprojectsbybranchall(){
        this.postService.getTrendingProjectsByBranchAll()
        .subscribe(
            (data) => {
            this.projectstrending = data;
            console.log(" trending projects from project branch all....."+JSON.stringify(this.projectstrending))                  
            },
        error => this.errorMessage = <any>error)  
    }

   

    //filter by starred
    filterBystarred(){

        this.filterBy = 'starred';

        this.userid =  localStorage.getItem("userid");
        this.userIdModel = new UserIdModel(this.userid);
        this.postService.getPostsStarredByUser(this.userIdModel)
                        .subscribe(
                            (data) => {
                            this.post = data;
                            console.log("posts starred....."+JSON.stringify(this.post))                  
                            },
                        error => this.errorMessage = <any>error)  
    }


}
                

