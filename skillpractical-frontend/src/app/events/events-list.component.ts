import { Component, Input,OnChanges } from '@angular/core'
import { EventService } from './shared/event.service'
import { ActivatedRoute } from '@angular/router'
import { IEvent,UserIdModel } from './shared/index'

import { UserNameModel } from './shared/username.model';

import {UserService} from '../community/user.service';
import { IUser } from '../community/interfaces/userInterface';

import {Router} from '@angular/router';
import { CollegeCodeModel } from './index';

@Component({
   selector: 'events-list',
   templateUrl:'event-list.component.html',
   styleUrls: ['event-list.component.css']
})
export class EventListComponent {
 searchTerm:string = "";
 foundEvents: IEvent[];

 public listFilter:any
 public linkToShare:any
 public title:any
 public description:any
 public image:any

 events:IEvent[]
 modelusername = new UserNameModel('');
  currentusername:string;
     errorMessage:string;

      user: IUser;
     
      userid:string;
      collegename: string;
      usernamemodel = new UserNameModel('');

    //   showattendedevents:boolean;
    
      collegecode:string;

      modeluserid = new UserIdModel('')

 constructor(private router:Router,private userService:UserService,private eventService: EventService, private route:ActivatedRoute){
    
 }

 collegecodemodel = new CollegeCodeModel('');
 showbanner :boolean;

 ngOnInit(){

    this.currentusername =  localStorage.getItem("currentUserName");
    
                this.modelusername = new UserNameModel(this.currentusername);
              
                this.userService.getUserByName(this.modelusername).subscribe(
                      user => {
                        this.user = user;
                        this.collegecode = this.user.collegecode;

                        this.collegecodemodel = new CollegeCodeModel(this.collegecode);

                        this.eventService.getEvents(this.collegecodemodel)                   
                        .subscribe(
                         (data) => {
                           this.events = data;
                           this.filterByall();
                          },
                       error => this.errorMessage = <any>error);  
                       console.log("model username for registered events"+JSON.stringify(this.modelusername))

                          console.log("user from event list on college based"+JSON.stringify(this.user.collegecode));
                          if(this.collegecode == '' || this.collegecode == "null" || this.collegecode == null){
                            this.showbanner = true;
                            console.log("collegecode"+this.collegecode);
                        }
                        
                      },
                      error => console.log(error)
                    )

//    this.events = this.route.snapshot.data['events']
  
 }

    filterBy: string = 'all';

   visibleEvents: IEvent[] = [];
   
   attendedEvents:IEvent[];

    filterByall(){
        this.filterBy = 'all';
             if(this.events){
            this.filterEvents(this.filterBy);
        }
    }

    filterByeducation(){
        this.filterBy = 'education';
         if(this.events){
            this.filterEvents(this.filterBy);
        }
    }

    filterBysports(){
         this.filterBy = 'sports';
         if(this.events){
            this.filterEvents(this.filterBy);
        }
    }

      filterBycultural(){
         this.filterBy = 'cultural';
         if(this.events){
            this.filterEvents(this.filterBy);
        }
    }


       filterByothers(){
         this.filterBy = 'others';
         if(this.events){
            this.filterEvents(this.filterBy);
        }
    }


    filterEvents(filter){
        if(filter === 'all'){
                this.visibleEvents = this.events.slice(0);
        }else {
                this.visibleEvents = this.events.filter(events => {
                    return events.genre.toLocaleLowerCase() === filter;
                })
        }
        console.log("visble events"+JSON.stringify(this.visibleEvents));
    }

    novisibleevents:boolean;

    filterByregisteredevents(){
         this.filterBy = 'registeredevents';

         this.userid =  localStorage.getItem("userid");

          this.modeluserid = new UserIdModel(this.userid);

           this.eventService.getEventsByUserId(this.modeluserid)                   
                              .subscribe(
                               (data) => {
                                 this.visibleEvents = data;
                                  console.log("events by username.."+JSON.stringify(this.visibleEvents))
                                  
                                },
                             error => this.errorMessage = <any>error);  
                             console.log("model username for registered events"+JSON.stringify(this.modelusername))
                            
    }  


        // filterByAttendedevents(){

        //         this.filterBy = 'attendedevents';

        //         this.showattendedevents = true;
               

        //          this.currentusername =  localStorage.getItem("currentUserName");

        //         this.usernamemodel = new UserNameModel(this.currentusername)
        //         this.eventService.getAttendedEventsByUserid(this.usernamemodel)                   
        //                 .subscribe(
        //                   (data) => {
        //                     this.visibleEvents = data;
        //                     console.log("attended events by userid.."+JSON.stringify(this.visibleEvents))                    
        //                     },
        //                     error => this.errorMessage = <any>error);  
        //             console.log("request attended events model"+JSON.stringify(this.usernamemodel))
        // }


        filterBymycertificates(){
            this.router.navigate(['events/mycertificates']);
        }

}