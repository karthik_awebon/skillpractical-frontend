import { Component, Input } from '@angular/core'
import { IEvent } from '../shared/index'

import { EventService } from '../shared/index'

@Component({
  selector: 'attended-events',
  templateUrl:'attendedevents.component.html', 
  styles: [`
      .thumbnail { min-height: 210px; }
    .pad-left { margin-left: 10px; }
    .well div { color: grey; }
  `]
})
export class AttendedEventsComponent {

   @Input() event:IEvent

   constructor(private eventService: EventService) {

          console.log("attended events"+this.event);
  }


  onclickreloadtorelatedprojects(){
    window.location.reload();
  }

}