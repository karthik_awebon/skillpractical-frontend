import { Component, Input } from '@angular/core'
import { IEvent } from '../shared/index'

import { EventService } from '../shared/index'
import { UpcomingEventsModel } from '../index'
import { Router } from '@angular/router';

@Component({
  selector: 'upcoming-events',
  templateUrl:'upcomingevents.component.html', 
  styles: [`
      .thumbnail { min-height: 210px; }
    .pad-left { margin-left: 10px; }
    .well div { color: grey; }
  `]
})
export class UpcomingEventsComponent {

   @Input() event:IEvent

   constructor(private eventService: EventService,private router: Router) {

          console.log("upcoming events"+this.event);
          this.router.routeReuseStrategy.shouldReuseRoute = function() {
            return false;
        };
        
  }

  

}