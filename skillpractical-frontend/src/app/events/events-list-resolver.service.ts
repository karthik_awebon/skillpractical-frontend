import { Injectable } from '@angular/core'
import { Resolve } from '@angular/router'
import { EventService } from './shared/event.service'

import { CollegeCodeModel } from './index';

import { UserNameModel } from '../community/username.model';
import {UserService} from '../community/user.service';
import { IUser } from '../community/interfaces/userInterface';

@Injectable()
export class EventListResolver implements Resolve<any> {

    constructor(private eventService: EventService,private userService:UserService){
        
    }

    collegecodemodel = new CollegeCodeModel('');

    user: IUser;

    currentusername:string;
    modelusername = new UserNameModel('');

    collegecode:string;

    
    ngOnInit() {
       
    }


    resolve(){      

            this.currentusername =  localStorage.getItem("currentUserName");
            
                        this.modelusername = new UserNameModel(this.currentusername);
                      
                        this.userService.getUserByName(this.modelusername).subscribe(
                              user => {
                                this.user = user;
                                this.collegecode = this.user.collegecode;

                                  console.log("user from event list on college based"+JSON.stringify(this.user.collegecode));
                                  this.collegecodemodel = new CollegeCodeModel(this.collegecode);
                              },
                              error => console.log(error)
                            )

                    // console.log("college code model"+JSON.stringify(this.user.collegecode));

            return this.eventService.getEvents(this.collegecodemodel).map(events => events )
        }
   
}