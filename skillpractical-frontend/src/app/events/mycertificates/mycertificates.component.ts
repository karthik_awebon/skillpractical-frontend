import { Component, Input,OnInit} from '@angular/core'
import { IEvent } from '../shared/index'

import {UserIdModel} from '../shared/userid.model'
import {EventService} from '../shared/event.service'
import {IEventCertificate} from '../shared/eventcertificate.interface'

@Component({
  selector: 'mycertificates-events',
  styleUrls: ['mycertificates.component.css'],
  templateUrl:'mycertificates.component.html', 

})
export class MyCertificatesComponent implements OnInit{

  userid:string;

  modeluserid = new UserIdModel('');
  eventcertificate:IEventCertificate[];
  errorMessage:string

   constructor(private eventService: EventService) {

    
        
  }

  ngOnInit(){
    this.userid =  localStorage.getItem("userid");
    
              this.modeluserid = new UserIdModel(this.userid);
    
               this.eventService.getEventsCertificatesByUserId(this.modeluserid)                   
                                  .subscribe(
                                   (data) => {
                                     this.eventcertificate = data;
                                      console.log("events certificate by username.."+JSON.stringify(this.eventcertificate))
                                      
                                    },
                                 error => this.errorMessage = <any>error);  

  }

}