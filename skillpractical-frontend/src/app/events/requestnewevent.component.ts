import { Component, Input, ViewContainerRef } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import { RequestNewEventModel } from './index';

import { NgForm } from '@angular/forms';

import { EventService, UserNameModel } from './shared/index'

import {UserService} from '../community/user.service';

import { IUser } from '../community/interfaces/userInterface'

import { ToastrService, ToastrConfig } from 'toastr-ng2';

@Component({
  selector: 'new-event-query-form',
  templateUrl:'requestnewevent.component.html',
  styleUrls: ['requestnewevent.component.css']
  
})
export class NewEventQueryFormComponent {

  @ViewChild('closeBtn') closeBtn: ElementRef;

    currentusername:string;
    model = new RequestNewEventModel('','','','','','','');
    modelusername = new UserNameModel('');

    user: IUser;

    userid: string;
    username: string;
    collegecode:string;

   constructor(private toastrService: ToastrService,private eventService: EventService,
    private userService:UserService,private toastrConfig: ToastrConfig) {
      toastrConfig.timeOut = 2000;
   }

    ngOnInit() {

            this.currentusername =  localStorage.getItem("currentUserName");

            this.modelusername = new UserNameModel(this.currentusername);
          
            this.userService.getUserByName(this.modelusername).subscribe(
                  user => {
                    this.user = user;
                    this.collegecode = this.user.collegecode;

                      console.log("user from new event queryform component"+JSON.stringify(this.user));
                    
                  },
                  error => console.log(error)
                )
    }
  

    submitForm(form: NgForm) {
     
                this.model.userid = this.user.userid;
                this.model.username = this.user.username;
                this.model.collegecode = this.collegecode;
                // this.model = new QueryFormModel(this.user.userid,this.user.username,'','','')
                 this.eventService.postNewEventQueryForm(this.model)
                                  .subscribe(
                                    data => {     
                                    
                                      this.toastrService.success('Request Successful!');
                                      this.closeBtn.nativeElement.click();
                                      form.reset();

                                    },
                                    err => console.log('error: ', err)
                                  );
                                  console.log("new event request form model"+JSON.stringify(this.model));
              }
              closemodel(form: NgForm){
                form.reset();
              }
    }
