export class UnRegisterEvent {
    constructor(
        public userid:string,
        public username:string,
        public emailid:string,
        public eventid:number,
        public name:string,
        public date:Date
    ) {

    }
}