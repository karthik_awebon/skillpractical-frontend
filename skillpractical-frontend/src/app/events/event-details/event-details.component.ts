import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import { IEvent, ISession, EventService } from '../shared/index'
import { Subscription } from 'rxjs/Subscription';
import { EventIdModel, ISponsors } from '../index'
import { UserNameModel } from '../../community/username.model';
import {UserService} from '../../community/user.service';
import { IUser } from '../../community/interfaces/userInterface';
import { RegisterEvent } from './register-event.model';
import { CheckRegisteredEvent } from '../shared/checkregisteredevent.model';
import { ICheckRegisteredEvent } from '../shared/checkregisteredevent.interface';
import { UpcomingEventsModel } from '../shared/upcomingevents.model';
import { ToastrService } from 'toastr-ng2';
import { UnRegisterEvent } from 'app/events/event-details/unregister-event.model';
@Component({
  templateUrl: 'event-details.componnet.html',  
  styleUrls: ['event-details.component.css']
})
export class EventDetailsComponent implements OnInit {
  errorMessage:string;
 public linkToShare:any
 public title:any
 public description:any
 public image:any
  event:IEvent
  addMode: boolean
  filterBy: string = 'all';
  
  private sub: Subscription;
  eventid:number;
  
  eventdate:string;
  sessions: ISession[]
  model = new EventIdModel(null);
  currentusername:string;
  modelusername = new UserNameModel('');
  user: IUser;
  userid:string;
  // eventidd:number;
  modelregisterevent = new RegisterEvent('','','','',null,'',null,'','','','','','');
  eventalreadyregistered : boolean = false;
  checkregisteredeventmodel = new CheckRegisteredEvent(null,'');
  upcomingeventsmodel = new UpcomingEventsModel('','');
  modelunregiterevent = new UnRegisterEvent('','','',null,'',null);
  checkregisteredevent: ICheckRegisteredEvent;
  checkregistrationstatus:ICheckRegisteredEvent;
  //this id is used to display register and unregister button.
  checkregisteredeventid: any;
  registrationstatus: string;
  upcomingevents: IEvent[];
  
  eventidmodelforsponsors = new EventIdModel(null);
  eventsponsors: ISponsors[];
  constructor(private toastrService: ToastrService,private eventService: EventService, 
    private route: ActivatedRoute,private userService:UserService) {
          
  }
  public repoUrl:string;
    showSuccessCancelRegistration() {
    this.toastrService.success('Cancel Event request Successful!');
  }
   showSuccessRegister() {
    this.toastrService.success('Registration request Successful!');
  }
  
      
  ngOnInit() {
         this.userid =  localStorage.getItem("userid");
         this.currentusername = localStorage.getItem("currentUserName");
          this.sub = this.route.params.subscribe(
                    params => {                
                        let id = params['id'];                      
                        this.eventid = id;                     
                }); 
                   this.model = new EventIdModel(this.eventid);
                   this.eventService.getEvent(this.model)
                            .subscribe(
                            (event) => {
                                this.event = event;
                                this.checkregistereventmethod();
                                
                                console.log("event.."+JSON.stringify(this.event))
                                
                                this.eventdate = event.eventdate;
                        //upcoming events
                        this.upcomingeventsmodel = new UpcomingEventsModel(this.event.genre,this.event.collegecode)
                        this.eventService.getUpcomingEventsByGenre(this.upcomingeventsmodel)                   
                                  .subscribe(
                                      (data) => {
                                        this.upcomingevents = data;
                                          console.log("upcoming events by genre and college.."+JSON.stringify(this.upcomingevents))                    
                                        },
                                    error => this.errorMessage = <any>error);  
                                    console.log("request upcoming events model"+JSON.stringify(this.upcomingeventsmodel))            
                                    
                        //sponsors
                        this.eventidmodelforsponsors = new EventIdModel(this.event.eventid)
                        this.eventService.getEventSponsorByEventid(this.eventidmodelforsponsors)                   
                                  .subscribe(
                                      (data) => {
                                        this.eventsponsors = data;
                                          console.log("event sponsors by event id.."+JSON.stringify(this.eventsponsors))                    
                                        },
                                    error => this.errorMessage = <any>error);  
                                    console.log("request event sponsors model"+JSON.stringify(this.eventidmodelforsponsors))               
                        },
                        error => this.errorMessage = <any>error);  
        
          // this.model = new EventIdModel(this.eventid);
          this.eventService.getSessionsByEventId(this.model)
                              .subscribe(
                               (sessions) => {
                                 this.sessions = sessions;
                                  console.log("event.."+JSON.stringify(this.sessions))     
                                },
                             error => this.errorMessage = <any>error);  
                             console.log("model sessions"+JSON.stringify(this.model))         
                      
                                          
        }
        checkregistereventmethod(){
              // check for registered events
              this.checkregisteredeventmodel = new CheckRegisteredEvent(this.event.eventid,this.userid);
              this.eventService.checkregisteredeventwithuserid(this.checkregisteredeventmodel)                           
                                  .subscribe(
                                      (data) => {
                                         this.checkregisteredevent = data;
                                         this.registrationstatus = this.checkregisteredevent.registration_status;
                                         this.checkregisteredeventid = this.checkregisteredevent.eventid; 
                        //  this.registrationstatus = this.checkregisteredevent.registration_status;                        
              console.log("registered event details event id.."+ this.checkregisteredeventid)
              console.log("current event id"+this.eventid);
              // registration status (confirmed or pending)
              this.checkregisteredeventmodel = new CheckRegisteredEvent(this.event.eventid,this.userid);
              this.eventService.checkregistrationstatuswithuserid(this.checkregisteredeventmodel)                           
                                  .subscribe(
                                      (data) => {
                                         this.checkregistrationstatus = data;         
                            this.registrationstatus = this.checkregistrationstatus.registration_status; 
                          //  this.registrationstatus = this.checkregisteredevent.registration_status;                        
                console.log("registered event details event id.."+ this.checkregisteredeventid)
                console.log("current event id"+this.eventid);
                                                 },
                            error => this.errorMessage = <any>error);  
                                                 },
                            error => this.errorMessage = <any>error);  
                console.log("model checkregistered event"+JSON.stringify(this.checkregisteredeventmodel)); 
                // console.log("event registered true or false"+this.eventalreadyregistered);       
        }
    emailid: string
    phonenumber :string
    isValid:boolean = true;
    isValidForm() {
       return this.isValid;
   }
    registerforevent(){
      this.isValid = false;
              this.currentusername = localStorage.getItem("currentUserName");
              this.modelusername = new UserNameModel(this.currentusername); 
                                      
              this.userService.getUserByName(this.modelusername).subscribe(
                                user => {
                                  this.user = user;
                                  this.emailid = this.user.emailid;
                                  this.phonenumber = this.user.phonenumber;
                                  this.modelregisterevent = new RegisterEvent(this.userid,this.currentusername,this.emailid,this.phonenumber,
                                    this.event.eventid,this.event.name,this.event.date,
                                    this.event.time,this.event.price,this.event.mode,this.event.subject,this.event.genre,this.event.address);
                
                                            this.eventService.postRegisterEventForm(this.modelregisterevent)
                                                  .subscribe(
                                                    data => {
                                                      console.log('success: ', data)
                                                      this.isValid = true;
                                                      this.ngOnInit();
                
                                                    },
                                                    err => console.log('error: ', err)
                                                  );
                                                  console.log("register event model"+JSON.stringify(this.modelregisterevent));
                               
                                  },
                                  error => console.log(error)
                                ) 
      
    }
     unregisterforevent(){  
      this.isValid = false;
      this.currentusername = localStorage.getItem("currentUserName");
      this.modelusername = new UserNameModel(this.currentusername); 
                              
      this.userService.getUserByName(this.modelusername).subscribe(
                        user => {
                          this.user = user;
                          this.emailid = this.user.emailid;
                          this.phonenumber = this.user.phonenumber;
                          this.modelregisterevent = new RegisterEvent(this.userid,this.currentusername,this.emailid,this.phonenumber,
                            this.event.eventid,this.event.name,this.event.date,
                            this.event.time,this.event.price,this.event.mode,this.event.subject,this.event.genre,this.event.address);
        
                            this.modelunregiterevent = new UnRegisterEvent(this.userid,this.currentusername,this.emailid,this.event.eventid,this.event.name,this.event.date);
                            this.eventService.checkregisteredeventwithuseridandunregister(this.modelunregiterevent)
                                                .subscribe(
                                                  data => {
                                                    console.log('success: ', data)
                                                    this.isValid = true;
                                                    this.ngOnInit();
              
                                                  },
                                                  err => console.log('error: ', err)
                                                );
                                                console.log("unregister event model"+JSON.stringify(this.modelunregiterevent));
                       
                          },
                          error => console.log(error)
                        ) 
            
               
    }
    
}