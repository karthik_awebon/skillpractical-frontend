export class RegisterEvent {
    constructor(
        public userid:string,
        public username:string,
        public emailid:string,
        public phonenumber:string,
        public eventid:number,
        public name:string,
        public date:Date,
        public time:string,
        public price: string,
        public mode: string,
        public subject: string,
        public genre: string,
        public address:string
    ) {

    }
}