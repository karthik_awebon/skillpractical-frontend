import { Component, Input, OnChanges } from '@angular/core'
import { ISession } from '../shared/index'

@Component({
    selector:'session-list',
    templateUrl:'session-list.component.html'
})

export class SessionListComponnet{
    @Input() sessions:ISession[];
    
}