export interface ICheckRegisteredEvent {
   eventid: number
   registration_status: string
}