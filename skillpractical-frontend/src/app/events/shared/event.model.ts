export interface IEvent {
  eventid: number
  name: string
  description:string
  subject: string
  genre: string
  date: Date
  time: string
  price: string
  signedUrl1: string

  mode:string
  type:string
  level:string

  costcertificate:string
  costkit:string
  costmaterial:string
 
  tutor:string
  tutorprofile:string
  
  address: string
  city: string
  country: string

  onlineUrl: string
  collegecode:string
  collegename:string
  
}

export interface ISession {
  session_id: number
  session_day: number
  session_abstract: string
  session_date:string
}