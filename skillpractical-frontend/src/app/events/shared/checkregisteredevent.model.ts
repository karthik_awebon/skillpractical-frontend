export class CheckRegisteredEvent{
    constructor(
        public eventid:number,
        public userid:string
    ){}
}