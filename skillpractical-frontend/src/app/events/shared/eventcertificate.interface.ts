export interface IEventCertificate {
    name: string
    eventid: number
    subject:string
    type:string
    userid: string
    username:string
    signedUrl1:string
    date:Date
  }