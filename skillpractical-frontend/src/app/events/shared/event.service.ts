import { Injectable, EventEmitter} from '@angular/core'
import { Subject, Observable} from 'rxjs/RX'
import { IEvent, ISession} from './event.model'
import { Router } from '@angular/router'
import { Http, Response, Headers, RequestOptions } from '@angular/http'

import { EventIdModel, RequestNewEventModel } from './index'

import { RegisterEvent, ISponsors,UserIdModel } from '../index'

import { UserNameModel } from '../shared/username.model';

import { CheckRegisteredEvent} from './checkregisteredevent.model';

import { ICheckRegisteredEvent } from './checkregisteredevent.interface';

import { UpcomingEventsModel } from '../shared/upcomingevents.model';

import { ApiSettings } from '../../ApiSettings';

import {CollegeCodeModel} from './index';

import {IEventCertificate} from '../shared/eventcertificate.interface'
import { UnRegisterEvent } from 'app/events/event-details/unregister-event.model';

@Injectable()
export class EventService{

    constructor(private http:Http){
      
    }

    private _eventsUrl = ApiSettings.API_ENDPOINT +  '/rest/events/reqEventsList.on';

    getEvents(collegeCodeModel:CollegeCodeModel): Observable<IEvent[]> {

        let body = JSON.stringify(collegeCodeModel);

        console.log("collegecodemodel"+body);

        let headers = new Headers({'content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
        let options = new RequestOptions({headers: headers});
        return this.http.post(this._eventsUrl,body,options)
            .map((response: Response) => <IEvent[]> response.json().events_list)
            .do(data => console.log('All Events: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    private url = ApiSettings.API_ENDPOINT + '/rest/events/reqEventInfoByEventId.on';

    getEvent(eventIdModel:EventIdModel): Observable<any> {
        console.log("model.."+JSON.stringify(eventIdModel));
         let body = JSON.stringify(eventIdModel);
            let headers = new Headers({'content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this.url,body,options)
            .map((response: Response) => <IEvent[]> response.json().event_info)
            .do(data => console.log('All Event by ID: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    private url1 = ApiSettings.API_ENDPOINT + '/rest/events/reqEventSessionsInfoByEventId.on';

    getSessionsByEventId(eventIdModel:EventIdModel): Observable<any> {
        console.log("model.."+JSON.stringify(eventIdModel));
         let body = JSON.stringify(eventIdModel);
            let headers = new Headers({'content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this.url1,body,options)
            .map((response: Response) => <ISession[]> response.json().sessions_info)
            .do(data => console.log('All sessions by Event Id: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    private extractData(res: Response) {
        let body = res.json();
        return body.fields || { };
    }

    private registereventurl = ApiSettings.API_ENDPOINT + '/rest/events/reqRegisterEventInfo.on';
     postRegisterEventForm(registerEvent: RegisterEvent) : Observable<any> {
        let body = JSON.stringify(registerEvent);
        let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.registereventurl, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
    }

    private urlgeteventsbyusername = ApiSettings.API_ENDPOINT + '/rest/events/reqEventsByUserName.on';

    getEventsByUserId(UserIdModel:UserIdModel): Observable<any> {
        console.log("model username for registered events.."+JSON.stringify(UserIdModel));
         let body = JSON.stringify(UserIdModel);
            let headers = new Headers({'content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this.urlgeteventsbyusername,body,options)
            .map((response: Response) => <IEvent[]> response.json().events_info_by_username)
            .do(data => console.log('All events by Username: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    private urlcheckregisteredeventwithuserid = ApiSettings.API_ENDPOINT + '/rest/events/checkregisteredeventbyuserid.on';

    checkregisteredeventwithuserid(CheckRegisteredEvent:CheckRegisteredEvent): Observable<any> {
        console.log("model check registered event from event service.."+JSON.stringify(CheckRegisteredEvent));
         let body = JSON.stringify(CheckRegisteredEvent);
            let headers = new Headers({'content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this.urlcheckregisteredeventwithuserid,body,options)
            .map((response: Response) => <ICheckRegisteredEvent> response.json().checkregisteredeventbyuserid_info)
            .do(data => console.log('Check registered events: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


     private urlcheckregistrationstatuswithuserid = ApiSettings.API_ENDPOINT + '/rest/events/checkregistrationstatuseventbyuserid.on';

    checkregistrationstatuswithuserid(CheckRegisteredEvent:CheckRegisteredEvent): Observable<any> {
        console.log("model check registrations status from event service.."+JSON.stringify(CheckRegisteredEvent));
         let body = JSON.stringify(CheckRegisteredEvent);
            let headers = new Headers({'content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this.urlcheckregistrationstatuswithuserid,body,options)
            .map((response: Response) => <ICheckRegisteredEvent> response.json().checkregistrationstatuseventbyuserid_info)
            .do(data => console.log('Check registration status: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    private urlcheckregisteredeventwithuseridandunregister = ApiSettings.API_ENDPOINT + '/rest/events/reqUnRegisterEventInfo.on';

    checkregisteredeventwithuseridandunregister(UnRegisterEvent:UnRegisterEvent): Observable<any> {
        console.log("model check registered event from event service and unregister .."+JSON.stringify(UnRegisterEvent));
         let body = JSON.stringify(UnRegisterEvent);
        let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.urlcheckregisteredeventwithuseridandunregister, body, options)
                        .map(this.extractData)
                        .catch(this.handleError); 
    }

    private urlforgettingupcomingevents = ApiSettings.API_ENDPOINT + '/rest/events/reqUpcomingEventByGenreAndCollegeCode.on';

    getUpcomingEventsByGenre(UpcomingEventsModel:UpcomingEventsModel): Observable<any> {
        console.log("model upcoming events from events service.."+JSON.stringify(UpcomingEventsModel));
         let body = JSON.stringify(UpcomingEventsModel);
            let headers = new Headers({'content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this.urlforgettingupcomingevents,body,options)
            .map((response: Response) => <IEvent[]> response.json().upcomingevents_info)
            .do(data => console.log('All upcoming events by genre: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    private urlforgettingeventsponsors = ApiSettings.API_ENDPOINT + '/rest/events/reqEventSponsorsByEventid.on';

    getEventSponsorByEventid(EventIdModel:EventIdModel): Observable<any> {
        console.log("event id model for event sponsors from events service.."+JSON.stringify(EventIdModel));
         let body = JSON.stringify(EventIdModel);
            let headers = new Headers({'content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this.urlforgettingeventsponsors,body,options)
            .map((response: Response) => <ISponsors[]> response.json().eventsponsorsbyeventid_info)
            .do(data => console.log('All events sponsors by eventid: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    //                    request new event

    private neweventrequesturl = ApiSettings.API_ENDPOINT + '/rest/events/reqNewEventInfo.on';
     postNewEventQueryForm(RequestNewEventModel: RequestNewEventModel) : Observable<any> {
         console.log("new event request"+ RequestNewEventModel);
        let body = JSON.stringify(RequestNewEventModel);
        let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.neweventrequesturl, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
    }

    private urlforgettingattendedevents = ApiSettings.API_ENDPOINT + '/rest/events/reqselectAttendedEventsByUserid.on';

    getAttendedEventsByUserid(UserNameModel:UserNameModel): Observable<any> {
        console.log("user id model for attended events from events service.."+JSON.stringify(UserNameModel));
         let body = JSON.stringify(UserNameModel);
            let headers = new Headers({'content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this.http.post(this.urlforgettingattendedevents,body,options)
            .map((response: Response) => <IEvent[]> response.json().AttendedEvents_info)
            .do(data => console.log('All attended events by user name: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    private urlgeteventscertificatebyusername = ApiSettings.API_ENDPOINT + '/rest/events/reqselectEventsCertificateByUserid.on';
    
    getEventsCertificatesByUserId(UserIdModel:UserIdModel): Observable<any> {
            console.log("model userid for events certificates.."+JSON.stringify(UserIdModel));
             let body = JSON.stringify(UserIdModel);
                let headers = new Headers({'content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
                let options = new RequestOptions({headers: headers});
            return this.http.post(this.urlgeteventscertificatebyusername,body,options)
                .map((response: Response) => <IEventCertificate[]> response.json().EventsCertificate_info)
                .do(data => console.log('All events cetificates by userid: ' +  JSON.stringify(data)))
                .catch(this.handleError);      
        }

}