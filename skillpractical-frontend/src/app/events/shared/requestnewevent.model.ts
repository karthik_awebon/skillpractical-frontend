export class RequestNewEventModel{
    constructor(
        public userid:string,
        public username:string,
        public eventtitle:string,
        public typeofrequest:string,
        public natureofevent:string,
        public eventdescription:string,
        public collegecode:string

    ){}
}