export interface ISponsors {
  name: string
  eventid: number
  sponsor_name: string
  sponsor_websiteurl:string
  sponsor_imageurl:string
}