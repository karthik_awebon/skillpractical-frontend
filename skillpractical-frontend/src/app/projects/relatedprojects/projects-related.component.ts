import { Component, Input } from '@angular/core'
import { IProject } from '../shared/index'

import { ProjectService } from '../shared/index'
import { ProjectSubjectModel, SaveProjectModel, CheckSavedProjectModel, ICheckSavedProject } from '../index'
import { Router } from '@angular/router';

@Component({
  selector: 'projects-related',
  templateUrl:'projects-related.component.html', 
  styleUrls: ['projects-related.component.css']
})

export class ProjectsRelatedComponent {

   @Input() project:IProject

   saveprojectmodel = new SaveProjectModel('','','','','','','');
   
      checksavedprojectmodel = new CheckSavedProjectModel('','');
      
      checksavedproject: ICheckSavedProject;
      
      checksavedprojectid: string;
   
      userid:string;
      currentusername:string;


   constructor(private router: Router,private projectService: ProjectService) {

          console.log("project from related projects"+this.project);
          
        this.router.routeReuseStrategy.shouldReuseRoute = function() {
          return false;
      };
  }


  ngOnInit(){
    this.currentusername =  localStorage.getItem("currentUserName");
    this.userid =  localStorage.getItem("userid");
    this.checkprojectsavedmethod();
 }


 checkprojectsavedmethod(){
  
 // check for registered events
        this.checksavedprojectmodel = new CheckSavedProjectModel(this.project.project_id,this.userid);
        this.projectService.checksavedprojectwithuserid(this.checksavedprojectmodel)                           
                            .subscribe(
                                (data) => {
                                   this.checksavedproject = data;
                                   this.checksavedprojectid = this.checksavedproject.project_id;
                              
                                },
                                err => console.log('error: ', err)
                            );
              console.log("check saved project: "+this.checkprojectsavedmethod);
 }


 saveproject(project){
      console.log("clicked saved project"+JSON.stringify(project));

       this.saveprojectmodel = new SaveProjectModel(project.project_id,project.project_title,this.currentusername,
      this.userid,project.project_subject_area,project.project_type,project.project_image_url);
            this.projectService.postSaveProject(this.saveprojectmodel)                         
                                .subscribe(
                                  data => {
                                    console.log('success: ', data)

                                    this.ngOnInit();

                                  },
                                  err => console.log('error: ', err)
                                );
                                console.log("save project model"+JSON.stringify(this.saveprojectmodel));
    
  }
  
   unsaveproject(project){  
          
        this.checksavedprojectmodel = new CheckSavedProjectModel(project.project_id,this.userid);
            this.projectService.checksavedprojectwithuseridandunregister(this.checksavedprojectmodel)
                                       
                                .subscribe(
                                  data => {
                                    console.log('success: ', data)

                                    this.ngOnInit();

                                  },
                                  err => console.log('error: ', err)
                                );
          console.log("unregister saved project model"+JSON.stringify(this.checksavedprojectmodel));

  }


 
  

}