export interface IProjectBlogsRelated {
  blogid:string
  project_id: string
  blog_link:string
  blog_link_name:string
}