export class SaveProjectModel{
    constructor(
        public project_id:string,
        public project_title:string,
        public username:string,
        public userid:string,
        // public userid:string
        public project_subject_area:string,
        public project_type:string,
        public project_image_url:string
    ){}
}