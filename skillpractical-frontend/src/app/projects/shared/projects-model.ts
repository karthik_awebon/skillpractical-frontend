export interface IProject {
  project_id: string
  project_branch: string
  project_type:string
  project_subject_area: string
  project_title: string
  project_branchimage_url:string;
  project_image_url: string
  project_abstract: string
  project_description: string
  project_technologies_used:string
  project_languages_used:string
  project_tools_used:string
  project_reference_paper_url:string
  project_procedure_url:string
  project_code_url:string
  project_future_scope:string
  project_sub_subject_area:string
}
