import { Injectable,OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { ICheckSavedProject, CheckSavedProjectModel,IProject,ProjectIdModel, ProjectSubjectModel,IProjectReferencePaperRelated,IProjectBlogsRelated } from './index';
import { ExistingProjectQueryFormModel,NewProjectQueryFormModel,SaveProjectModel } from '../index';

import { ApiSettings } from '../../ApiSettings';
import { UserIdModel } from 'app/projects/shared/userid.model';
import { TagBranchModel } from 'app/projects/shared/tagbranch.model';
import { ITagProjects } from 'app/projects/shared/projectstags.interface';
import { ProjectTagModel } from 'app/projects/shared/projecttag.model';
import { ProjectBranchModel } from 'app/projects/shared/projectbranch.model';
import { ProjectTypeAndTagModel } from 'app/projects/shared/projectstagandtype.model';

@Injectable()
export class ProjectService {
    

    constructor(private _http: Http) {}

    private extractData(res: Response) {
        let body = res.json();
        return body.fields || { };
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }


    private _projectUrl = ApiSettings.API_ENDPOINT+'/rest/projects/reqProjectsList.on';

    getProjects(): Observable<IProject[]> {
        let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
        let options = new RequestOptions({headers: headers});
        return this._http.get(this._projectUrl,options)
            .map((response: Response) => <IProject[]> response.json().Projects_list)
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    
    private _Allprojecttags = ApiSettings.API_ENDPOINT+'/rest/projects/selectAllProjectsTags.on';

    getAllTags(): Observable<ITagProjects[]> {
        let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
        let options = new RequestOptions({headers: headers});
        return this._http.get(this._Allprojecttags,options)
            .map((response: Response) => <ITagProjects[]> response.json().AllProjectsTags_list)
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    private url = ApiSettings.API_ENDPOINT + '/rest/projects/reqProjectsById.on';

    getProjectById(projectIdModel:ProjectIdModel): Observable<any> {
        console.log("model.."+JSON.stringify(projectIdModel));
         let body = JSON.stringify(projectIdModel);
            let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this._http.post(this.url,body,options)
            .map((response: Response) => <IProject[]> response.json().projects_info)
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    private urltagsbybranch = ApiSettings.API_ENDPOINT + '/rest/projects/reqProjectsTagsByBranch.on';
    getTagsByBranch(TagBranchModel:TagBranchModel): Observable<any> {
        console.log("project branch model.."+JSON.stringify(TagBranchModel));
         let body = JSON.stringify(TagBranchModel);
            let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this._http.post(this.urltagsbybranch,body,options)
            .map((response: Response) => <ITagProjects[]> response.json().ProjectsTagsByBranch_info)
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    private urlprojectsbytype = ApiSettings.API_ENDPOINT + '/rest/projects/reqProjectsByType.on';
    getProjectsByType(ProjectTypeAndTagModel:ProjectTypeAndTagModel): Observable<any> {
        console.log("project type and tag model .."+JSON.stringify(ProjectTypeAndTagModel));
         let body = JSON.stringify(ProjectTypeAndTagModel);
            let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this._http.post(this.urlprojectsbytype,body,options)
            .map((response: Response) => <IProject[]> response.json().ProjectsByType_info)
            .do(data => console.log('project by type: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }

    private urlprojectsbytag = ApiSettings.API_ENDPOINT + '/rest/projects/reqProjectsByTag.on';
    getProjectsByTag(ProjectTagModel:ProjectTagModel): Observable<any> {
        console.log("project tag model .."+JSON.stringify(ProjectTagModel));
         let body = JSON.stringify(ProjectTagModel);
            let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this._http.post(this.urlprojectsbytag,body,options)
            .map((response: Response) => <IProject[]> response.json().ProjectsByTags_info)
            .do(data => console.log('project by tag: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    private urlprojectsbybranch = ApiSettings.API_ENDPOINT + '/rest/projects/reqProjectsByBranch.on';
    getProjectsByBranch(ProjectBranchModel:ProjectBranchModel): Observable<any> {
        console.log("project tag model .."+JSON.stringify(ProjectBranchModel));
         let body = JSON.stringify(ProjectBranchModel);
            let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this._http.post(this.urlprojectsbybranch,body,options)
            .map((response: Response) => <IProject[]> response.json().ProjectsByBranch_info)
            .do(data => console.log('project by branch ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }




    // private url1 = ApiSettings.API_ENDPOINT +  '/rest/projects/reqRelatedProjectsByName.on';
    // getRelatedProjectBySubject(projectSubjectModel:ProjectSubjectModel): Observable<any> {
    //         console.log("model.."+JSON.stringify(projectSubjectModel));
    //         let body = JSON.stringify(projectSubjectModel);
    //             let headers = new Headers({'content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
    //             let options = new RequestOptions({headers: headers});
    //         return this._http.post(this.url1,body,options)
    //             .map((response: Response) => <IProject[]> response.json().Relatedprojects_info)
    //             .do(data => console.log('All Related Projects: ' +  JSON.stringify(data)))
    //             .catch(this.handleError);      
    //     }


        private url1 = ApiSettings.API_ENDPOINT +  '/rest/projects/getRelatedProjectsByProjectId.on';
        getRelatedProjectByProjectId(ProjectIdModel:ProjectIdModel): Observable<any> {
                console.log("projet id model.."+JSON.stringify(ProjectIdModel));
                let body = JSON.stringify(ProjectIdModel);
                    let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
                    let options = new RequestOptions({headers: headers});
                return this._http.post(this.url1,body,options)
                    .map((response: Response) => <IProject[]> response.json().Relatedprojects_info)
                    .do(data => console.log('All Related Projects: ' +  JSON.stringify(data)))
                    .catch(this.handleError);      
            }
    

    private postexistingprojectqueryformurl = ApiSettings.API_ENDPOINT + '/rest/projects/reqProjectQueryFormInfo.on';
     postExistingProjectQueryForm(existingProjectQueryFormModel: ExistingProjectQueryFormModel) : Observable<any> {
         console.log("query form model"+JSON.stringify(existingProjectQueryFormModel));
        let body = JSON.stringify(existingProjectQueryFormModel);
        let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this.postexistingprojectqueryformurl, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);   
    }

    private postnewprojectqueryformurl = ApiSettings.API_ENDPOINT + '/rest/projects/reqnewprojectformbyuserInfo.on';
     postNewProjectQueryForm(NewProjectQueryFormModel: NewProjectQueryFormModel) : Observable<any> {
        console.log("new project query form model"+JSON.stringify(NewProjectQueryFormModel));
        let body = JSON.stringify(NewProjectQueryFormModel);
        let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this.postnewprojectqueryformurl, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
    }

                                        // realted blogs
   
    private urlrelatedblogs = ApiSettings.API_ENDPOINT + '/rest/projects/reqgetRelatedBlogsByProjectId.on';
    getRelatedBlogsByProjectId(ProjectIdModel:ProjectIdModel): Observable<any> {
            console.log("model.."+JSON.stringify(ProjectIdModel));
            let body = JSON.stringify(ProjectIdModel);
                let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
                let options = new RequestOptions({headers: headers});
            return this._http.post(this.urlrelatedblogs,body,options)
                .map((response: Response) => <IProjectBlogsRelated[]> response.json().relateblogsbyprojectid_info)
                .do(data => console.log('All Related Projects: ' +  JSON.stringify(data)))
                .catch(this.handleError);      
        }


                                        // related reference paper

    private urlrelatedreferencepaper = ApiSettings.API_ENDPOINT + '/rest/projects/reqgetRelatedReferencePaperByProjectId.on';
    getRelatedReferencePaperByProjectId(ProjectIdModel:ProjectIdModel): Observable<any> {
            console.log("model.."+JSON.stringify(ProjectIdModel));
            let body = JSON.stringify(ProjectIdModel);
                let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
                let options = new RequestOptions({headers: headers});
            return this._http.post(this.urlrelatedreferencepaper,body,options)
                .map((response: Response) => <IProjectReferencePaperRelated[]> response.json().relatedreferencepaperbyprojectid_info)
                .do(data => console.log('All Related Projects: ' +  JSON.stringify(data)))
                .catch(this.handleError);      
        }


    private saveprojecturl = ApiSettings.API_ENDPOINT + '/rest/projects/reqsaveprojectbyuserInfo.on';
     postSaveProject(SaveProjectModel: SaveProjectModel) : Observable<any> {
        console.log("save project model"+JSON.stringify(SaveProjectModel));
        let body = JSON.stringify(SaveProjectModel);
        let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this.saveprojecturl, body, options)
                        .map(this.extractData)
                        .catch(this.handleError);
    }


    //   private _projectStarredUrl = ApiSettings.API_ENDPOINT + '/rest/projects/reqStarredProjectsList.on';

    //   getProjectsWhereStarred(): Observable<IProject[]> {
    //     let headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+localStorage.getItem("token")});
    //     let options = new RequestOptions({headers: headers});
    //     return this._http.get(this._projectStarredUrl,options)
    //         .map((response: Response) => <IProject[]> response.json().Starred_Projects_list)
    //         .do(data => console.log('All: ' +  JSON.stringify(data)))
    //         .catch(this.handleError);
    // }


    private _projectStarredUrl = ApiSettings.API_ENDPOINT + '/rest/projects/reqStarredProjectsList.on';
    getProjectsWhereStarred(UserIdModel:UserIdModel): Observable<any> {
            console.log("user id model.."+JSON.stringify(UserIdModel));
            let body = JSON.stringify(UserIdModel);
                let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
                let options = new RequestOptions({headers: headers});
            return this._http.post(this._projectStarredUrl,body,options)
                .map((response: Response) => <IProject[]> response.json().Starred_Projects_list)
                .do(data => console.log('All starred project by user: ' +  JSON.stringify(data)))
                .catch(this.handleError);      
        }


    
    private urlchecksavedprojectwithuserid = ApiSettings.API_ENDPOINT + '/rest/projects/checksavedprojectbyuserid.on';

    checksavedprojectwithuserid(CheckSavedProjectModel:CheckSavedProjectModel): Observable<any> {
        console.log("model check saved project from project service.."+JSON.stringify(CheckSavedProjectModel));
         let body = JSON.stringify(CheckSavedProjectModel);
            let headers = new Headers({'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token")});
            let options = new RequestOptions({headers: headers});
        return this._http.post(this.urlchecksavedprojectwithuserid,body,options)
            .map((response: Response) => <ICheckSavedProject> response.json().checksavedprojectbyuserid_info)
            .do(data => console.log('Check saved project: ' +  JSON.stringify(data)))
            .catch(this.handleError);      
    }


    private urlchecksavedprojectwithuseridandunregister = ApiSettings.API_ENDPOINT + '/rest/projects/reqUnSaveProjectInfo.on';

    checksavedprojectwithuseridandunregister(CheckSavedProjectModel:CheckSavedProjectModel): Observable<any> {
        console.log("model check saved project from project service and unsave .."+JSON.stringify(CheckSavedProjectModel));
         let body = JSON.stringify(CheckSavedProjectModel);
        let headers = new Headers({ 'Content-Type': 'application/json','Authorization': 'Bearer '+localStorage.getItem("token") });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this.urlchecksavedprojectwithuseridandunregister, body, options)
                        .map(this.extractData)
                        .catch(this.handleError); 
    }
                                                // update views

    private updateViewsCount = ApiSettings.API_ENDPOINT +'/rest/projects/reqUpdateViewsInfo.on';
   
    UpdateViewsCount(ProjectIdModel: ProjectIdModel) : Observable<any> {
            console.log("post comment model"+JSON.stringify(ProjectIdModel));
          let body = JSON.stringify(ProjectIdModel);
          let headers = new Headers({ 'Content-Type': 'application/json' , 'Authorization': 'Bearer '+localStorage.getItem("token") });
          let options = new RequestOptions({ headers: headers });
  
          return this._http.post(this.updateViewsCount, body, options)
                          .map(this.extractData)
                          .catch(this.handleError);
     }

}
