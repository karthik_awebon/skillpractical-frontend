export interface IProjectReferencePaperRelated {
  referencepaperid:string
  project_id: string
  referencepaper_url:string
  referencepaper_url_name:string
}