import { PipeTransform, Pipe } from '@angular/core'

import { IProject } from './shared/index'

@Pipe({
    name: 'projectsFilter'
})
export class ProjectFilterPipe implements PipeTransform {

         transform(value: IProject[], filterBy: string): IProject[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        
        return filterBy ? value.filter((project: IProject) =>
            project.project_title.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }

    
}