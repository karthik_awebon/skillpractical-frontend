import { Component, Input,OnChanges } from '@angular/core'
import { ProjectService } from './shared/index'
import { ActivatedRoute } from '@angular/router'
import { IProject } from './shared/index'
import { UserIdModel } from 'app/projects/shared/userid.model';
import { ITagProjects } from 'app/projects/shared/projectstags.interface';
import { TagBranchModel } from 'app/projects/shared/tagbranch.model';
import { ProjectTagModel } from 'app/projects/shared/projecttag.model';
import { ProjectBranchModel } from 'app/projects/shared/projectbranch.model';
import { ProjectTypeAndTagModel } from 'app/projects/shared/projectstagandtype.model';

@Component({
   selector: 'projects-list',
   templateUrl:'projects-list.component.html',
   styleUrls: ['projects-list.component.css']
})
export class ProjectListComponent {

public linkToShare:any;

 searchTerm:string = "";
 foundProjects: IProject[];

 public listFilter:any;

 errorMessage:string;

 projects:IProject[]
title:string;

useridmodel = new UserIdModel('');

userid:string;

tags:ITagProjects[];

selectedTag:string = 'All';

modeltagbranch = new TagBranchModel('');

modelprojecttag = new ProjectTagModel('');

modelprojectbranch = new ProjectBranchModel('');

modelprojecttypeandtag = new ProjectTypeAndTagModel('','');

starredclickstyle:any;

 constructor(private projectService: ProjectService, private route:ActivatedRoute){
    
 }

 ngOnInit(){
   
   this.filterByall();

   this.starredclickstyle = {
    'pointer-events': 'none'
    }

 }

 clearall(){
     this.filterBy = 'All'
     this.selectedTag = 'All';
     this.filterByType='';
     this.filterByStarred = '';
     this.ngOnInit();
 }

   filterBy: string = 'All';

   filterByType: string;

   filterByStarred:string;

   visibleProjects: IProject[] = [];


  tagsByBranch(){
    this.modeltagbranch.tag_branch = this.title;   
    this.projectService.getTagsByBranch(this.modeltagbranch)
            .subscribe(
                (data) => {
                this.tags = data;
                console.log("tags by branch....."+JSON.stringify(this.tags))                  
                },
            error => this.errorMessage = <any>error)  
  }

  Alltags(){
     
    this.projectService.getAllTags()
            .subscribe(
                (data) => {
                this.tags = data;
                console.log(" All tags....."+JSON.stringify(this.tags))                  
                },
            error => this.errorMessage = <any>error)  
  }

  AllBranchesProjects(){
    this.projectService.getProjects()
    .subscribe(
        (data) => {
        this.visibleProjects = data;
        console.log(" All projects....."+JSON.stringify(this.tags))                  
        },
    error => this.errorMessage = <any>error)  
  }

  isSelected(tag): boolean {
    if(!this.selectedTag) {
        return false;
    }
    return this.selectedTag ===  tag ? true : false;
  }

  public getPointerShowOrNot(){
      var none;
      return none;
  }

  filterBytag(tag){

         this.filterByStarred = '';

        this.modelprojecttag.tag = tag;

        this.selectedTag = tag;

        this.filterByType = '';

        this.starredclickstyle = {
            
        }

        this.projectService.getProjectsByTag(this.modelprojecttag)
            .subscribe(
                (data) => {
                this.visibleProjects = data;
                console.log("project by tag....."+JSON.stringify(this.visibleProjects));                  
                },
            error => this.errorMessage = <any>error)  

    }

    filterByBranch(){

        this.selectedTag = 'All';
        this.filterByType = '';
        this.filterByStarred = '';

        this.modelprojectbranch.project_branch = this.title
        this.projectService.getProjectsByBranch(this.modelprojectbranch)
            .subscribe(
                (data) => {
                this.visibleProjects = data;
                console.log("project by tag....."+JSON.stringify(this.visibleProjects));                  
                },
            error => this.errorMessage = <any>error) 

    }


    filterByall(){
        this.title = 'All';

        this.filterBy = 'All';
        
        this.Alltags();
        this.AllBranchesProjects();

    }

    filterByelectrical(){
        this.title = 'Electrical';


        this.filterBy = 'electrical';

        this.tagsByBranch();

        this.filterByBranch();
       
    }

    filterByelectronics(){
        this.title = 'Electronics';

         this.filterBy = 'electronics';

         this.tagsByBranch();

         this.filterByBranch();
         
    }

   filterBycomputerscience(){
        this.title = 'Computer Science';

       this.filterBy = 'computerscience';

       this.tagsByBranch();

       this.filterByBranch();
      
   }

  filterBymechanical(){
      this.title = 'Mechanical';

       this.filterBy = 'mechanical';
      
       this.tagsByBranch();

       this.filterByBranch();
   }

    filterBycivil(){
        this.title = 'CIVIL';

       this.filterBy = 'civil';
       
       this.tagsByBranch();

       this.filterByBranch();
       
   }

   
    //beginner
    filterBybeginner(){
        this.filterByType = 'Beginner';
        this.getprojectsbytype();
       
   }

   //intermediate
    filterBymidlevel(){
        this.filterByType = 'Intermediate';
        this.getprojectsbytype();
   }

   //advanced
    filterByadvanced(){
        this.filterByType = 'Advanced';
        this.getprojectsbytype();
    }


    getprojectsbytype(){

        this.modelprojecttypeandtag.tag = this.selectedTag;
        this.modelprojecttypeandtag.project_type = this.filterByType;

        this.projectService.getProjectsByType(this.modelprojecttypeandtag)
        .subscribe(
            (data) => {
            this.visibleProjects = data;
            console.log("project by type....."+JSON.stringify(this.visibleProjects));                  
            },
        error => this.errorMessage = <any>error) 

    }

    //starred projects

      filterBystarred(){
        this.starredclickstyle = {
            'pointer-events': 'none'
        }

        //to clear the selected tags
        this.filterBy = 'All';
        this.selectedTag = 'Saved';
        this.filterByType = ''
        //to clear the selected tags


        this.filterByStarred = 'starred';

        this.userid = localStorage.getItem("userid");

        this.useridmodel.userid = this.userid;

        this.projectService.getProjectsWhereStarred(this.useridmodel)
                    .subscribe(
                        (data) => {
                          this.visibleProjects = data;
                          console.log("projects starred....."+JSON.stringify(this.projects))                  
                          },
                      error => this.errorMessage = <any>error)   

        console.log("starred button clicked.")
    }

}