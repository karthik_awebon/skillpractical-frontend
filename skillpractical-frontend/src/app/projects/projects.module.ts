import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'


import { projectsRoutes } from './projects.routes'

import {ShareButtonsModule} from 'ngx-sharebuttons';


import { ProjectThumbnailComponent,
         ProjectListComponent,
         ProjectDetailsComponent,ProjectService,
         ProjectFilterPipe, ProjectsRelatedComponent, QueryFormComponent, NewProjectQueryFormComponent
        } from './index'
        

import { PdfViewerComponent } from 'ng2-pdf-viewer';

import { ToastModule } from 'ng2-toastr/ng2-toastr';

import {NgxPaginationModule} from 'ngx-pagination';
import { CapitalizePipe } from 'app/projects/capitalize.pipe';


@NgModule({
    imports:[
        
        CommonModule,
        FormsModule, ReactiveFormsModule,
        RouterModule.forChild(projectsRoutes),
        ShareButtonsModule.forRoot(),
        ToastModule.forRoot(),
        NgxPaginationModule

    ],
    declarations:[
        CapitalizePipe,
       ProjectThumbnailComponent,ProjectListComponent,ProjectDetailsComponent,
       ProjectFilterPipe,PdfViewerComponent, ProjectsRelatedComponent, QueryFormComponent,NewProjectQueryFormComponent
    ],
    providers:[
        ProjectService

    ]

})
export class ProjectsModule { 

 }
 