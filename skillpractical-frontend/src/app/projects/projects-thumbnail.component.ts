import { Component, Input } from '@angular/core'
import { IProject } from './shared/index'

import {UserService} from './../community/user.service';

import { UserNameModel } from './../community/username.model';

import { ProjectService,SaveProjectModel } from './shared/index'

import { IUser } from './../community/interfaces/userInterface';

import {ICheckSavedProject,CheckSavedProjectModel,} from './shared/index'

import { ToastrService } from 'toastr-ng2';

@Component({
  selector: 'project-thumbnail',
   templateUrl: './projects-thumbnail.component.html',
   styleUrls: ['projects-thumbnail.component.css']
})
export class ProjectThumbnailComponent {
   @Input() project:IProject

   saveprojectmodel = new SaveProjectModel('','','','','','','');

   checksavedprojectmodel = new CheckSavedProjectModel('','');
   
   checksavedproject: ICheckSavedProject;
   
   checksavedprojectid: string;

   userid:string;
   currentusername:string;

   constructor(private toastrService: ToastrService,private projectService: ProjectService){
    
   }


   ngOnInit(){
      this.currentusername =  localStorage.getItem("currentUserName");
      this.userid =  localStorage.getItem("userid");
      this.checkprojectsavedmethod();
   }


   checkprojectsavedmethod(){
    
   // check for registered events
          this.checksavedprojectmodel = new CheckSavedProjectModel(this.project.project_id,this.userid);
          this.projectService.checksavedprojectwithuserid(this.checksavedprojectmodel)                           
                              .subscribe(
                                  (data) => {
                                     this.checksavedproject = data;
                                     this.checksavedprojectid = this.checksavedproject.project_id;                          
                                  },
                                  err => console.log('error: ', err)
                              );
                console.log("check saved project: "+this.checkprojectsavedmethod);
   }


   saveproject(project){
    
        console.log("clicked saved project"+JSON.stringify(project));

         this.saveprojectmodel = new SaveProjectModel(project.project_id,project.project_title,this.currentusername,
        this.userid,project.project_subject_area,project.project_type,project.project_image_url);
              this.projectService.postSaveProject(this.saveprojectmodel)                         
                                  .subscribe(
                                    data => {
                                      console.log('success: ', data)

                                      this.ngOnInit();
                                      this.toastrService.success('Project Saved Successfully!');

                                    },
                                    err => console.log('error: ', err)
                                  );
                                  console.log("save project model"+JSON.stringify(this.saveprojectmodel));
      
    }
    
     unsaveproject(project){  

          this.checksavedprojectmodel = new CheckSavedProjectModel(project.project_id,this.userid);
              this.projectService.checksavedprojectwithuseridandunregister(this.checksavedprojectmodel)
                                         
                                  .subscribe(
                                    data => {
                                      console.log('success: ', data)

                                      this.ngOnInit();
                                      this.toastrService.success('Project UnSaved Successfully!');

                                    },
                                    err => console.log('error: ', err)
                                  );
            console.log("unregister saved project model"+JSON.stringify(this.checksavedprojectmodel));

    }

}