export class ExistingProjectQueryFormModel{
    constructor(
         public userid:string,
         public username:string,
        //  public intention: string,
         public title: string,
         public description: string,
         public project_id:string,
         public project_branch:string
    ){}
        
}