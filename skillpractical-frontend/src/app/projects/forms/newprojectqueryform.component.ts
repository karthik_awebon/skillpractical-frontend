import { Component, Input, ViewChild, ElementRef } from '@angular/core';
import { NewProjectQueryFormModel } from './index';
import { NgForm, FormGroup } from '@angular/forms';
import { ProjectService,UserNameModel} from '../shared/index'
import {UserService} from '../../community/user.service';
import { IUser } from '../../community/interfaces/userInterface';

import { ToastrService, ToastrConfig } from 'toastr-ng2';
import { TagBranchModel } from 'app/projects/shared/tagbranch.model';
import { ITagProjects } from 'app/projects/shared/projectstags.interface';

@Component({
  selector: 'new-project-query-form',
  templateUrl:'newprojectqueryform.component.html', 
  styleUrls: ['newprojectqueryform.component.css']
  
})
export class NewProjectQueryFormComponent {
  @ViewChild('closeBtn') closeBtn: ElementRef;

    public specialization:any;
  
     hasWhoswithError = false;
     hasBranchError = false;
     hasSpecilizationError = false;
  
     branches = ['Electrical','Electronics','Computer Science','Civil','Mechanical','other'];
     
    currentusername:string;
    model = new NewProjectQueryFormModel('','','default','default','','','default');
    modelusername = new UserNameModel('');
    user: IUser;
    userid: string;
    username: string;

    modeltagbybarnch = new TagBranchModel('');

    tags:ITagProjects[];

    errorMessage:string;
    
   constructor(private toastrService: ToastrService,private projectService: ProjectService,
    private userService:UserService,private toastrConfig: ToastrConfig) {
    toastrConfig.timeOut = 2000;
  }


   callType(value){
    console.log("seleted branch"+value);
    // this.order.type=value;

  this.gettagsbybranch(value);
  
  }

  gettagsbybranch(value){

    this.modeltagbybarnch.tag_branch = value;
    this.projectService.getTagsByBranch(this.modeltagbybarnch)
    .subscribe(
       (data) => {
          this.tags = data;
          
           console.log("tags....."+JSON.stringify(this.tags))                             
             },
          error => this.errorMessage = <any>error);

  }

    ngOnInit() {
      
      //this.modalForm.reset();
            this.currentusername =  localStorage.getItem("currentUserName");
            this.modelusername = new UserNameModel(this.currentusername);
          
            this.userService.getUserByName(this.modelusername).subscribe(
                  user => {
                    this.user = user;
                      console.log("user from new project queryform component"+JSON.stringify(this.user));
                    
                  },
                  error => console.log(error)
                )
                //call this wherever you want to close modal
                
    }
  
    submitForm(form: NgForm) {
      
                this.model.userid = this.user.userid;
                this.model.username = this.user.username;
                // this.model = new QueryFormModel(this.user.userid,this.user.username,'','','')
                 this.projectService.postNewProjectQueryForm(this.model)
                                  .subscribe(
                                    data => { 
                                     
                                      this.toastrService.success('Request Successful!');
                                      form.reset();
                                      this.closeBtn.nativeElement.click();
                                     
                                    },
                                    err => console.log('error: ', err)
                                    
                                  );
                                 
                                  console.log("new project post form model"+JSON.stringify(this.model));
                                 
              }

            
              validateWhoswith(value) {
                if (value === 'default')
                  this.hasWhoswithError = true;
                else
                  this.hasWhoswithError = false;
              }
              validateBranch(value){
                if (value === 'default')
                this.hasBranchError = true;
                else
                this.hasBranchError = false;
              }
              validateSpecialization(value){
                if (value === 'default')
                this.hasSpecilizationError = true;
                else
                this.hasSpecilizationError = false;
              }
              closemodel(form){
                form.reset();
              }
}