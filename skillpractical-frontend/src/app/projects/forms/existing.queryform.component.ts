import { Component, Input, ViewContainerRef } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import { ExistingProjectQueryFormModel } from './index';

import { NgForm } from '@angular/forms';

import { ProjectService,UserNameModel} from '../shared/index'

import {UserService} from '../../community/user.service';

import { IUser } from '../../community/interfaces/userInterface'

import { IProject } from '../shared/index'

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { ToastrService } from 'toastr-ng2';

@Component({
  selector: 'query-form',
  templateUrl:'existing.queryform.component.html', 
  styleUrls: ['existing.queryform.component.css']
  
})
export class QueryFormComponent {

  @ViewChild('closeBtn') closeBtn: ElementRef;

  

  @Input() project: IProject;

  currentusername:string;
  model = new ExistingProjectQueryFormModel('','','','','','');
  modelusername = new UserNameModel('');

  user: IUser;

  userid: string;
  username: string;

  modal:boolean;

   constructor(private toastrService: ToastrService,private _vcr: ViewContainerRef,private projectService: ProjectService,private userService:UserService) {
            
   }

    ngOnInit() {

            this.currentusername =  localStorage.getItem("currentUserName");

            this.modelusername = new UserNameModel(this.currentusername);
          
            this.userService.getUserByName(this.modelusername).subscribe(
                  user => {
                    this.user = user;

                      console.log("user from queryform component"+JSON.stringify(this.user));
                    
                  },
                  error => console.log(error)
                )
    }
  
    submitForm(form: NgForm) {
                this.model.userid = this.user.userid;
                this.model.username = this.user.username;
                this.model.project_id = this.project.project_id;
                this.model.project_branch = this.project.project_branch;
                // this.model = new QueryFormModel(this.user.userid,this.user.username,'','','')
                 this.projectService.postExistingProjectQueryForm(this.model)
                                  .subscribe(
                                    data => {  
                                      
                                      this.toastrService.success('Request Successful!');
                                      this.closeBtn.nativeElement.click();
                                      
                                    },
                                    err => console.log('error: ', err)
                                  );
                                  console.log("project post form model"+JSON.stringify(this.model));
              }


    
    }
