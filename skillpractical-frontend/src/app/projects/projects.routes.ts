import {
    ProjectListComponent,
    ProjectDetailsComponent,QueryFormComponent
} from './index'


import {LoginGuard}  from '../user/routerguard/loginguard.component'

export const projectsRoutes = [
   {path: 'projectsList', canActivate: [LoginGuard],component: ProjectListComponent}
    ,{path: 'projectsinfo/:id', component: ProjectDetailsComponent},
    {path: 'projectqueryform', component: QueryFormComponent}
    // {path: 'projectsdetail/:id/:subject', component: ProjectDetailsComponent}
    // ,{path: 'relatedprojectinfo/:id2', component: ProjectDetailsComponent}
]