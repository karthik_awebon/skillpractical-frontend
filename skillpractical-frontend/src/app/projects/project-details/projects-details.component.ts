import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import { IProject, ProjectService,SaveProjectModel } from '../shared/index'

import { Subscription } from 'rxjs/Subscription';

import {UserService} from '../../community/user.service';

import { UserNameModel } from '../../community/username.model';

import {ICheckSavedProject,CheckSavedProjectModel, ProjectIdModel, ProjectSubjectModel,IProjectBlogsRelated,IProjectReferencePaperRelated } from '../shared/index'
import {BrowserModule, DomSanitizer,SafeResourceUrl} from '@angular/platform-browser'

import { IUser } from '../../community/interfaces/userInterface';

@Component({
  templateUrl: 'projects-details.component.html',  
  styles: ['./projects-details.component.css'] 
})
export class ProjectDetailsComponent implements OnInit {

 
 public pdfShowreferencepaper:any;
 public pdfShow:any;

pdfSrcprocedure: string ;

// page: number = 1;
pageurlprocedure:SafeResourceUrl;

 pdfSrcreferencepapaer: string;
 pageurlreferencepaper:SafeResourceUrl;

  private sub: Subscription;
  errorMessage:string;
  projectid:string; 
  
//   subject:string;

  project:IProject;
  relatedproject:IProject[];

  relatedblogs:IProjectBlogsRelated[];
  relatedreferencepaper:IProjectReferencePaperRelated[]

  modelProjectId = new ProjectIdModel('');
  modelsubject = new ProjectSubjectModel('');

  project_sub_subject_area:string;

  currentusername:string;

  modelusername = new UserNameModel('');
  
  user: IUser;
  userid:string;

  project_code_url:string;

  constructor(private userService:UserService,private projectService: ProjectService, private route: ActivatedRoute,private domSanitizer:DomSanitizer) {
       
  }
  goBack() {
    window.history.back();
  }
  ngOnInit() {
      this.currentusername =  localStorage.getItem("currentUserName");

      this.sub = this.route.params.subscribe(
                    params => {
                    
                        let id = params['id'];
                        // let subject = params['subject']
                        this.projectid = id;
                        // this.subject = subject;
                        console.log("projectid.."+this.projectid);
                        // console.log("subject area.."+this.subject);
                });   

            this.modelProjectId = new ProjectIdModel(this.projectid);
            this.projectService.getProjectById(this.modelProjectId)
                            .subscribe(
                            (project) => {
                                this.project = project; 
                                this.project_code_url = this.project.project_code_url;
                                this.pdfSrcprocedure = this.project.project_procedure_url;
                                this.project_sub_subject_area = this.project.project_sub_subject_area;
                                 this.pageurlprocedure = this.domSanitizer.bypassSecurityTrustResourceUrl(this.pdfSrcprocedure);  
                                   this.pdfSrcreferencepapaer = this.project.project_reference_paper_url;
                                   this.pageurlreferencepaper = this.domSanitizer.bypassSecurityTrustResourceUrl(this.pdfSrcreferencepapaer); 

                                    

                // this.modelsubject = new ProjectSubjectModel(this.project_sub_subject_area);
                //     this.projectService.getRelatedProjectBySubject(this.modelsubject)
                //                     .subscribe(
                //                     (relatedproject) => {
                //                         this.relatedproject = relatedproject;
                //                         console.log("related projects.."+JSON.stringify(this.relatedproject))
                //                     },
                //                     error => this.errorMessage = <any>error);



                                //update project views
              
                this.projectService.UpdateViewsCount(this.modelProjectId)
                .subscribe(
                  data => {
                
                        console.log('success: ', data)
                  },
                  err => console.log('error: ', err)
                );
                                  //update project views



                this.projectService.getRelatedProjectByProjectId(this.modelProjectId)
                .subscribe(
                (relatedproject) => {
                    this.relatedproject = relatedproject;
                    console.log("related projects by project.."+JSON.stringify(this.relatedproject))
                },
                error => this.errorMessage = <any>error);

                            },
                            error => this.errorMessage = <any>error);


                    this.projectService.getRelatedBlogsByProjectId(this.modelProjectId)
                                    .subscribe(
                                    (relatedblogs) => {
                                        this.relatedblogs = relatedblogs;
                                        console.log("related blogs.."+JSON.stringify(this.relatedblogs))
                                    },
                                    error => this.errorMessage = <any>error);

                     this.projectService.getRelatedReferencePaperByProjectId(this.modelProjectId)
                                    .subscribe(
                                    (relatedreferencepaper) => {
                                        this.relatedreferencepaper = relatedreferencepaper;
                                        console.log("related reference papers.."+JSON.stringify(this.relatedreferencepaper))
                                    },
                                    error => this.errorMessage = <any>error);


                      this.modelusername = new UserNameModel(this.currentusername);
                                
                      this.userService.getUserByName(this.modelusername).subscribe(
                                        user => {
                                          this.user = user;
                                          this.userid = this.user.userid;

                                          // this.userid = this.user.userid;      
                                            console.log("user firstname from event detail"+this.user.firstname);
                                            console.log("user from event details"+JSON.stringify(this.user));
                                          },
                                          error => console.log(error)
                                        )
                           
}

   

}